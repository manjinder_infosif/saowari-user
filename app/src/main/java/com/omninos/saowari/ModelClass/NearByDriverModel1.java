package com.omninos.saowari.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NearByDriverModel1 {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<NearByDriverModel.Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NearByDriverModel.Detail> getDetails() {
        return details;
    }

    public void setDetails(List<NearByDriverModel.Detail> details) {
        this.details = details;
    }


    public class Detail {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("driverName")
        @Expose
        private String driverName;
        @SerializedName("driverVehicleType")
        @Expose
        private String driverVehicleType;
        @SerializedName("previousLongitude")
        @Expose
        private String previousLongitude;
        @SerializedName("previousLatitude")
        @Expose
        private String previousLatitude;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("distance")
        @Expose
        private String distance;
        @SerializedName("duration")
        @Expose
        private String duration;
        @SerializedName("rating")
        @Expose
        private String rating;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getDriverVehicleType() {
            return driverVehicleType;
        }

        public void setDriverVehicleType(String driverVehicleType) {
            this.driverVehicleType = driverVehicleType;
        }

        public String getPreviousLongitude() {
            return previousLongitude;
        }

        public void setPreviousLongitude(String previousLongitude) {
            this.previousLongitude = previousLongitude;
        }

        public String getPreviousLatitude() {
            return previousLatitude;
        }

        public void setPreviousLatitude(String previousLatitude) {
            this.previousLatitude = previousLatitude;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

    }
}
