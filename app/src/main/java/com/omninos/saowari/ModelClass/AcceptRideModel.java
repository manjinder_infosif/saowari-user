package com.omninos.saowari.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AcceptRideModel {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private Details details;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Details getDetails() {
        return details;
    }

    public void setDetails(Details details) {
        this.details = details;
    }

    public class Details {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("jobStatus")
        @Expose
        private String jobStatus;
        @SerializedName("rideOtp")
        @Expose
        private String rideOtp;
        @SerializedName("jobId")
        @Expose
        private String jobId;
        @SerializedName("vehicleId")
        @Expose
        private String vehicleId;
        @SerializedName("driverId")
        @Expose
        private String driverId;
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("picAddress")
        @Expose
        private String picAddress;
        @SerializedName("picLat")
        @Expose
        private String picLat;
        @SerializedName("picLong")
        @Expose
        private String picLong;
        @SerializedName("dropAddress")
        @Expose
        private String dropAddress;
        @SerializedName("dropLat")
        @Expose
        private String dropLat;
        @SerializedName("dropLong")
        @Expose
        private String dropLong;
        @SerializedName("payment")
        @Expose
        private String payment;
        @SerializedName("paymentType")
        @Expose
        private String paymentType;
        @SerializedName("distance")
        @Expose
        private String distance;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("driverName")
        @Expose
        private String driverName;
        @SerializedName("driverImage")
        @Expose
        private String driverImage;
        @SerializedName("driverPhone")
        @Expose
        private String driverPhone;
        @SerializedName("driverLatitude")
        @Expose
        private String driverLatitude;
        @SerializedName("driverLongitude")
        @Expose
        private String driverLongitude;
        @SerializedName("driverVehicleNumber")
        @Expose
        private String driverVehicleNumber;
        @SerializedName("driverRating")
        @Expose
        private String driverRating;
        @SerializedName("vehicleBrand")
        @Expose
        private String vehicleBrand;
        @SerializedName("vehicleModel")
        @Expose
        private String vehicleModel;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getJobStatus() {
            return jobStatus;
        }

        public void setJobStatus(String jobStatus) {
            this.jobStatus = jobStatus;
        }

        public String getRideOtp() {
            return rideOtp;
        }

        public void setRideOtp(String rideOtp) {
            this.rideOtp = rideOtp;
        }

        public String getJobId() {
            return jobId;
        }

        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        public String getVehicleId() {
            return vehicleId;
        }

        public void setVehicleId(String vehicleId) {
            this.vehicleId = vehicleId;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getPicAddress() {
            return picAddress;
        }

        public void setPicAddress(String picAddress) {
            this.picAddress = picAddress;
        }

        public String getPicLat() {
            return picLat;
        }

        public void setPicLat(String picLat) {
            this.picLat = picLat;
        }

        public String getPicLong() {
            return picLong;
        }

        public void setPicLong(String picLong) {
            this.picLong = picLong;
        }

        public String getDropAddress() {
            return dropAddress;
        }

        public void setDropAddress(String dropAddress) {
            this.dropAddress = dropAddress;
        }

        public String getDropLat() {
            return dropLat;
        }

        public void setDropLat(String dropLat) {
            this.dropLat = dropLat;
        }

        public String getDropLong() {
            return dropLong;
        }

        public void setDropLong(String dropLong) {
            this.dropLong = dropLong;
        }

        public String getPayment() {
            return payment;
        }

        public void setPayment(String payment) {
            this.payment = payment;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getDriverImage() {
            return driverImage;
        }

        public void setDriverImage(String driverImage) {
            this.driverImage = driverImage;
        }

        public String getDriverPhone() {
            return driverPhone;
        }

        public void setDriverPhone(String driverPhone) {
            this.driverPhone = driverPhone;
        }

        public String getDriverLatitude() {
            return driverLatitude;
        }

        public void setDriverLatitude(String driverLatitude) {
            this.driverLatitude = driverLatitude;
        }

        public String getDriverLongitude() {
            return driverLongitude;
        }

        public void setDriverLongitude(String driverLongitude) {
            this.driverLongitude = driverLongitude;
        }

        public String getDriverVehicleNumber() {
            return driverVehicleNumber;
        }

        public void setDriverVehicleNumber(String driverVehicleNumber) {
            this.driverVehicleNumber = driverVehicleNumber;
        }

        public String getDriverRating() {
            return driverRating;
        }

        public void setDriverRating(String driverRating) {
            this.driverRating = driverRating;
        }

        public String getVehicleBrand() {
            return vehicleBrand;
        }

        public void setVehicleBrand(String vehicleBrand) {
            this.vehicleBrand = vehicleBrand;
        }

        public String getVehicleModel() {
            return vehicleModel;
        }

        public void setVehicleModel(String vehicleModel) {
            this.vehicleModel = vehicleModel;
        }

    }
}
