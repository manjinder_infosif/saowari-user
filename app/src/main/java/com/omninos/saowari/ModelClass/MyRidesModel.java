package com.omninos.saowari.ModelClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyRidesModel {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("details")
    @Expose
    private List<Detail> details = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Detail> getDetails() {
        return details;
    }

    public void setDetails(List<Detail> details) {
        this.details = details;
    }

    public class Detail {

        @SerializedName("jobId")
        @Expose
        private String jobId;
        @SerializedName("jobStatus")
        @Expose
        private String jobStatus;
        @SerializedName("picAddress")
        @Expose
        private String picAddress;
        @SerializedName("dropAddress")
        @Expose
        private String dropAddress;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("jobDateTime")
        @Expose
        private String jobDateTime;

        public String getJobId() {
            return jobId;
        }

        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        public String getJobStatus() {
            return jobStatus;
        }

        public void setJobStatus(String jobStatus) {
            this.jobStatus = jobStatus;
        }

        public String getPicAddress() {
            return picAddress;
        }

        public void setPicAddress(String picAddress) {
            this.picAddress = picAddress;
        }

        public String getDropAddress() {
            return dropAddress;
        }

        public void setDropAddress(String dropAddress) {
            this.dropAddress = dropAddress;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getJobDateTime() {
            return jobDateTime;
        }

        public void setJobDateTime(String jobDateTime) {
            this.jobDateTime = jobDateTime;
        }

    }
}
