package com.omninos.saowari.Retrofit;

import com.omninos.saowari.ModelClass.AcceptRideModel;
import com.omninos.saowari.ModelClass.CheckUserJob;
import com.omninos.saowari.ModelClass.GetCallListModel;
import com.omninos.saowari.ModelClass.GetContactNumber;
import com.omninos.saowari.ModelClass.GetConversionModel;
import com.omninos.saowari.ModelClass.GetDriverCurrentLocation;
import com.omninos.saowari.ModelClass.GetLatLngModel;
import com.omninos.saowari.ModelClass.HelpAndContactusModel;
import com.omninos.saowari.ModelClass.LoginRegisterModel;
import com.omninos.saowari.ModelClass.MessageInboxModel;
import com.omninos.saowari.ModelClass.MyRidesModel;
import com.omninos.saowari.ModelClass.NearByDriverModel;
import com.omninos.saowari.ModelClass.NearByDriverModel1;
import com.omninos.saowari.ModelClass.PlaceSearchModel;
import com.omninos.saowari.ModelClass.SendMessageModel;
import com.omninos.saowari.ModelClass.VehicleTypePojo;
import com.omninos.saowari.ModelClass.VersionModel;
import com.omninos.saowari.directionApi.DirectionPojo;
//70
//188
//83
//828
import org.json.JSONArray;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;

public interface Api {
    @Multipart
    @POST("userRegister")
    Call<LoginRegisterModel> userRegister(@Part("name") RequestBody name,
                                          @Part("phone") RequestBody phone,
                                          @Part("email") RequestBody email,
                                          @Part("dateOfBirth") RequestBody dateOfBirth,
                                          @Part("gender") RequestBody gender,
                                          @Part("device_type") RequestBody device_type,
                                          @Part("reg_id") RequestBody reg_id,
                                          @Part("latitude") RequestBody latitude,
                                          @Part("longitude") RequestBody longitude,
                                          @Part("login_type") RequestBody login_type,
                                          @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("checkPhoneNumber")
    Call<LoginRegisterModel> userLogin(@Field("phone") String phone,
                                       @Field("latitude") String latitude,
                                       @Field("longitude") String longitude,
                                       @Field("reg_id") String reg_id,
                                       @Field("device_type") String device_type,
                                       @Field("login_type") String login_type);


    @GET("maps/api/place/autocomplete/json?")
    Call<PlaceSearchModel> placeSearch(@QueryMap Map<String, String> places);


    @GET("maps/api/geocode/json?")
    Call<GetLatLngModel> getLocationFromAddress(@QueryMap Map<String, String> address);


    @GET("maps/api/directions/json?")
    Call<DirectionPojo> getPolyLine(@QueryMap Map<String, String> data);

    @GET("brandDetails")
    Call<VehicleTypePojo> vehicleDetails();

    @FormUrlEncoded
    @POST("nearByDriver")
    Call<NearByDriverModel1> nearDriver(@Field("vehicleId") String vehicleId,
                                        @Field("latitude") String latitude,
                                        @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("nearByDriver")
    Call<Map> nearDriver1(@Field("vehicleId") String vehicleId,
                          @Field("latitude") String latitude,
                          @Field("longitude") String longitude);


    @FormUrlEncoded
    @POST("userSendBookingRequest")
    Call<Map> SendRequest(@Field("picAddress") String picAddress,
                          @Field("picLat") String picLat,
                          @Field("picLong") String picLong,
                          @Field("dropAddress") String dropAddress,
                          @Field("dropLat") String dropLat,
                          @Field("dropLong") String dropLong,
                          @Field("vehicleId") String vehicleId,
                          @Field("userId") String userId,
                          @Field("payment") String payment,
                          @Field("paymentType") String paymentType,
                          @Field("distance") String distance);


    @FormUrlEncoded
    @POST("userAcceptRide")
    Call<AcceptRideModel> acceptRide(@Field("userId") String UserId);


    @FormUrlEncoded
    @POST("checkUserJob")
    Call<GetDriverCurrentLocation> getCurrentDriverLocation(@Field("jobId") String jobId);


    @FormUrlEncoded
    @POST("userRides")
    Call<MyRidesModel> myRide(@Field("userId") String userId);

    @Multipart
    @POST("updateUserProfile")
    Call<LoginRegisterModel> updateProfile(@Part("name") RequestBody name,
                                           @Part("userId") RequestBody userId,
                                           @Part("phone") RequestBody phone,
                                           @Part("latitude") RequestBody latitude,
                                           @Part("longitude") RequestBody longitude,
                                           @Part("email") RequestBody email,
                                           @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("userHelpContact")
    Call<HelpAndContactusModel> HelpContact(@Field("type") String type,
                                            @Field("name") String name,
                                            @Field("phone") String phone,
                                            @Field("email") String email,
                                            @Field("message") String message,
                                            @Field("userId") String driverId);

    @FormUrlEncoded
    @POST("userEmergencyContacts")
    Call<Map> saveContact(@Field("userId") String userId,
                          @Field("emergencyContact") JSONArray jsonArray);


    @FormUrlEncoded
    @POST("getEmergencyContactsList")
    Call<GetContactNumber> getNumber(@Field("userId") String userId);


    @FormUrlEncoded
    @POST("inboxMessage")
    Call<MessageInboxModel> getInbox(@Field("sender_id") String sender_id,
                                     @Field("type") String type);

    @FormUrlEncoded
    @POST("conversationMessage")
    Call<GetConversionModel> getConversion(@Field("sender_id") String sender_id,
                                           @Field("reciver_id") String reciver_id,
                                           @Field("type") String type);

    @FormUrlEncoded
    @POST("sendMessage")
    Call<SendMessageModel> sendMessage(@Field("sender_id") String sender_id,
                                       @Field("reciver_id") String reciver_id,
                                       @Field("message") String message,
                                       @Field("type") String type);


    @FormUrlEncoded
    @POST("userCancelJob")
    Call<Map> cancleJob(@Field("userId") String jobId,
                        @Field("reason") String reason);

    @FormUrlEncoded
    @POST("userLogOut")
    Call<Map> logOut(@Field("userId") String userId);

    @FormUrlEncoded
    @POST("addcalls")
    Call<Map> addCall(@Field("userId") String userId,
                      @Field("driverId") String driverId,
                      @Field("type") String type,
                      @Field("startTime") String startTime,
                      @Field("callDate") String endTime);

    @FormUrlEncoded
    @POST("userRatingDriver")
    Call<Map> driverRating(@Field("jobId") String jobId,
                           @Field("driverId") String driverId,
                           @Field("userId") String userId,
                           @Field("comment") String comment,
                           @Field("rating") String rating);

    @FormUrlEncoded
    @POST("userCallList")
    Call<GetCallListModel> getCallList(@Field("userId") String userId);


    @FormUrlEncoded
    @POST("checkVersion")
    Call<VersionModel> version(@Field("type") String type);

    @FormUrlEncoded
    @POST("checkUserJobStatus")
    Call<CheckUserJob> checkJob(@Field("userId") String userId);
}
