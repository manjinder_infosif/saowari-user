package com.omninos.saowari.Retrofit;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    public static Retrofit retrofit,retrofitRoute = null;


    private static final String BASE_URL_DIRECTION = "https://maps.googleapis.com/";

    public static Retrofit getApiClient(){

        if (retrofit==null){


            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .retryOnConnectionFailure(true)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(30, TimeUnit.SECONDS)
                    .build();

            retrofit=new Retrofit.Builder()
                    .baseUrl("http://futurewingsvisa.in/saowariApplication/index.php/api/Users/")
//                    .baseUrl("http://3.130.252.86/newApp/index.php/api/Users/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }



    public static Retrofit getClientRoute() {

        if (retrofitRoute == null) {


            retrofitRoute = new Retrofit.Builder()
                    .baseUrl(BASE_URL_DIRECTION)
                    .addConverterFactory(GsonConverterFactory.create())

                    .build();
        }
        return retrofitRoute;

    }
}
