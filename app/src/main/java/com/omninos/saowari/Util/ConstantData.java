package com.omninos.saowari.Util;

public class ConstantData {
    public static final String USERID = "User_id";
    public static final String TOKEN = "Token";
    public static final String IMAGEPATH = "imagePath";
    public static final String IMAGE = "image";
    public static final String IMAGELINK = "ImageLink";
    public static final String SAOWARI_USER = "Saowari_user";
    public static final String LOG_IN_DATA = "log_in_data";
    public static final String Fill_DETAILS = "Fill Mandetory Fields";
    public static final String SHOW_PROGRESS_MESSAGE ="Please Wait..." ;
    public static String EnableInternet = "1";



    public static final String JOB_ID = "job_id";
    public static final String DRIVER_ID = "driver_id";
    public static final String EMERGENCY_CONTACT = "emergency_contact";
    public static final String DrIVER_NUMBER = "driver_number";

}