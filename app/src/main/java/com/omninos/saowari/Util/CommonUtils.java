package com.omninos.saowari.Util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;
import com.kaopiz.kprogresshud.KProgressHUD;


public class CommonUtils {

    private static KProgressHUD progressDialog;

    //
    public static void showProgress(Activity activity) {
//        progressDialog = new Dialog(activity);
//        progressDialog.setTitle("Please wait...");
//        progressDialog.setContentView(R.layout.spinkitanimation);
//        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        Window dialogWindow = progressDialog.getWindow();
//        dialogWindow.setGravity(Gravity.CENTER);
//        progressDialog.setCancelable(false);
//        progressDialog.show();
        progressDialog = KProgressHUD.create(activity)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
    }

    public static void dismissProgress() {
        progressDialog.dismiss();
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static void showSnackbarAlert(View view, String message) {
        TSnackbar snackbar = TSnackbar.make(view, message, TSnackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.WHITE);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.RED);
        TextView textView = snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

}
