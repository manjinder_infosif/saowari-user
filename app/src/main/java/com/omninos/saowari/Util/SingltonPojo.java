package com.omninos.saowari.Util;


import com.google.android.gms.maps.model.LatLng;

public class SingltonPojo {

    LatLng CurrentLatlng;
    String Mobilenumber,PickUpPoint,SouceLat,SourceLng,DropPoint,DestiLat,DestiLng,vehicleId,Payment,Cancelrequest,DriverLastLat,DriverLastLng;

    String LocationStatus,editAddress,editLat,editlng,CallStatus;

    public String getCallStatus() {
        return CallStatus;
    }

    public void setCallStatus(String callStatus) {
        CallStatus = callStatus;
    }

    public String getEditAddress() {
        return editAddress;
    }

    public void setEditAddress(String editAddress) {
        this.editAddress = editAddress;
    }

    public String getEditLat() {
        return editLat;
    }

    public void setEditLat(String editLat) {
        this.editLat = editLat;
    }

    public String getEditlng() {
        return editlng;
    }

    public void setEditlng(String editlng) {
        this.editlng = editlng;
    }

    public String getLocationStatus() {
        return LocationStatus;
    }

    public void setLocationStatus(String locationStatus) {
        LocationStatus = locationStatus;
    }

    public String getDriverLastLat() {
        return DriverLastLat;
    }

    public void setDriverLastLat(String driverLastLat) {
        DriverLastLat = driverLastLat;
    }

    public String getDriverLastLng() {
        return DriverLastLng;
    }

    public void setDriverLastLng(String driverLastLng) {
        DriverLastLng = driverLastLng;
    }

    public String getCancelrequest() {
        return Cancelrequest;
    }

    public void setCancelrequest(String cancelrequest) {
        Cancelrequest = cancelrequest;
    }

    public String getPayment() {
        return Payment;
    }

    public void setPayment(String payment) {
        Payment = payment;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getDropPoint() {
        return DropPoint;
    }

    public void setDropPoint(String dropPoint) {
        DropPoint = dropPoint;
    }

    public String getDestiLat() {
        return DestiLat;
    }

    public void setDestiLat(String destiLat) {
        DestiLat = destiLat;
    }

    public String getDestiLng() {
        return DestiLng;
    }

    public void setDestiLng(String destiLng) {
        DestiLng = destiLng;
    }

    public String getPickUpPoint() {
        return PickUpPoint;
    }

    public void setPickUpPoint(String pickUpPoint) {
        PickUpPoint = pickUpPoint;
    }

    public String getSouceLat() {
        return SouceLat;
    }

    public void setSouceLat(String souceLat) {
        SouceLat = souceLat;
    }

    public String getSourceLng() {
        return SourceLng;
    }

    public void setSourceLng(String sourceLng) {
        SourceLng = sourceLng;
    }

    public LatLng getCurrentLatlng() {
        return CurrentLatlng;
    }

    public void setCurrentLatlng(LatLng currentLatlng) {
        CurrentLatlng = currentLatlng;
    }

    public String getMobilenumber() {
        return Mobilenumber;
    }

    public void setMobilenumber(String mobilenumber) {
        Mobilenumber = mobilenumber;
    }
}
