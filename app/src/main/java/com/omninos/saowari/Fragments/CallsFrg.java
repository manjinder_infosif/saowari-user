package com.omninos.saowari.Fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.omninos.saowari.ModelClass.GetCallListModel;
import com.omninos.saowari.MyViewModelClasses.MessegeViewModel;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.adapters.CallAdapter;

public class CallsFrg extends Fragment {

    private RecyclerView callsRC;
    View view;
    private CallAdapter adapter;
    private MessegeViewModel viewModel;


    public CallsFrg() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_calls_frg, container, false);

        viewModel = ViewModelProviders.of(this).get(MessegeViewModel.class);

        initView();
        SetUp();
        getList();
        return view;
    }

    private void getList() {

        viewModel.getCallListModelLiveData(getActivity(), App.getAppPreference().GetString(ConstantData.USERID)).observe(getActivity(), new Observer<GetCallListModel>() {
            @Override
            public void onChanged(@Nullable GetCallListModel getCallListModel) {
                if (getCallListModel.getSuccess().equalsIgnoreCase("1")) {
                    if (getCallListModel.getDetails() != null) {
                        adapter = new CallAdapter(getActivity(), getCallListModel.getDetails());
                        callsRC.setAdapter(adapter);
                    }
                } else {

                }
            }
        });
    }

    private void initView() {
        callsRC = view.findViewById(R.id.callsRC);
    }

    private void SetUp() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        callsRC.setLayoutManager(linearLayoutManager);
    }
}
