package com.omninos.saowari.Fragments;


import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.omninos.saowari.ModelClass.MessageInboxModel;
import com.omninos.saowari.MyViewModelClasses.MessegeViewModel;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.activities.ChatActivity;
import com.omninos.saowari.adapters.ChatAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatsFrg extends Fragment {

    View view;
    private RecyclerView chatsRC;
    private ChatAdapter adapter;
    private MessegeViewModel viewModel;

    public ChatsFrg() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_chats_frg, container, false);
        viewModel = ViewModelProviders.of(this).get(MessegeViewModel.class);
        initView();
        SetUp();
        return view;
    }

    private void initView() {
        chatsRC = view.findViewById(R.id.chatsRC);
    }

    private void SetUp() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        chatsRC.setLayoutManager(linearLayoutManager);

        getList();


    }

    private void getList() {
        viewModel.messageInboxModelLiveData(getActivity(), App.getAppPreference().GetString(ConstantData.USERID)).observe(getActivity(), new Observer<MessageInboxModel>() {
            @Override
            public void onChanged(@Nullable final MessageInboxModel messageInboxModel) {
                if (messageInboxModel.getSuccess().equalsIgnoreCase("1")) {
                    if (messageInboxModel.getMessageDetails() != null) {
                        adapter = new ChatAdapter(getActivity(), messageInboxModel.getMessageDetails(), new ChatAdapter.Choose() {
                            @Override
                            public void Select(int position) {
                                Intent intent = new Intent(getActivity(), ChatActivity.class);
//                                if (messageInboxModel.getMessageDetails().get(position).getType().equalsIgnoreCase("user")) {
//
//                                    intent.putExtra("driver", messageInboxModel.getMessageDetails().get(position).getReciverId());
//                                } else {

                                if (messageInboxModel.getMessageDetails().get(position).getType().equalsIgnoreCase("driver")){
                                    intent.putExtra("driver", messageInboxModel.getMessageDetails().get(position).getSenderId());
                                }else if (messageInboxModel.getMessageDetails().get(position).getType().equalsIgnoreCase("user")){
                                    intent.putExtra("driver", messageInboxModel.getMessageDetails().get(position).getReciverId());
                                }

                                intent.putExtra("status", messageInboxModel.getMessageDetails().get(position).getDriveStatus());
//                                }
                                intent.putExtra("Title", messageInboxModel.getMessageDetails().get(position).getName());
                                startActivity(intent);
                            }
                        });
                        chatsRC.setAdapter(adapter);
                    }
                } else {
                    CommonUtils.showSnackbarAlert(chatsRC, messageInboxModel.getMessage());
                }

            }
        });
    }

}
