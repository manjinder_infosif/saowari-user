package com.omninos.saowari.Fragments;


import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowari.MyViewModelClasses.BookingViewModel;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.activities.MainActivity;

import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DriverRatingFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private RatingBar ratingBar;
    private TextView rateId;
    private CircleImageView driverImage;
    private EditText comments;
    private Button sendReview;
    private String ratingValue = "0.0", DriverName, DriverImage, JobID, DriverId;
    private BookingViewModel viewModel;

    public DriverRatingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_driver_rating, container, false);
        viewModel = ViewModelProviders.of(this).get(BookingViewModel.class);

        Dialog dialog = getDialog();

        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);

        initView(view);
        Setups(view);
        return view;
    }

    private void Setups(View view) {
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
//                Toast.makeText(getActivity(), String.valueOf(rating), Toast.LENGTH_SHORT).show();
                ratingValue = String.valueOf(rating);
            }
        });
        sendReview.setOnClickListener(this);

    }

    private void initView(View view) {
        ratingBar = view.findViewById(R.id.ratingBar);
        rateId = view.findViewById(R.id.rateId);
        driverImage = view.findViewById(R.id.driverImage);
        comments = view.findViewById(R.id.comments);
        sendReview = view.findViewById(R.id.sendReview);
        DriverName = getArguments().getString("DriverName");
        DriverImage = getArguments().getString("DriverImage");
        JobID = getArguments().getString("JobID");
        DriverId = getArguments().getString("DriverId");
        rateId.setText("Rate your trip with " + DriverName);
        Glide.with(getActivity()).load(DriverImage).into(driverImage);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendReview:
                GiveRatig();
                break;
        }
    }

    private void GiveRatig() {
        String Feedback = comments.getText().toString();
        viewModel.Rating(getActivity(), App.getAppPreference().GetString(ConstantData.USERID), JobID, DriverId, Feedback, ratingValue).observe(getActivity(), new Observer<Map>() {
            @Override
            public void onChanged(@Nullable Map map) {
                if (map.get("success").equals("1")) {
                    App.getAppPreference().SaveString(ConstantData.JOB_ID, "");
                    App.getAppPreference().SaveString("MainData","0");
                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finishAffinity();
                } else {

                }
            }
        });
    }

}
