package com.omninos.saowari.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.omninos.saowari.R;
import com.omninos.saowari.activities.BookingActivity;
import com.omninos.saowari.activities.PaymentActivity;
import com.omninos.saowari.adapters.VehicleDetailAdapter;

public class BottomVehicleDetailFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    View view;
    private RecyclerView recyclerView;
    private VehicleDetailAdapter adapter;
    private TextView paymentButton;
    private Button confirmBooking;

    public BottomVehicleDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_bottom_vehicle_detail, container, false);
        initView();

        return view;
    }

    private void initView() {
        recyclerView = view.findViewById(R.id.recyclerView);
        paymentButton = view.findViewById(R.id.paymentButton);
        confirmBooking=view.findViewById(R.id.confirmBooking);

        paymentButton.setOnClickListener(this);
        confirmBooking.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView);
//        adapter = new VehicleDetailAdapter(getActivity());
//        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.paymentButton:
                startActivity(new Intent(getActivity(), PaymentActivity.class));
                break;

            case R.id.confirmBooking:
                startActivity(new Intent(getActivity(), BookingActivity.class));
                break;

        }
    }
}
