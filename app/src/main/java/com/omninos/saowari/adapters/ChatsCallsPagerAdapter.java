package com.omninos.saowari.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.omninos.saowari.Fragments.CallsFrg;
import com.omninos.saowari.Fragments.ChatsFrg;

public class ChatsCallsPagerAdapter extends FragmentPagerAdapter {
    public ChatsCallsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new ChatsFrg();
        } else if (position == 1) {
            fragment = new CallsFrg();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Chats";
        } else if (position == 1) {
            title = "Calls";
        }
        return title;
    }
}
