package com.omninos.saowari.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.omninos.saowari.ModelClass.MyRidesModel;
import com.omninos.saowari.R;

import java.util.List;

public class MyRideAdapter extends RecyclerView.Adapter<MyRideAdapter.MyViewHolder> {

    Context context;
    private List<MyRidesModel.Detail> list;

    public MyRideAdapter(Context context, List<MyRidesModel.Detail> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_myride_layout, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.numberRide.setText("No. " + list.get(i).getJobId());
        myViewHolder.dateTimeRide.setText(list.get(i).getJobDateTime());
        myViewHolder.picAddress.setText(list.get(i).getPicAddress());
        myViewHolder.dropAddress.setText(list.get(i).getDropAddress());
        if (list.get(i).getJobStatus().equalsIgnoreCase("0")) {
            myViewHolder.finishedRide.setText("Waiting");
        } else if (list.get(i).getJobStatus().equalsIgnoreCase("1")) {
            myViewHolder.finishedRide.setText("Accept");
        } else if (list.get(i).getJobStatus().equalsIgnoreCase("2")) {
            myViewHolder.finishedRide.setText("Reject");
        } else if (list.get(i).getJobStatus().equalsIgnoreCase("3")) {
            myViewHolder.finishedRide.setText("Arrive");
        } else if (list.get(i).getJobStatus().equalsIgnoreCase("4")) {
            myViewHolder.finishedRide.setText("Start");
        } else if (list.get(i).getJobStatus().equalsIgnoreCase("5")) {
            myViewHolder.finishedRide.setText("Drop");
        } else if (list.get(i).getJobStatus().equalsIgnoreCase("6")) {
            myViewHolder.finishedRide.setText("Finished");
        } else if (list.get(i).getJobStatus().equalsIgnoreCase("7")) {
            myViewHolder.finishedRide.setText("Canceled");
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView numberRide, dateTimeRide, picAddress, dropAddress;
        private Button statusRide, finishedRide;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            numberRide = itemView.findViewById(R.id.numberRide);
            dateTimeRide = itemView.findViewById(R.id.dateTimeRide);
            picAddress = itemView.findViewById(R.id.picAddress);
            dropAddress = itemView.findViewById(R.id.dropAddress);
            statusRide = itemView.findViewById(R.id.statusRide);
            finishedRide = itemView.findViewById(R.id.finishedRide);

        }
    }
}
