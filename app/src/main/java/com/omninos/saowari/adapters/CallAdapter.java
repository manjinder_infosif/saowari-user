package com.omninos.saowari.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowari.ModelClass.GetCallListModel;
import com.omninos.saowari.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CallAdapter extends RecyclerView.Adapter<CallAdapter.MyViewHolder> {

    Context context;
    List<GetCallListModel.Detail> list;


    public CallAdapter(Context context, List<GetCallListModel.Detail> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_call_layout, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Glide.with(context).load(list.get(i).getImage()).into(myViewHolder.imageCallsItem);
        myViewHolder.callsName.setText(list.get(i).getName());
        myViewHolder.dateAndTime.setText(list.get(i).getCallDate() + "," + list.get(i).getStartTime());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView imageCallsItem;
        private TextView callsName, dateAndTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageCallsItem = itemView.findViewById(R.id.imageCallsItem);
            callsName = itemView.findViewById(R.id.callsName);
            dateAndTime = itemView.findViewById(R.id.dateAndTime);
        }
    }
}
