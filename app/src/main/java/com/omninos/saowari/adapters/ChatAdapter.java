package com.omninos.saowari.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowari.ModelClass.MessageInboxModel;
import com.omninos.saowari.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.MyViewHolder> {


    Context context;
    List<MessageInboxModel.MessageDetail> list;
    Choose choose;

    public interface Choose {
        void Select(int position);
    }

    public ChatAdapter(Context context, List<MessageInboxModel.MessageDetail> list, Choose choose) {
        this.context = context;
        this.list = list;
        this.choose = choose;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_chat_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Glide.with(context).load(list.get(i).getImage()).into(myViewHolder.imageChatItem);
        myViewHolder.nametv.setText(list.get(i).getName());
        myViewHolder.messageTv.setText(list.get(i).getMessage());
        myViewHolder.timeData.setText(list.get(i).getTime());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private RelativeLayout chat_items;
        private TextView nametv, messageTv, timeData;
        private CircleImageView imageChatItem;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            chat_items = itemView.findViewById(R.id.chat_items);
            chat_items.setOnClickListener(this);

            nametv = itemView.findViewById(R.id.nametv);
            messageTv = itemView.findViewById(R.id.messageTv);
            timeData = itemView.findViewById(R.id.timeData);
            imageChatItem = itemView.findViewById(R.id.imageChatItem);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.chat_items:
                    choose.Select(getLayoutPosition());
                    break;
            }
        }
    }
}
