package com.omninos.saowari.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowari.ModelClass.VehicleTypePojo;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;

import org.w3c.dom.Text;

import java.util.List;

public class VehicleDetailAdapter extends RecyclerView.Adapter<VehicleDetailAdapter.MyViewHolder> {

    Context context;
    private List<VehicleTypePojo.Detail> list;
    private String newDistance, timeTaken;
    private int duration, price, converKm;
    ChooseVehical chooseVehical;
    double km;

    public interface ChooseVehical {
        void choose(String position);

        void passanger(String maxUsr);

        void longPressData(int position);
    }


    public VehicleDetailAdapter(Context context, String timeTaken, String newDistance, int duration, List<VehicleTypePojo.Detail> list, ChooseVehical chooseVehical) {
        this.context = context;
        this.timeTaken = timeTaken;
        this.list = list;
        this.newDistance = newDistance;
        this.duration = duration;
        this.chooseVehical = chooseVehical;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.custom_vehicle_layout, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, final int i) {
        Glide.with(context).load(list.get(i).getServiceImage1()).into(myViewHolder.bike_icon);
        myViewHolder.typeName.setText(list.get(i).getTitle());
        myViewHolder.nearvehicle.setText(newDistance + " Km");
        myViewHolder.timeVehicle.setText(timeTaken);

        if (list.get(i).getPricingLogic().equalsIgnoreCase("MIN")) {


            try {
                double tempprice = (duration * Double.parseDouble(list.get(i).getUnitTimePricing()) + Double.parseDouble(list.get(i).getBasePrice()));
                price = (int) tempprice;

            } catch (Exception e) {

            }

        } else if (list.get(i).getPricingLogic().equalsIgnoreCase("DISTANCE")) {
            try {

                km = Double.parseDouble(newDistance);
                converKm = (int) km;
                double tempPrice = (km * Double.parseDouble(list.get(i).getUnitDistancePrice()) + Double.parseDouble(list.get(i).getBasePrice()));
                price = (int) tempPrice;

            } catch (Exception e) {

            }
        } else if (list.get(i).getPricingLogic().equalsIgnoreCase("DISTANCEMIN")) {
            try {
                km = Double.parseDouble(newDistance);
                converKm = (int) km;
                double TempPrice = (km * Double.parseDouble(list.get(i).getUnitDistancePrice()));
                price = (int) TempPrice;

                double Temp1 = (duration * Double.parseDouble(list.get(i).getUnitTimePricing()));
                price = price + (int) Temp1;

                double Temp2 = Double.parseDouble(list.get(i).getBaseDistance());
                price = price + (int) Temp2;

            } catch (Exception e) {

            }
        }

        myViewHolder.priceText.setText("৳ " + String.valueOf(price));

        myViewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseVehical.choose(list.get(i).getId());
                App.getSinltonPojo().setPayment(myViewHolder.priceText.getText().toString().replace("$৳", "").trim());
                chooseVehical.passanger(list.get(i).getSeatCapacity());
//
            }
        });

        if (i == 0) {
            chooseVehical.choose(list.get(i).getId());
            App.getSinltonPojo().setPayment(myViewHolder.priceText.getText().toString().replace("৳", "").trim());
            chooseVehical.passanger(list.get(i).getSeatCapacity());
        }

//        myViewHolder.mainLayout.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                chooseVehical.longPressData(i);
//                return true;
//            }
//        });
        myViewHolder.openDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseVehical.longPressData(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout mainLayout;
        private ImageView bike_icon, openDetail;
        private TextView priceText, nearvehicle, timeVehicle, typeName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            bike_icon = itemView.findViewById(R.id.bike_icon);
            priceText = itemView.findViewById(R.id.priceText);
            nearvehicle = itemView.findViewById(R.id.nearvehicle);
            timeVehicle = itemView.findViewById(R.id.timeVehicle);
            mainLayout = itemView.findViewById(R.id.mainLayout);
            typeName = itemView.findViewById(R.id.typeName);
            openDetail = itemView.findViewById(R.id.openDetail);

        }
    }
}
