package com.omninos.saowari.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.omninos.saowari.ModelClass.ChatModelClass;
import com.omninos.saowari.ModelClass.GetConversionModel;
import com.omninos.saowari.R;

import java.util.ArrayList;
import java.util.List;

public class ChatInboxAdapter extends RecyclerView.Adapter<ChatInboxAdapter.MyViewHolder> {

    Context context;
    List<GetConversionModel.MessageDetail> list;

    public ChatInboxAdapter(Context context, List<GetConversionModel.MessageDetail> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.custom_chat_layout, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        if (list.get(i).getType().equalsIgnoreCase("driver")) {
            myViewHolder.right.setVisibility(View.GONE);
            myViewHolder.left.setVisibility(View.VISIBLE);
            myViewHolder.leftChat.setText(list.get(i).getMessage());
        } else {
            myViewHolder.right.setVisibility(View.VISIBLE);
            myViewHolder.left.setVisibility(View.GONE);
            myViewHolder.rightChat.setText(list.get(i).getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout right, left;
        TextView rightChat, leftChat;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            right = itemView.findViewById(R.id.right);
            left = itemView.findViewById(R.id.left);
            rightChat = itemView.findViewById(R.id.rightChat);
            leftChat = itemView.findViewById(R.id.leftChat);

        }
    }
}
