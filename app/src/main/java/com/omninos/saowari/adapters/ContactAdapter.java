package com.omninos.saowari.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.omninos.saowari.ModelClass.GetContactNumber;
import com.omninos.saowari.R;

import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.MyViewHolder> {
    Context context;
    List<GetContactNumber> list;
    List<String> numberList;
    List<String> nameList;
    String status;

    public ContactAdapter(Context context, List<GetContactNumber> list, String status) {
        this.context = context;
        this.list = list;
        this.status = status;
    }

    public ContactAdapter(Context context, List<String> numberList, List<String> nameList, String status) {
        this.context = context;
        this.numberList = numberList;
        this.nameList = nameList;
        this.status = status;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_emergency_layout, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (status.equalsIgnoreCase("1")) {
            holder.tv_emergency_contact_name.setText(list.get(position).getDetails().get(position).getName());
            holder.tv_emergency_contact_number.setText(list.get(position).getDetails().get(position).getPhone());
        } else {
            String contactName = nameList.get(position);
            String contactNumber = numberList.get(position);
            if (!contactName.equalsIgnoreCase("")) {
                holder.tv_emergency_contact_name.setText(contactName);
            }
            if (!contactNumber.equalsIgnoreCase("")) {
                holder.tv_emergency_contact_number.setText(contactNumber);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (status.equalsIgnoreCase("1")) {
            return list.size();
        } else {
            return numberList.size();
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_emergency_contact_name, tv_emergency_contact_number;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_emergency_contact_name = itemView.findViewById(R.id.tv_emergency_contact_name);
            tv_emergency_contact_number = itemView.findViewById(R.id.tv_emergency_contact_number);

        }
    }
}
