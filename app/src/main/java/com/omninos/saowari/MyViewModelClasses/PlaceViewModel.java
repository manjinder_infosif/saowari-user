package com.omninos.saowari.MyViewModelClasses;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import com.omninos.saowari.ModelClass.GetLatLngModel;
import com.omninos.saowari.ModelClass.PlaceSearchModel;
import com.omninos.saowari.Retrofit.Api;
import com.omninos.saowari.Retrofit.ApiClient;
import com.omninos.saowari.Util.CommonUtils;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceViewModel extends ViewModel {

    private MutableLiveData<PlaceSearchModel> placeSearchModelMutableLiveData;

    private MutableLiveData<GetLatLngModel> getLatLngModelMutableLiveData;

    public LiveData<PlaceSearchModel> placeSearchModelLiveData(final Activity activity, Map<String, String> place) {
        if (CommonUtils.isNetworkConnected(activity)) {
            placeSearchModelMutableLiveData = new MutableLiveData<>();

            Api api = ApiClient.getClientRoute().create(Api.class);

            api.placeSearch(place).enqueue(new Callback<PlaceSearchModel>() {
                @Override
                public void onResponse(Call<PlaceSearchModel> call, Response<PlaceSearchModel> response) {
                    if (response.body() != null) {
                        placeSearchModelMutableLiveData.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<PlaceSearchModel> call, Throwable t) {
                    Toast.makeText(activity, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return placeSearchModelMutableLiveData;
    }


    public LiveData<GetLatLngModel> getLatLngModelLiveData(final Activity activity, Map<String, String> address) {
        if (CommonUtils.isNetworkConnected(activity)) {

            getLatLngModelMutableLiveData = new MutableLiveData<>();
            Api api = ApiClient.getClientRoute().create(Api.class);

            api.getLocationFromAddress(address).enqueue(new Callback<GetLatLngModel>() {
                @Override
                public void onResponse(Call<GetLatLngModel> call, Response<GetLatLngModel> response) {
                    if (response.body() != null) {
                        getLatLngModelMutableLiveData.setValue(response.body());
                    } else {
                        Toast.makeText(activity, "", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GetLatLngModel> call, Throwable t) {

                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }

        return getLatLngModelMutableLiveData;
    }
}
