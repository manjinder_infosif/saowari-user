package com.omninos.saowari.MyViewModelClasses;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.Handler;
import android.widget.Toast;

import com.omninos.saowari.ModelClass.AcceptRideModel;
import com.omninos.saowari.ModelClass.GetDriverCurrentLocation;
import com.omninos.saowari.Retrofit.Api;
import com.omninos.saowari.Retrofit.ApiClient;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingViewModel extends ViewModel {
    private MutableLiveData<Map> bookingRequest;
    private MutableLiveData<AcceptRideModel> acceptRideModelMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<GetDriverCurrentLocation> getDriverCurrentLocationMutableLiveData = new MutableLiveData<>();
    private MutableLiveData<Map> DriverRating;

    private MutableLiveData<Map> cancle;


    public LiveData<Map> booking(final Activity activity, String picAddress, String picLat, String picLong, String dropAddress, String dropLat, String dropLong, String vehicleId, String userId, String payment, String paymentType, String distance) {

        bookingRequest = new MutableLiveData<>();

        Api api = ApiClient.getApiClient().create(Api.class);

//        CommonUtils.showProgress(activity);
        api.SendRequest(picAddress, picLat, picLong, dropAddress, dropLat, dropLong, vehicleId, userId, payment, paymentType, distance).enqueue(new Callback<Map>() {
            @Override
            public void onResponse(Call<Map> call, Response<Map> response) {
//                CommonUtils.dismissProgress();
                if (response.body() != null) {
                    bookingRequest.setValue(response.body());
                } else {

                }
            }

            @Override
            public void onFailure(Call<Map> call, Throwable t) {
//                CommonUtils.dismiss();
                Toast.makeText(activity, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        return bookingRequest;
    }


    public LiveData<AcceptRideModel> acceptRideModelLiveData(final Activity activity, final String userId) {

        if (App.getSinltonPojo().getCancelrequest().equalsIgnoreCase("0")) {

            if (CommonUtils.isNetworkConnected(activity)) {
//            CommonUtils.showProgress(activity);
                Api api = ApiClient.getApiClient().create(Api.class);
                api.acceptRide(userId).enqueue(new Callback<AcceptRideModel>() {
                    @Override
                    public void onResponse(Call<AcceptRideModel> call, Response<AcceptRideModel> response) {
//                    CommonUtils.dismiss();
                        if (response.body() != null) {
                            if (response.body().getSuccess().equalsIgnoreCase("1")) {
                                acceptRideModelMutableLiveData.postValue(response.body());
                            } else {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        acceptRideModelLiveData(activity, userId);
                                    }
                                }, 5000);
                            }

                        } else {
//                        acceptRideModelLiveData(activity, userId);

                        }
                    }

                    @Override
                    public void onFailure(Call<AcceptRideModel> call, Throwable t) {
//                    CommonUtils.dismiss();
                        Toast.makeText(activity, t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

            } else {
                Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
            }
        } else {

        }

        return acceptRideModelMutableLiveData;
    }


    public LiveData<GetDriverCurrentLocation> getDriverCurrentLocationLiveData(final Activity activity, final String jobID) {

        Api api = ApiClient.getApiClient().create(Api.class);

        api.getCurrentDriverLocation(jobID).enqueue(new Callback<GetDriverCurrentLocation>() {
            @Override
            public void onResponse(Call<GetDriverCurrentLocation> call, Response<GetDriverCurrentLocation> response) {
                if (response.body() != null) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        getDriverCurrentLocationMutableLiveData.setValue(response.body());
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getDriverCurrentLocationLiveData(activity, jobID);
                            }
                        }, 10000);
                    }
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getDriverCurrentLocationLiveData(activity, jobID);
                        }
                    }, 10000);
                }
            }

            @Override
            public void onFailure(Call<GetDriverCurrentLocation> call, Throwable t) {
                GetDriverCurrentLocation getDriverCurrentLocation = new GetDriverCurrentLocation();
                getDriverCurrentLocation.setSuccess("0");
                getDriverCurrentLocation.setMessage("Please Check Internet Connection");
                getDriverCurrentLocationMutableLiveData.setValue(getDriverCurrentLocation);

            }
        });

        return getDriverCurrentLocationMutableLiveData;
    }

    public LiveData<Map> cancleJob(Activity activity, String userId, String reason) {
        cancle = new MutableLiveData<>();

        Api api = ApiClient.getApiClient().create(Api.class);
        api.cancleJob(userId, reason).enqueue(new Callback<Map>() {
            @Override
            public void onResponse(Call<Map> call, Response<Map> response) {
                if (response.body() != null) {
                    cancle.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Map> call, Throwable t) {

            }
        });
        return cancle;
    }


    public LiveData<Map> Rating(final Activity activity, String userId, String jobId, String driverId, String comments, String rating) {

        if (CommonUtils.isNetworkConnected(activity)) {

            DriverRating = new MutableLiveData<>();
            CommonUtils.showProgress(activity);
            Api api = ApiClient.getApiClient().create(Api.class);

            api.driverRating(jobId, driverId, userId, comments, rating).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        DriverRating.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Toast.makeText(activity, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }

        return DriverRating;
    }


}
