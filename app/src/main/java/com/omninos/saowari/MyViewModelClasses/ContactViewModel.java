package com.omninos.saowari.MyViewModelClasses;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import com.omninos.saowari.ModelClass.GetContactNumber;
import com.omninos.saowari.Retrofit.Api;
import com.omninos.saowari.Retrofit.ApiClient;
import com.omninos.saowari.Util.CommonUtils;

import org.json.JSONArray;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactViewModel extends ViewModel {

    private MutableLiveData<GetContactNumber> getContactNumberMutableLiveData;

    private MutableLiveData<Map> saveData;

    private MutableLiveData<Map> callDetail;


    public LiveData<GetContactNumber> getContactNumberLiveData(final Activity activity, String UserId) {
        getContactNumberMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

            CommonUtils.showProgress(activity);

            Api api = ApiClient.getApiClient().create(Api.class);

            api.getNumber(UserId).enqueue(new Callback<GetContactNumber>() {
                @Override
                public void onResponse(Call<GetContactNumber> call, Response<GetContactNumber> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        getContactNumberMutableLiveData.setValue(response.body());
                    } else {
                        GetContactNumber getContactNumber = new GetContactNumber();
                        getContactNumber.setSuccess("0");
                        getContactNumber.setMessage("Server Error");
                        getContactNumberMutableLiveData.setValue(getContactNumber);
                    }
                }

                @Override
                public void onFailure(Call<GetContactNumber> call, Throwable t) {
                    CommonUtils.dismissProgress();

                    GetContactNumber getContactNumber = new GetContactNumber();
                    getContactNumber.setSuccess("0");
                    getContactNumber.setMessage("Server Error");
                    getContactNumberMutableLiveData.setValue(getContactNumber);
                }
            });

        } else {

            GetContactNumber getContactNumber = new GetContactNumber();
            getContactNumber.setSuccess("0");
            getContactNumber.setMessage("Please Check Internet Connection");
            getContactNumberMutableLiveData.setValue(getContactNumber);
        }

        return getContactNumberMutableLiveData;
    }

    public LiveData<Map> saveContactList(final Activity activity, String Userid, JSONArray jsonArray) {
        saveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {


            CommonUtils.showProgress(activity);

            Api api = ApiClient.getApiClient().create(Api.class);
            api.saveContact(Userid, jsonArray).enqueue(new Callback<Map>() {
                @Override
                public void onResponse(Call<Map> call, Response<Map> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        saveData.setValue(response.body());
                    }
                }

                @Override
                public void onFailure(Call<Map> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    Toast.makeText(activity, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return saveData;
    }

    public LiveData<Map> Calls(Activity activity, String userId, String driverId, String startTime, String endTime) {

        callDetail = new MutableLiveData<>();

        Api api = ApiClient.getApiClient().create(Api.class);

        api.addCall(userId, driverId, "0", startTime, endTime).enqueue(new Callback<Map>() {
            @Override
            public void onResponse(Call<Map> call, Response<Map> response) {
                if (response.body() != null) {
                    callDetail.setValue(response.body());
                } else {

                }
            }

            @Override
            public void onFailure(Call<Map> call, Throwable t) {

            }
        });

        return callDetail;
    }

}
