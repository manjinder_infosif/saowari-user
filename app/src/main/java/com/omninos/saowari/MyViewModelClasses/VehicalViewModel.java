package com.omninos.saowari.MyViewModelClasses;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.widget.Toast;

import com.omninos.saowari.ModelClass.VehicleTypePojo;
import com.omninos.saowari.Retrofit.Api;
import com.omninos.saowari.Retrofit.ApiClient;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.directionApi.DirectionPojo;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VehicalViewModel extends ViewModel {

    private MutableLiveData<DirectionPojo> directionPojoMutableLiveData;

    public LiveData<DirectionPojo> getRoute(final Activity activity, Map<String, String> data) {
        directionPojoMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

//            CommonUtils.showProgress(activity,"");
            Api api = ApiClient.getClientRoute().create(Api.class);

            api.getPolyLine(data).enqueue(new Callback<DirectionPojo>() {
                @Override
                public void onResponse(Call<DirectionPojo> call, Response<DirectionPojo> response) {
//                    CommonUtils.dismiss();
                    if (response.isSuccessful()) {
                        directionPojoMutableLiveData.setValue(response.body());
                    } else {

                    }
                }

                @Override
                public void onFailure(Call<DirectionPojo> call, Throwable t) {
//                    CommonUtils.dismiss();
                    Toast.makeText(activity, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });

        } else {
            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
        }
        return directionPojoMutableLiveData;
    }


    private MutableLiveData<VehicleTypePojo> getvehicle;

    public LiveData<VehicleTypePojo> vehicleTypePojoLiveData(Activity activity) {
        getvehicle = new MutableLiveData<>();

        if (CommonUtils.isNetworkConnected(activity)) {

            Api api = ApiClient.getApiClient().create(Api.class);
            api.vehicleDetails().enqueue(new Callback<VehicleTypePojo>() {
                @Override
                public void onResponse(Call<VehicleTypePojo> call, Response<VehicleTypePojo> response) {
                    if (response.body() != null) {
                        getvehicle.setValue(response.body());
                    } else {
                        VehicleTypePojo vehicleTypePojo = new VehicleTypePojo();
                        vehicleTypePojo.setSuccess("0");
                        vehicleTypePojo.setMessage("Server Error");
                        getvehicle.setValue(vehicleTypePojo);
                    }
                }

                @Override
                public void onFailure(Call<VehicleTypePojo> call, Throwable t) {
                    VehicleTypePojo vehicleTypePojo = new VehicleTypePojo();
                    vehicleTypePojo.setSuccess("0");
                    vehicleTypePojo.setMessage("Server Error");
                    getvehicle.setValue(vehicleTypePojo);
                }
            });
        } else {
            VehicleTypePojo vehicleTypePojo = new VehicleTypePojo();
            vehicleTypePojo.setSuccess("0");
            vehicleTypePojo.setMessage("Check Internet Connection");
            getvehicle.setValue(vehicleTypePojo);
        }

        return getvehicle;
    }

}
