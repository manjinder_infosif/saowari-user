package com.omninos.saowari.MyViewModelClasses;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.omninos.saowari.ModelClass.NearByDriverModel;
import com.omninos.saowari.ModelClass.NearByDriverModel1;
import com.omninos.saowari.Retrofit.Api;
import com.omninos.saowari.Retrofit.ApiClient;
import com.omninos.saowari.Util.CommonUtils;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NearDriverViewModel extends ViewModel {

    private MutableLiveData<NearByDriverModel1> nearByDriverModelMutableLiveData;

    public LiveData<NearByDriverModel1> near(Activity activity, String vehicleId, String latitude, String longitude) {
        nearByDriverModelMutableLiveData = new MutableLiveData<>();

        if (CommonUtils.isNetworkConnected(activity)) {
            Api api = ApiClient.getApiClient().create(Api.class);
            api.nearDriver(vehicleId, latitude, longitude).enqueue(new Callback<NearByDriverModel1>() {
                @Override
                public void onResponse(Call<NearByDriverModel1> call, Response<NearByDriverModel1> response) {
                    if (response.body() != null) {
                        nearByDriverModelMutableLiveData.setValue(response.body());
                    } else {
                        NearByDriverModel1 driverModel = new NearByDriverModel1();
                        driverModel.setSuccess("0");
                        driverModel.setMessage("Server Error");
                        nearByDriverModelMutableLiveData.setValue(driverModel);
                    }
                }

                @Override
                public void onFailure(Call<NearByDriverModel1> call, Throwable t) {

                    NearByDriverModel1 driverModel = new NearByDriverModel1();
                    driverModel.setSuccess("0");
                    driverModel.setMessage("Server Error");
                    nearByDriverModelMutableLiveData.setValue(driverModel);
                }
            });
        } else {

//            NearByDriverModel driverModel = new NearByDriverModel();
//            driverModel.setSuccess("0");
//            driverModel.setMessage("Please check Internet Connection");
//            nearByDriverModelMutableLiveData.setValue(driverModel);
        }

        return nearByDriverModelMutableLiveData;
    }

    private MutableLiveData<Map> nearData;

    public LiveData<Map> nearNewData(Activity activity, String vehicleId, String latitude, String longitude){

        nearData=new MutableLiveData<>();

        Api api = ApiClient.getApiClient().create(Api.class);

        api.nearDriver1(vehicleId, latitude, longitude).enqueue(new Callback<Map>() {
            @Override
            public void onResponse(Call<Map> call, Response<Map> response) {
                nearData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Map> call, Throwable t) {

            }
        });
        return nearData;
    }
}
