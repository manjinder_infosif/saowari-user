package com.omninos.saowari.MyViewModelClasses;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.omninos.saowari.ModelClass.MyRidesModel;
import com.omninos.saowari.Retrofit.Api;
import com.omninos.saowari.Retrofit.ApiClient;
import com.omninos.saowari.Util.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyRideViewModel extends ViewModel {

    private MutableLiveData<MyRidesModel> myRidesModelMutableLiveData;

    public LiveData<MyRidesModel> modelLiveData(Activity activity, String userId) {
        myRidesModelMutableLiveData = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

            CommonUtils.showProgress(activity);
            Api api = ApiClient.getApiClient().create(Api.class);
            api.myRide(userId).enqueue(new Callback<MyRidesModel>() {
                @Override
                public void onResponse(Call<MyRidesModel> call, Response<MyRidesModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        myRidesModelMutableLiveData.setValue(response.body());
                    } else {
                        MyRidesModel myRidesModel = new MyRidesModel();
                        myRidesModel.setSuccess("0");
                        myRidesModel.setMessage("Server Error");
                        myRidesModelMutableLiveData.setValue(myRidesModel);
                    }
                }

                @Override
                public void onFailure(Call<MyRidesModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    MyRidesModel myRidesModel = new MyRidesModel();
                    myRidesModel.setSuccess("0");
                    myRidesModel.setMessage("Server Error");
                    myRidesModelMutableLiveData.setValue(myRidesModel);
                }
            });
        } else {

            MyRidesModel myRidesModel = new MyRidesModel();
            myRidesModel.setSuccess("0");
            myRidesModel.setMessage("Please Check Internet Connection");
            myRidesModelMutableLiveData.setValue(myRidesModel);
        }
        return myRidesModelMutableLiveData;
    }
}
