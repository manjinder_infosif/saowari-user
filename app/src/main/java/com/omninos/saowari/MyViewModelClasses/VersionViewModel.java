package com.omninos.saowari.MyViewModelClasses;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.omninos.saowari.ModelClass.CheckUserJob;
import com.omninos.saowari.ModelClass.VersionModel;
import com.omninos.saowari.Retrofit.Api;
import com.omninos.saowari.Retrofit.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VersionViewModel extends ViewModel {

    private MutableLiveData<VersionModel> versionModelMutableLiveData;

    private MutableLiveData<CheckUserJob> checkUserJobMutableLiveData;


    public LiveData<VersionModel> modelLiveData(Activity activity) {

        versionModelMutableLiveData = new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);

        api.version("user").enqueue(new Callback<VersionModel>() {
            @Override
            public void onResponse(Call<VersionModel> call, Response<VersionModel> response) {
                if (response.body() != null) {
                    versionModelMutableLiveData.setValue(response.body());
                } else {

                }
            }

            @Override
            public void onFailure(Call<VersionModel> call, Throwable t) {

            }
        });

        return versionModelMutableLiveData;
    }


    public LiveData<CheckUserJob> checkJobUser(Activity activity, String userId) {
        checkUserJobMutableLiveData = new MutableLiveData<>();

        Api api = ApiClient.getApiClient().create(Api.class);
        api.checkJob(userId).enqueue(new Callback<CheckUserJob>() {
            @Override
            public void onResponse(Call<CheckUserJob> call, Response<CheckUserJob> response) {
                if (response.body() != null) {
                    checkUserJobMutableLiveData.setValue(response.body());
                } else {

                }
            }

            @Override
            public void onFailure(Call<CheckUserJob> call, Throwable t) {

            }
        });
        return checkUserJobMutableLiveData;
    }
}
