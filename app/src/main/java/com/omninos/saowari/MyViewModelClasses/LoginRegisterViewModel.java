package com.omninos.saowari.MyViewModelClasses;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.omninos.saowari.ModelClass.LoginRegisterModel;
import com.omninos.saowari.Retrofit.Api;
import com.omninos.saowari.Retrofit.ApiClient;
import com.omninos.saowari.Util.CommonUtils;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginRegisterViewModel extends ViewModel {
    private MutableLiveData<LoginRegisterModel> userLogin;

    private MutableLiveData<LoginRegisterModel> userRegister;

    private MutableLiveData<LoginRegisterModel> update;

    private MutableLiveData<Map> userLogout;

    public LiveData<LoginRegisterModel> Login(Activity activity, String phone, String latitude, String longitude, String reg_id, String device_type, String login_type) {
        userLogin = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

            CommonUtils.showProgress(activity);
            Api api = ApiClient.getApiClient().create(Api.class);

            api.userLogin(phone, latitude, longitude, reg_id, device_type, login_type).enqueue(new Callback<LoginRegisterModel>() {
                @Override
                public void onResponse(Call<LoginRegisterModel> call, Response<LoginRegisterModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        userLogin.setValue(response.body());
                    } else {
                        LoginRegisterModel loginRegisterModel = new LoginRegisterModel();
                        loginRegisterModel.setSuccess("0");
                        loginRegisterModel.setMessage("Server Error");
                        userLogin.setValue(loginRegisterModel);
                    }
                }

                @Override
                public void onFailure(Call<LoginRegisterModel> call, Throwable t) {
                    CommonUtils.dismissProgress();

                    LoginRegisterModel loginRegisterModel = new LoginRegisterModel();
                    loginRegisterModel.setSuccess("0");
                    loginRegisterModel.setMessage("Server Error");
                    userLogin.setValue(loginRegisterModel);
                }
            });

        } else {
            LoginRegisterModel loginRegisterModel = new LoginRegisterModel();
            loginRegisterModel.setSuccess("0");
            loginRegisterModel.setMessage("Check Internet Connection");
            userLogin.setValue(loginRegisterModel);
        }

        return userLogin;
    }

    public LiveData<LoginRegisterModel> Register(Activity activity, RequestBody name, RequestBody phone, RequestBody email, RequestBody dateOfBirth, RequestBody gender, RequestBody device_type, RequestBody reg_id, RequestBody latitude, RequestBody longitude, RequestBody login_type, MultipartBody.Part image) {
        userRegister = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

            Api api = ApiClient.getApiClient().create(Api.class);
            CommonUtils.showProgress(activity);

            api.userRegister(name, phone, email, dateOfBirth, gender, device_type, reg_id, latitude, longitude, login_type, image).enqueue(new Callback<LoginRegisterModel>() {
                @Override
                public void onResponse(Call<LoginRegisterModel> call, Response<LoginRegisterModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        userRegister.setValue(response.body());
                    } else {

                        LoginRegisterModel loginRegisterModel = new LoginRegisterModel();
                        loginRegisterModel.setSuccess("0");
                        loginRegisterModel.setMessage("Server Error");
                        userRegister.setValue(loginRegisterModel);
                    }
                }

                @Override
                public void onFailure(Call<LoginRegisterModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    LoginRegisterModel loginRegisterModel = new LoginRegisterModel();
                    loginRegisterModel.setSuccess("0");
                    loginRegisterModel.setMessage("Server Error");
                    userRegister.setValue(loginRegisterModel);
                }
            });

        } else {
            LoginRegisterModel loginRegisterModel = new LoginRegisterModel();
            loginRegisterModel.setSuccess("0");
            loginRegisterModel.setMessage("Check Internet Connection");
            userRegister.setValue(loginRegisterModel);
        }

        return userRegister;
    }

    public LiveData<LoginRegisterModel> updateProfileLive(Activity activity, final RequestBody name, RequestBody userId, RequestBody phone, RequestBody latitude, RequestBody longitude, RequestBody email, MultipartBody.Part image) {
        update = new MutableLiveData<>();
        if (CommonUtils.isNetworkConnected(activity)) {

            CommonUtils.showProgress(activity);
            Api api = ApiClient.getApiClient().create(Api.class);

            api.updateProfile(name, userId, phone, latitude, longitude, email, image).enqueue(new Callback<LoginRegisterModel>() {
                @Override
                public void onResponse(Call<LoginRegisterModel> call, Response<LoginRegisterModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        update.setValue(response.body());
                    } else {
                        LoginRegisterModel loginRegisterModel = new LoginRegisterModel();
                        loginRegisterModel.setMessage("Server Error");
                        loginRegisterModel.setSuccess("0");
                        update.setValue(loginRegisterModel);
                    }
                }

                @Override
                public void onFailure(Call<LoginRegisterModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    LoginRegisterModel loginRegisterModel = new LoginRegisterModel();
                    loginRegisterModel.setMessage("Server Error");
                    loginRegisterModel.setSuccess("0");
                    update.setValue(loginRegisterModel);
                }
            });
        } else {
            LoginRegisterModel loginRegisterModel = new LoginRegisterModel();
            loginRegisterModel.setMessage("Please check Internet Connection");
            loginRegisterModel.setSuccess("0");
            update.setValue(loginRegisterModel);
        }

        return update;
    }


    public LiveData<Map> LogOut(Activity activity, String userId) {
        userLogout = new MutableLiveData<>();
        CommonUtils.showProgress(activity);
        Api api = ApiClient.getApiClient().create(Api.class);
        api.logOut(userId).enqueue(new Callback<Map>() {
            @Override
            public void onResponse(Call<Map> call, Response<Map> response) {
                CommonUtils.dismissProgress();
                if (response.body() != null) {
                    userLogout.setValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<Map> call, Throwable t) {
                CommonUtils.dismissProgress();

            }
        });
        return userLogout;
    }
}
