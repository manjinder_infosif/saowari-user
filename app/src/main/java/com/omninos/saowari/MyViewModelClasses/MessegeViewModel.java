package com.omninos.saowari.MyViewModelClasses;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;


import com.omninos.saowari.ModelClass.GetCallListModel;
import com.omninos.saowari.ModelClass.GetConversionModel;
import com.omninos.saowari.ModelClass.MessageInboxModel;
import com.omninos.saowari.ModelClass.SendMessageModel;
import com.omninos.saowari.Retrofit.Api;
import com.omninos.saowari.Retrofit.ApiClient;
import com.omninos.saowari.Util.CommonUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessegeViewModel extends ViewModel {
    private MutableLiveData<MessageInboxModel> messageInboxModelMutableLiveData;

    private MutableLiveData<GetConversionModel> getConversionModelMutableLiveData;

    private MutableLiveData<SendMessageModel> sendMessage;

    private MutableLiveData<GetCallListModel> getCallListModelMutableLiveData;


    public LiveData<MessageInboxModel> messageInboxModelLiveData(Activity activity, String sender_id) {
        messageInboxModelMutableLiveData = new MutableLiveData<>();

        if (CommonUtils.isNetworkConnected(activity)) {
            CommonUtils.showProgress(activity);
            Api api = ApiClient.getApiClient().create(Api.class);
            api.getInbox(sender_id, "user").enqueue(new Callback<MessageInboxModel>() {
                @Override
                public void onResponse(Call<MessageInboxModel> call, Response<MessageInboxModel> response) {
                    CommonUtils.dismissProgress();
                    if (response.body() != null) {
                        messageInboxModelMutableLiveData.setValue(response.body());
                    } else {
                        MessageInboxModel messageInboxModel = new MessageInboxModel();
                        messageInboxModel.setMessage("Server Error");
                        messageInboxModel.setSuccess("0");
                        messageInboxModelMutableLiveData.setValue(messageInboxModel);
                    }
                }

                @Override
                public void onFailure(Call<MessageInboxModel> call, Throwable t) {
                    CommonUtils.dismissProgress();
                    MessageInboxModel messageInboxModel = new MessageInboxModel();
                    messageInboxModel.setMessage("Server Error");
                    messageInboxModel.setSuccess("0");
                    messageInboxModelMutableLiveData.setValue(messageInboxModel);
                }
            });
        } else {
            CommonUtils.dismissProgress();
            MessageInboxModel messageInboxModel = new MessageInboxModel();
            messageInboxModel.setMessage("Server Error");
            messageInboxModel.setSuccess("0");
            messageInboxModelMutableLiveData.setValue(messageInboxModel);
        }


        return messageInboxModelMutableLiveData;
    }

    public LiveData<GetConversionModel> getConversionModelLiveData(Activity activity, String sender_id, String reciver_id) {
        getConversionModelMutableLiveData = new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);
        api.getConversion(sender_id, reciver_id, "user").enqueue(new Callback<GetConversionModel>() {
            @Override
            public void onResponse(Call<GetConversionModel> call, Response<GetConversionModel> response) {
                if (response.body() != null) {
                    getConversionModelMutableLiveData.setValue(response.body());
                } else {
                    GetConversionModel getConversionModel = new GetConversionModel();
                    getConversionModel.setMessage("Server Error");
                    getConversionModel.setSuccess("0");
                    getConversionModelMutableLiveData.setValue(getConversionModel);
                }
            }

            @Override
            public void onFailure(Call<GetConversionModel> call, Throwable t) {
                GetConversionModel getConversionModel = new GetConversionModel();
                getConversionModel.setMessage("Server Error");
                getConversionModel.setSuccess("0");
                getConversionModelMutableLiveData.setValue(getConversionModel);
            }
        });
        return getConversionModelMutableLiveData;
    }


    public LiveData<SendMessageModel> message(Activity activity, String sender_id, String receiver_id, String messageData) {
        sendMessage = new MutableLiveData<>();

        Api api = ApiClient.getApiClient().create(Api.class);
        api.sendMessage(sender_id, receiver_id, messageData, "user").enqueue(new Callback<SendMessageModel>() {
            @Override
            public void onResponse(Call<SendMessageModel> call, Response<SendMessageModel> response) {
                if (response.body() != null) {
                    sendMessage.setValue(response.body());
                } else {
                    SendMessageModel sendMessageModel = new SendMessageModel();
                    sendMessageModel.setSuccess("0");
                    sendMessageModel.setMessage("Server Error");
                    sendMessage.setValue(sendMessageModel);
                }
            }

            @Override
            public void onFailure(Call<SendMessageModel> call, Throwable t) {

                SendMessageModel sendMessageModel = new SendMessageModel();
                sendMessageModel.setSuccess("0");
                sendMessageModel.setMessage("Server Error");
                sendMessage.setValue(sendMessageModel);
            }
        });

        return sendMessage;
    }


    public LiveData<GetCallListModel> getCallListModelLiveData(Activity activity, String userId) {

        getCallListModelMutableLiveData = new MutableLiveData<>();
        Api api = ApiClient.getApiClient().create(Api.class);

        api.getCallList(userId).enqueue(new Callback<GetCallListModel>() {
            @Override
            public void onResponse(Call<GetCallListModel> call, Response<GetCallListModel> response) {
                if (response.body() != null) {
                    getCallListModelMutableLiveData.setValue(response.body());
                } else {
                    GetCallListModel getCallListModel = new GetCallListModel();
                    getCallListModel.setMessage("Server Error");
                    getCallListModel.setSuccess("0");
                    getCallListModelMutableLiveData.setValue(getCallListModel);
                }
            }

            @Override
            public void onFailure(Call<GetCallListModel> call, Throwable t) {
                GetCallListModel getCallListModel = new GetCallListModel();
                getCallListModel.setMessage("Server Error");
                getCallListModel.setSuccess("0");
                getCallListModelMutableLiveData.setValue(getCallListModel);
            }
        });
        return getCallListModelMutableLiveData;
    }
}
