package com.omninos.saowari.activities;

import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.saowari.ModelClass.MyRidesModel;
import com.omninos.saowari.MyViewModelClasses.MyRideViewModel;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.adapters.MyRideAdapter;
import com.omninos.saowari.servicess.MySerives;

import java.util.ArrayList;
import java.util.List;

public class MyRidesActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private MyRideAdapter adapter;
    private ImageView firstIcon;
    private TextView title, noDetailFound;
    private MyRideViewModel viewModel;
    private List<MyRidesModel.Detail> list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_rides);

        stopService(new Intent(MyRidesActivity.this, MySerives.class));
        Intent intent = new Intent(MyRidesActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        viewModel = ViewModelProviders.of(this).get(MyRideViewModel.class);
        initView();
        SetUp();

        getRideList();
    }

    private void getRideList() {
        viewModel.modelLiveData(MyRidesActivity.this, App.getAppPreference().GetString(ConstantData.USERID)).observe(MyRidesActivity.this, new Observer<MyRidesModel>() {
            @Override
            public void onChanged(@Nullable MyRidesModel myRidesModel) {
                if (myRidesModel.getSuccess().equalsIgnoreCase("1")) {
                    recyclerView.setVisibility(View.VISIBLE);
                    noDetailFound.setVisibility(View.GONE);
                    if (myRidesModel.getDetails() != null) {
                        list = myRidesModel.getDetails();

                        adapter = new MyRideAdapter(MyRidesActivity.this, list);
                        recyclerView.setAdapter(adapter);

                    }
                } else {
                    noDetailFound.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    CommonUtils.showSnackbarAlert(firstIcon, myRidesModel.getMessage());
                }
            }
        });
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
        firstIcon = findViewById(R.id.firstIcon);
        title = findViewById(R.id.title);
        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        title.setText("My Rides");

        noDetailFound = findViewById(R.id.noDetailFound);
    }

    private void SetUp() {
        firstIcon.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstIcon:
                onBackPressed();
                break;
        }
    }
}
