package com.omninos.saowari.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;
import com.omninos.saowari.ModelClass.LoginRegisterModel;
import com.omninos.saowari.MyViewModelClasses.LoginRegisterViewModel;
import com.omninos.saowari.R;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.servicess.LocationService;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private Button nextButton;
    private LoginActivity activity;

    public static int APP_REQUEST_CODE = 99;
    private String MobileNumber;
    private LoginRegisterViewModel viewModel;


    private LocationManager locationManager;
    private GoogleApiClient client;
    private String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        activity = LoginActivity.this;

        viewModel = ViewModelProviders.of(this).get(LoginRegisterViewModel.class);


        token = FirebaseInstanceId.getInstance().getToken();
        if (Build.VERSION.SDK_INT > 23) {
            if (App.getSinltonPojo().getCurrentLatlng() != null) {

            } else {
                stopService(new Intent(activity, LocationService.class));
                Intent intent = new Intent(activity, LocationService.class);
                if (!isMyServiceRunning(intent.getClass())) {
                    startService(intent);
                }
            }
        } else {
            getLocation();
        }


        initView();
        SetUp();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void initView() {
        nextButton = findViewById(R.id.nextButton);
    }

    private void SetUp() {
        nextButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextButton:
                if (App.getSinltonPojo().getCurrentLatlng() != null) {
                    AccountKitLogin();
                } else {
                    CommonUtils.showSnackbarAlert(nextButton, "Please enable Location");
                }

                break;
        }
    }

    private void AccountKitLogin() {
        final Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == APP_REQUEST_CODE) {
            AccountKitLoginResult loginResult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);

            if (loginResult.getError() != null) {
                Toast.makeText(this, loginResult.getError().getErrorType().getMessage(), Toast.LENGTH_SHORT).show();
            } else if (loginResult.wasCancelled()) {
                Toast.makeText(this, "Login Cancelled", Toast.LENGTH_SHORT).show();
            } else {
                if (loginResult.getAccessToken() != null) {
                    AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                        @Override
                        public void onSuccess(Account account) {
                            MobileNumber = account.getPhoneNumber().toString();
                            System.out.println("MobileNumber: " + MobileNumber);
                            CheckUserLogin(MobileNumber);
                        }

                        @Override
                        public void onError(AccountKitError accountKitError) {

                        }
                    });

                } else {
                    Toast.makeText(this, loginResult.getAuthorizationCode(), Toast.LENGTH_SHORT).show();

                }
            }
        }
    }

    private void CheckUserLogin(final String mobileNumber) {
        viewModel.Login(activity, mobileNumber, String.valueOf(App.getSinltonPojo().getCurrentLatlng().latitude), String.valueOf(App.getSinltonPojo().getCurrentLatlng().longitude), token, "android", "normal").observe(activity, new Observer<LoginRegisterModel>() {
            @Override
            public void onChanged(@Nullable LoginRegisterModel loginRegisterModel) {
                if (loginRegisterModel.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().saveLoginDetail(loginRegisterModel);
                    App.getAppPreference().SaveString(ConstantData.USERID,loginRegisterModel.getDetails().getId());
                    App.getAppPreference().SaveString(ConstantData.TOKEN, "1");
                    startActivity(new Intent(activity, MainActivity.class));
                    finishAffinity();
                } else if (loginRegisterModel.getSuccess().equalsIgnoreCase("2")) {
                    App.getSinltonPojo().setMobilenumber(mobileNumber);
                    startActivity(new Intent(activity, SignUpActivity.class));
                } else {
                    CommonUtils.showSnackbarAlert(nextButton, loginRegisterModel.getMessage());
                }
            }
        });
    }


    private void getLocation() {
//        App.getSinltonPojo().setCurrentLatlng(new LatLng(31.635,76.7353));
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 500, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                    App.getSinltonPojo().setCurrentLatlng(new LatLng(location.getLatitude(), location.getLongitude()));
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        } else if (locationManager.isProviderEnabled(locationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 500, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    App.getSinltonPojo().setCurrentLatlng(new LatLng(location.getLatitude(), location.getLongitude()));
                    Geocoder geocoder;
                    List<Address> addresses;
                    geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }
}
