package com.omninos.saowari.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.servicess.MySerives;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {


    private CardView helpCard, contactUs,emergencyContact;
    private SettingActivity activity;

    private ImageView firstIcon;
    private TextView title, userName, userAddress;
    private CircleImageView user_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        activity = SettingActivity.this;

        stopService(new Intent(SettingActivity.this, MySerives.class));
        Intent intent = new Intent(SettingActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        initView();
        SetUps();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void initView() {
        helpCard = findViewById(R.id.helpCard);
        contactUs = findViewById(R.id.contactUs);

        firstIcon = findViewById(R.id.firstIcon);
        title = findViewById(R.id.title);
        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        title.setText("Settings");

        user_image = findViewById(R.id.user_image);
        userName = findViewById(R.id.userName);
        userAddress = findViewById(R.id.userAddress);

        emergencyContact=findViewById(R.id.emergencyContact);

        Glide.with(SettingActivity.this).load(App.getAppPreference().getLoginDetail().getDetails().getImage()).into(user_image);
        userName.setText(App.getAppPreference().getLoginDetail().getDetails().getName());


        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(Double.parseDouble(App.getAppPreference().getLoginDetail().getDetails().getLatitude()), Double.parseDouble(App.getAppPreference().getLoginDetail().getDetails().getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            userAddress.setText(address);

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void SetUps() {
        helpCard.setOnClickListener(this);
        contactUs.setOnClickListener(this);

        firstIcon.setOnClickListener(this);
        emergencyContact.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.helpCard:
                startActivity(new Intent(activity, HelpActivity.class));
                break;

            case R.id.contactUs:
                startActivity(new Intent(activity, ContactUsActivity.class));
                break;

            case R.id.firstIcon:
                onBackPressed();
                break;

            case R.id.emergencyContact:
                startActivity(new Intent(SettingActivity.this,EmergencyContactActivity.class));
                break;
        }
    }
}
