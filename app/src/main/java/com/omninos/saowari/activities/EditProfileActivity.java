package com.omninos.saowari.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowari.BuildConfig;
import com.omninos.saowari.ModelClass.LoginRegisterModel;
import com.omninos.saowari.MyViewModelClasses.LoginRegisterViewModel;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.Util.ImageUtil;
import com.omninos.saowari.servicess.MySerives;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView firstIcon;
    private TextView title;
    private CircleImageView imageEditP, addImageBtnEditP;
    private EditText nameEditP, addressEditP, phoneEditP, emailEditP;
    private EditProfileActivity activity;
    private String name, address, phone, email;

    private File photoFile;
    private static final int GALLERY_REQUEST = 101;

    private static final int CAMERA_REQUEST = 102;

    private Uri uri;
    private String UserimagePath = "", Latitude = "", Longitude = "";
    private Button saveEditProfileBtn;
    private LoginRegisterViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        activity = EditProfileActivity.this;

        stopService(new Intent(EditProfileActivity.this, MySerives.class));
        Intent intent = new Intent(EditProfileActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        viewModel = ViewModelProviders.of(this).get(LoginRegisterViewModel.class);

        initView();
        SetUp();

    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void initView() {
        firstIcon = findViewById(R.id.firstIcon);
        title = findViewById(R.id.title);

        imageEditP = findViewById(R.id.imageEditP);
        addImageBtnEditP = findViewById(R.id.addImageBtnEditP);
        nameEditP = findViewById(R.id.nameEditP);
        addressEditP = findViewById(R.id.addressEditP);
        phoneEditP = findViewById(R.id.phoneEditP);
        emailEditP = findViewById(R.id.emailEditP);

        saveEditProfileBtn = findViewById(R.id.saveEditProfileBtn);
    }

    private void SetUp() {
        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        firstIcon.setOnClickListener(this);
        title.setText("Edit Profile");

        Glide.with(EditProfileActivity.this).load(App.getAppPreference().getLoginDetail().getDetails().getImage()).into(imageEditP);

        nameEditP.setText(App.getAppPreference().getLoginDetail().getDetails().getName());
        phoneEditP.setText(App.getAppPreference().getLoginDetail().getDetails().getPhone());
        emailEditP.setText(App.getAppPreference().getLoginDetail().getDetails().getEmail());
        Latitude = App.getAppPreference().getLoginDetail().getDetails().getLatitude();
        Longitude = App.getAppPreference().getLoginDetail().getDetails().getLongitude();

        addressEditP.setFocusable(false);
        addressEditP.setOnClickListener(this);

        imageEditP.setOnClickListener(this);
        saveEditProfileBtn.setOnClickListener(this);


        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(Double.parseDouble(App.getAppPreference().getLoginDetail().getDetails().getLatitude()), Double.parseDouble(App.getAppPreference().getLoginDetail().getDetails().getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            addressEditP.setText(address);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstIcon:
                onBackPressed();
                break;

            case R.id.addressEditP:
                Intent intent = new Intent(EditProfileActivity.this, SearchPlaceActivity.class);
                intent.putExtra("Type", "3");
                startActivity(intent);
                break;

            case R.id.imageEditP:
                OpenImageData();
                break;

            case R.id.saveEditProfileBtn:
                UpdateProfile();
                break;
        }
    }

    private void UpdateProfile() {

         MultipartBody.Part user_image;

        name = nameEditP.getText().toString();
        address = addressEditP.getText().toString();
        phone = phoneEditP.getText().toString();
        email = emailEditP.getText().toString();
        if (name.isEmpty()) {
            CommonUtils.showSnackbarAlert(nameEditP, "enter name");
        } else if (address.isEmpty()) {
            CommonUtils.showSnackbarAlert(addImageBtnEditP, "enter address");
        } else if (phone.isEmpty()) {
            CommonUtils.showSnackbarAlert(phoneEditP, "enter phone number");
        } else if (email.isEmpty()) {
            CommonUtils.showSnackbarAlert(emailEditP, "enter email");
        } else {


            RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), name);
            RequestBody addressBody = RequestBody.create(MediaType.parse("text/plain"), address);
            RequestBody phoneBody = RequestBody.create(MediaType.parse("text/plain"), phone);
            RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), email);
            RequestBody latitudeBody = RequestBody.create(MediaType.parse("text/plain"), Latitude);
            RequestBody longitudeBody = RequestBody.create(MediaType.parse("text/plain"), Longitude);
            RequestBody useridBody = RequestBody.create(MediaType.parse("text/plain"), App.getAppPreference().GetString(ConstantData.USERID));
            if (!UserimagePath.isEmpty()) {
                File file = new File(UserimagePath);
                final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                user_image = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
            } else {
//                File file = new File(App.getAppPreferences().getUserImagePath(activity));
                final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "");
                user_image = MultipartBody.Part.createFormData("image", "", requestFile);
            }

            viewModel.updateProfileLive(EditProfileActivity.this, nameBody, useridBody, phoneBody, latitudeBody, longitudeBody, emailBody, user_image).observe(EditProfileActivity.this, new Observer<LoginRegisterModel>() {
                @Override
                public void onChanged(@Nullable LoginRegisterModel loginRegisterModel) {
                    if (loginRegisterModel.getSuccess().equalsIgnoreCase("1")) {
                        App.getAppPreference().saveLoginDetail(loginRegisterModel);
                        onBackPressed();
                    } else {
                        CommonUtils.showSnackbarAlert(emailEditP, loginRegisterModel.getMessage());
                    }
                }
            });

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (App.getSinltonPojo().getEditAddress() != null) {
            addressEditP.setText(App.getSinltonPojo().getEditAddress());
            Latitude=App.getSinltonPojo().getEditLat();
            Longitude=App.getSinltonPojo().getEditlng();
        }
    }


    private void OpenImageData() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            photoFile = null;
            photoFile = ImageUtil.getTemporaryCameraFile();
            if (photoFile != null) {
                Uri uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private void galleryIntent() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {
                UserimagePath = ImageUtil.compressImage(photoFile.getAbsolutePath());
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap mBitmapInsurance = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
//                userImage.setImageBitmap(mBitmapInsurance);
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap rotated = Bitmap.createBitmap(mBitmapInsurance, 0, 0, mBitmapInsurance.getWidth(), mBitmapInsurance.getHeight(),
                        matrix, true);

                if (Build.VERSION.SDK_INT > 25 && Build.VERSION.SDK_INT < 27) {
                    // Do something for lollipop and above versions
                    imageEditP.setImageBitmap(rotated);
                } else {
                    // do something for phones running an SDK before lollipop
                    imageEditP.setImageBitmap(mBitmapInsurance);
                }
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
                Glide.with(activity).load("file://" + UserimagePath).into(imageEditP);
            }

        }
    }
}
