package com.omninos.saowari.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.omninos.saowari.MyViewModelClasses.VehicalViewModel;
import com.omninos.saowari.R;
import com.infideap.drawerbehavior.AdvanceDrawerLayout;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.adapters.VehicleDetailAdapter;
import com.omninos.saowari.directionApi.DirectionPojo;
import com.omninos.saowari.directionApi.Leg;
import com.omninos.saowari.directionApi.Route;
import com.omninos.saowari.directionApi.Step;
import com.omninos.saowari.servicess.LocationService;
import com.skyfishjy.library.RippleBackground;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;

import de.hdodenhof.circleimageview.CircleImageView;
import in.shadowfax.proswipebutton.ProSwipeButton;

public class CopyMainActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {


    DrawerLayout navigationDrawer;


    private Activity activity;


    private GoogleMap map;
    private BottomSheetBehavior sheetBehavior;
    private ImageView showBottomSheet;
    private ImageView paymentTypeImage, cancelBooking, appbarMenu, callHome, msgHome, shareBooking;
    private TextView paymentTypeTV, user_number, User_name, maxUser, nameDriverBottomSheet, otpRider, carInfoBottomSheet, cancelRequest;
    private View driverInfoRL;
    private DrawerLayout drawer;
    private EditText fullTopAddress, fullBottomAddress;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    int placeLocation = 0, driverLocation = 0, duration = 0, newdurationHr = 0, newdurationMin = 0, myValue = 0;
    private String LocLat, LocLong, distance = "", newDistance = "", MobileNumber = "";
    private LatLng latLngSouce, latLngDesti;
    private RatingBar driverRatingBottomSheet;
    public static final String TAG = "Home";
    private Marker source, destination, drivers, drivers1, drivers2, drivers3, drivers4, drivers5, drivers6, drivers7;
    private CircleImageView imageDriverBottomSheet, imageDriverBottomSheet1;
    private VehicalViewModel vehicalViewModel;
//    private BookingViewModel bookingViewModel;


    private RecyclerView vehiclesHomeRC;
    private double sourceLat, sourceLng, destiLat, destiLng;
    private RelativeLayout mainLay;
    private PolylineOptions polylineOptions;
    private Polyline polyline;
    private Timer timer;


    //To calculate distance of to gmap points
    double _radiusEarthMiles = 3959;
    double _radiusEarthKM = 6371;
    double _m2km = 1.60934;
    double _toRad = Math.PI / 180;
    private double _centralAngle;

    private double Lat = 0.0, Lan = 0.0;
    private int checkCurrent = 0;
    //
    RippleBackground rippleBackground;
    ProSwipeButton proSwipeBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_copy_main);

        vehicalViewModel = ViewModelProviders.of(this).get(VehicalViewModel.class);

        navigationDrawer = findViewById(R.id.navigationDrawer);
//        navigationDrawer.setRadius(Gravity.START,25);

//        navigationDrawer.setViewScale(Gravity.START, 0.9f);
//        navigationDrawer.setRadius(Gravity.START, 35);
//        navigationDrawer.setViewElevation(Gravity.START, 20);


        services();
        findIds();


        rippleBackground = (RippleBackground) findViewById(R.id.content);

//        navigationIds();

    }


    private void findIds() {

        activity = CopyMainActivity.this;

        showBottomSheet = findViewById(R.id.showBottomSheet);
        showBottomSheet.setOnClickListener(this);

        appbarMenu = findViewById(R.id.appbarMenu);
        appbarMenu.setOnClickListener(this);

        drawer = findViewById(R.id.drawer);

//        paymentTypeImage = findViewById(R.id.paymentTypeImage);
        paymentTypeTV = findViewById(R.id.paymentTypeTV);
        fullTopAddress = findViewById(R.id.fullTopAddress);
        fullTopAddress.setFocusable(false);
        fullTopAddress.setOnClickListener(this);
        fullBottomAddress = findViewById(R.id.fullBottomAddress);
        fullBottomAddress.setFocusable(false);
        fullBottomAddress.setOnClickListener(this);

        imageDriverBottomSheet = findViewById(R.id.userImage);
        imageDriverBottomSheet1 = findViewById(R.id.imageDriverBottomSheet1);
        user_number = findViewById(R.id.user_number);
        User_name = findViewById(R.id.User_name);

        maxUser = findViewById(R.id.maxUser);

        nameDriverBottomSheet = findViewById(R.id.nameDriverBottomSheet);

        driverRatingBottomSheet = findViewById(R.id.driverRatingBottomSheet);
        otpRider = findViewById(R.id.otpRider);

        carInfoBottomSheet = findViewById(R.id.carInfoBottomSheet);

//        shimmerLayout = (ShimmerLayout) findViewById(R.id.shimmer_text);
//        shimmerLayout.startShimmerAnimation();

        mainLay = findViewById(R.id.mainLay);

        proSwipeBtn = (ProSwipeButton) findViewById(R.id.awesome_btn);

        proSwipeBtn.setOnSwipeListener(new ProSwipeButton.OnSwipeListener() {
            @Override
            public void onSwipeConfirm() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        proSwipeBtn.showResultIcon(true, true);
                        rippleBackground.stopRippleAnimation();
                        rippleBackground.setVisibility(View.GONE);
                        bottomSheetLayout();
                    }
                }, 2000);
            }
        });


//        if (!AppConstants.USER_IMAGE.equalsIgnoreCase("")) {
//            Glide.with(activity).load(App.getAppPreference().getString(AppConstants.USER_IMAGE)).into(imageDriverBottomSheet);
//        }
//        user_number.setText(App.getAppPreference().getString(AppConstants.USER_MOBILE));
//        User_name.setText(App.getAppPreference().getString(AppConstants.USER_NAME));


//        paymentTypeTV.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(activity, PaymentTypeActivity.class));
//            }
//        });


        LinearLayout layoutBottomSheet = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);


        cancelBooking = findViewById(R.id.cancelBooking);
        cancelBooking.setOnClickListener(this);

        msgHome = findViewById(R.id.msgHome);
        msgHome.setOnClickListener(this);

        callHome = findViewById(R.id.callHome);
        callHome.setOnClickListener(this);

        shareBooking = findViewById(R.id.shareBooking);
        shareBooking.setOnClickListener(this);


        driverInfoRL = findViewById(R.id.driverInfoRL);


        vehiclesHomeRC = findViewById(R.id.vehiclesHomeRC);
        vehiclesHomeRC.setLayoutManager(new LinearLayoutManager(activity,
                LinearLayoutManager.HORIZONTAL, false));

        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(vehiclesHomeRC);


//        driverInfoRL.setVisibility(View.VISIBLE);

//        Intent intent = getIntent();
//        String paymentType = intent.getStringExtra("paymentType");

//        if (App.getSinltonPojo().getPaymentType() != null) {
//            if (App.getSinltonPojo().getPaymentType().equalsIgnoreCase("Cash")) {
//                paymentTypeTV.setText(R.string.cash);
//                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                paymentTypeImage.setImageResource(R.drawable.ic_notes_color);
//            } else if (App.getSinltonPojo().getPaymentType().equalsIgnoreCase("Card")) {
//                paymentTypeTV.setText(R.string.card);
//                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                paymentTypeImage.setImageResource(R.drawable.credit_card__3_);
//            }
//
//        }

        Button pickUpBtn = findViewById(R.id.pickUpBtn);
        pickUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                proSwipeBtn.setVisibility(View.VISIBLE);

                rippleBackground.setVisibility(View.VISIBLE);
//                RelativeLayout.LayoutParams floatingButton = new RelativeLayout.LayoutParams(
//                        100, 100);
//                floatingButton.setMargins(0, 0, 10, 220);
//                floatingButton.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                floatingButton.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//                showBottomSheet.setLayoutParams(floatingButton);
//                takeScreenshot();
//                App.getSinltonPojo().setCancelrequest("0");
//                SendRequestToDriver();
//                driverInfoRL.setVisibility(View.VISIBLE);
                mainLay.setVisibility(View.GONE);
//                shimmerLayout.startShimmerAnimation();
                rippleBackground.startRippleAnimation();
            }
        });

        cancelRequest = findViewById(R.id.cancelRequest);
        cancelRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                App.getSinltonPojo().setCancelrequest("1");
                driverInfoRL.setVisibility(View.GONE);
                mainLay.setVisibility(View.GONE);
//                shimmerLayout.stopShimmerAnimation();
//                bottomSheetLayout();
            }
        });

    }

    private void services() {
        if (App.getSinltonPojo().getCurrentLatlng() != null) {
            setMap();
        } else {
            stopService(new Intent(CopyMainActivity.this, LocationService.class));
//            startService(new Intent(activity,MyLocationServices.class));
            Intent intent = new Intent(CopyMainActivity.this, LocationService.class);
            if (!isMyServiceRunning(intent.getClass())) {
                startService(intent);
                setMap();
            }
        }
    }

    private void setMap() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapHome);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.showBottomSheet:
                if (App.getSinltonPojo().getCurrentLatlng() != null) {
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(App.getSinltonPojo().getCurrentLatlng().latitude, App.getSinltonPojo().getCurrentLatlng().longitude), 12));


                    fullTopAddress.setText(GetCurrentLocationName(String.valueOf(App.getSinltonPojo().getCurrentLatlng().latitude), String.valueOf(App.getSinltonPojo().getCurrentLatlng().longitude)));

                    if (source != null) {
                        source.remove();
                    }
                    sourceLat = App.getSinltonPojo().getCurrentLatlng().latitude;
                    sourceLng = App.getSinltonPojo().getCurrentLatlng().longitude;
                    latLngSouce = new LatLng(App.getSinltonPojo().getCurrentLatlng().latitude, App.getSinltonPojo().getCurrentLatlng().longitude);
                    source = map.addMarker(new MarkerOptions()
                            .position(latLngSouce)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.green_marker)));

//                    initDirectionAPI(sourceLat, sourceLng, destiLat, destiLng);

                }
                break;

            case R.id.fullTopAddress:
                placeLocation = 1;
                Intent intent = new Intent(activity, SearchPlaceActivity.class);
                intent.putExtra("Type", "1");
                startActivity(intent);

//                openLocationActivity();
                break;

            case R.id.fullBottomAddress:
                placeLocation = 2;
//                openLocationActivity();
                Intent intent1 = new Intent(activity, SearchPlaceActivity.class);
                intent1.putExtra("Type", "2");
                startActivity(intent1);
                break;

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (App.getSinltonPojo().getSouceLat() != null) {
            sourceLng = Double.parseDouble(App.getSinltonPojo().getSourceLng());
            sourceLat = Double.parseDouble(App.getSinltonPojo().getSouceLat());
            fullTopAddress.setText(App.getSinltonPojo().getPickUpPoint());

            if (source != null) {
                source.remove();
            }
//                    source = map.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(LocLat), Double.parseDouble(LocLong))));

            latLngSouce = new LatLng(sourceLat, sourceLng);
            source = map.addMarker(new MarkerOptions()
                    .position(latLngSouce)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.green_marker)));
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(sourceLat, sourceLng), 12));

            if (checkCurrent == 0) {
                if (fullBottomAddress.getText().toString().isEmpty() || fullTopAddress.getText().toString().isEmpty()) {
                    sheetBehavior.setHideable(true);
                } else {
                    initDirectionAPI(Double.parseDouble(App.getSinltonPojo().getSouceLat()), Double.parseDouble(App.getSinltonPojo().getSourceLng()), Double.parseDouble(App.getSinltonPojo().getDestiLat()), Double.parseDouble(App.getSinltonPojo().getDestiLng()));
                }
            } else {
                if (fullBottomAddress.getText().toString().isEmpty() || fullTopAddress.getText().toString().isEmpty()) {
                    sheetBehavior.setHideable(true);
                } else {
                    initDirectionAPI(sourceLat, sourceLng, Double.parseDouble(App.getSinltonPojo().getDestiLat()), Double.parseDouble(App.getSinltonPojo().getDestiLng()));
                }
            }

        }
        if (App.getSinltonPojo().getDestiLat() != null) {
            destiLat = Double.parseDouble(App.getSinltonPojo().getDestiLat());
            destiLng = Double.parseDouble(App.getSinltonPojo().getDestiLng());
            fullBottomAddress.setText(App.getSinltonPojo().getDropPoint());
            if (checkCurrent == 0) {
                if (fullBottomAddress.getText().toString().isEmpty() || fullTopAddress.getText().toString().isEmpty()) {
                    sheetBehavior.setHideable(true);
                } else {
                    initDirectionAPI(Double.parseDouble(App.getSinltonPojo().getSouceLat()), Double.parseDouble(App.getSinltonPojo().getSourceLng()), Double.parseDouble(App.getSinltonPojo().getDestiLat()), Double.parseDouble(App.getSinltonPojo().getDestiLng()));
                }
            } else {
                if (fullBottomAddress.getText().toString().isEmpty() || fullTopAddress.getText().toString().isEmpty()) {
                    sheetBehavior.setHideable(true);
                } else {
                    initDirectionAPI(sourceLat, sourceLng, Double.parseDouble(App.getSinltonPojo().getDestiLat()), Double.parseDouble(App.getSinltonPojo().getDestiLng()));
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        if (App.getSinltonPojo().getCurrentLatlng() != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(App.getSinltonPojo().getCurrentLatlng().latitude, App.getSinltonPojo().getCurrentLatlng().longitude), 12));


            if (source != null) {
                source.remove();
            }

            sourceLat = App.getSinltonPojo().getCurrentLatlng().latitude;
            sourceLng = App.getSinltonPojo().getCurrentLatlng().longitude;
            latLngSouce = new LatLng(App.getSinltonPojo().getCurrentLatlng().latitude, App.getSinltonPojo().getCurrentLatlng().longitude);
            source = map.addMarker(new MarkerOptions()
                    .position(latLngSouce)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.green_marker)));
            fullTopAddress.setText(GetCurrentLocationName(String.valueOf(App.getSinltonPojo().getCurrentLatlng().latitude), String.valueOf(App.getSinltonPojo().getCurrentLatlng().longitude)));

            checkCurrent = 1;
        } else {
            services();
        }


    }

    public String GetCurrentLocationName(String latitude, String longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        String address = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }


    private void initDirectionAPI(final double sourceLat, final double sourceLng, final double destiLat, final double destiLng) {


        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (map != null) {
                    map.clear();
                }
                if (driverLocation == 0) {
                    latLngSouce = new LatLng(sourceLat, sourceLng);
                    source = map.addMarker(new MarkerOptions()
                            .position(latLngSouce)
                            .icon(bitmapDescriptorFromVector(CopyMainActivity.this, R.drawable.green_marker)));
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(sourceLat, sourceLng), 12));


                    latLngDesti = new LatLng(destiLat, destiLng);
                    destination = map.addMarker(new MarkerOptions()
                            .position(latLngDesti)
                            .icon(bitmapDescriptorFromVector(CopyMainActivity.this, R.drawable.red_marker)));
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destiLat, destiLng), 12));
                }


//                if (App.getSinltonPojo().getCurrentLatlng() != null) {
//
//                    Map<String, String> data = new HashMap<>();
//                    data.put("origin", sourceLat + "," + sourceLng);
//                    data.put("destination", destiLat + "," + destiLng);
//                    data.put("key", CopyMainActivity.this.getResources().getString(R.string.map_key));
//                    // ApiClient.getClientRoute();
//
//                    if (CommonUtils.isNetworkConnected(activity)) {
//                        vehicalViewModel.getRoute(activity, data).observe(CopyMainActivity.this, new Observer<DirectionPojo>() {
//                            @Override
//                            public void onChanged(@Nullable DirectionPojo directionPojo) {
//                                List<Route> routeList = directionPojo.getRoutes();
//
//                                polylineOptions = new PolylineOptions();
//                                polylineOptions.width(5).color(Color.BLUE).geodesic(true);
////
//                                for (int i = 0; i < routeList.size(); i++) {
//                                    List<Leg> legList = routeList.get(i).getLegs();
//                                    for (int j = 0; j < legList.size(); j++) {
//                                        distance = legList.get(j).getDistance().getText();
//                                        duration = legList.get(j).getDuration().getValue();
//                                        List<Step> stepList = legList.get(j).getSteps();
//                                        for (int k = 0; k < stepList.size(); k++) {
//                                            String polyline = stepList.get(k).getPolyline().getPoints();
//                                            List<LatLng> latlngList = decodePolyline(polyline);
//                                            for (int z = 0; z < latlngList.size(); z++) {
//                                                LatLng point = latlngList.get(z);
//                                                polylineOptions.add(point);
//                                            }
//                                        }
//                                    }
//                                }
//                                polyline = map.addPolyline(polylineOptions);
//
//                                myValue = 1;
//
//                                SetvehicleData();
//
////                                if (driverLocation == 0) {
////                                    setVehicalData(distance, duration);
////                                } else {
////                                    getDriverLocation(App.getAppPreference().getString(AppConstants.JOB_ID));
////                                }
//                            }
//                        });
//                    } else {
//                        Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
//                    }
//                }
                LatLng sydney1 = new LatLng(sourceLat, sourceLng);
                LatLng sydney2 = new LatLng(destiLat, destiLng);


                showCurvedPolyline(sydney1, sydney2, 0.5);
            }
        });

    }

    private void SetvehicleData() {
        bottomSheetLayout();
//        VehicleDetailAdapter adapter = new VehicleDetailAdapter(activity);
//        vehiclesHomeRC.setAdapter(adapter);
    }


    private void bottomSheetLayout() {

        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

    }

    private List<LatLng> decodePolyline(String polyline) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = polyline.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = polyline.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = polyline.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


    private BitmapDescriptor bitmapDescriptorFromVector(Context context,
                                                        @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void showCurvedPolyline(LatLng p1, LatLng p2, double k) {
        //Calculate distance and heading between two points
        double d = SphericalUtil.computeDistanceBetween(p1, p2);
        double h = SphericalUtil.computeHeading(p1, p2);

        //Midpoint position
        LatLng p = SphericalUtil.computeOffset(p1, d * 0.5, h);

        //Apply some mathematics to calculate position of the circle center
        double x = (1 - k * k) * d * 0.5 / (2 * k);
        double r = (1 + k * k) * d * 0.5 / (2 * k);

        LatLng c = SphericalUtil.computeOffset(p, x, h + 90.0);

        //Polyline options
        PolylineOptions options = new PolylineOptions();
        List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dash(30), new Gap(10));

        //Calculate heading between circle center and two points
        double h1 = SphericalUtil.computeHeading(c, p1);
        double h2 = SphericalUtil.computeHeading(c, p2);

        //Calculate positions of points on circle border and add them to polyline options
        int numpoints = 100;
        double step = (h2 - h1) / numpoints;

        for (int i = 0; i < numpoints; i++) {
            LatLng pi = SphericalUtil.computeOffset(c, r, h1 + i * step);
            options.add(pi);
        }

        //Draw polyline
        map.addPolyline(options.width(4).color(Color.BLACK).geodesic(false).pattern(pattern));
    }
}
