package com.omninos.saowari.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.adapters.ChatsCallsPagerAdapter;
import com.omninos.saowari.servicess.MySerives;

public class ChatAndCallsActivity extends AppCompatActivity implements View.OnClickListener {


    private TabLayout chatsAndCallTL;
    private ViewPager chatsAndCallVP;
    private ImageView firstIcon;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_and_calls);

        stopService(new Intent(ChatAndCallsActivity.this, MySerives.class));
        Intent intent = new Intent(ChatAndCallsActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }
        initView();
        SetUp();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void initView() {


        firstIcon = findViewById(R.id.firstIcon);
        title = findViewById(R.id.title);

        firstIcon.setOnClickListener(this);


        chatsAndCallTL = findViewById(R.id.chatsAndCallTL);
        chatsAndCallVP = findViewById(R.id.chatsAndCallVP);
        chatsAndCallVP.setAdapter(new ChatsCallsPagerAdapter(getSupportFragmentManager()));
        chatsAndCallTL.setupWithViewPager(chatsAndCallVP);
    }

    private void SetUp() {
        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        title.setText("");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstIcon:
                onBackPressed();
                break;
        }
    }
}
