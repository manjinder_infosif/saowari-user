package com.omninos.saowari.activities;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Dash;
import com.google.android.gms.maps.model.Gap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PatternItem;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;
import com.omninos.saowari.ModelClass.AcceptRideModel;
import com.omninos.saowari.ModelClass.CheckUserJob;
import com.omninos.saowari.ModelClass.GetDriverCurrentLocation;
import com.omninos.saowari.ModelClass.NearByDriverModel;
import com.omninos.saowari.ModelClass.NearByDriverModel1;
import com.omninos.saowari.ModelClass.VehicleTypePojo;
import com.omninos.saowari.ModelClass.VersionModel;
import com.omninos.saowari.MyViewModelClasses.BookingViewModel;
import com.omninos.saowari.MyViewModelClasses.LoginRegisterViewModel;
import com.omninos.saowari.MyViewModelClasses.NearDriverViewModel;
import com.omninos.saowari.MyViewModelClasses.VehicalViewModel;
import com.omninos.saowari.MyViewModelClasses.VersionViewModel;
import com.omninos.saowari.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.infideap.drawerbehavior.AdvanceDrawerLayout;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.Util.SnappingRecyclerView;
import com.omninos.saowari.adapters.VehicleDetailAdapter;
import com.omninos.saowari.directionApi.DirectionPojo;
import com.omninos.saowari.directionApi.Leg;
import com.omninos.saowari.directionApi.Route;
import com.omninos.saowari.directionApi.Step;
import com.omninos.saowari.servicess.LocationService;
import com.omninos.saowari.servicess.MySerives;
import com.skyfishjy.library.RippleBackground;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import in.shadowfax.proswipebutton.ProSwipeButton;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {


    AdvanceDrawerLayout navigationDrawer;


    private Activity activity;

    private LinearLayout oopsLayout;
    private CoordinatorLayout bgLayout;
    private GoogleMap map;
    private BottomSheetBehavior sheetBehavior;
    private ImageView showBottomSheet, reverseAddress;
    private ImageView paymentTypeImage, cancelBooking, appbarMenu, callHome, msgHome, shareBooking;
    private TextView paymentTypeTV, user_number, User_name, maxUser, nameDriverBottomSheet, otpRider, carInfoBottomSheet, cancelRequest;
    private View driverInfoRL;
    private DrawerLayout drawer;
    private EditText fullTopAddress, fullBottomAddress;
    int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    int placeLocation = 0, driverLocation = 0, duration = 0, newdurationHr = 0, newdurationMin = 0, myValue = 0;
    private String LocLat, LocLong, distance = "", timeTake = "", newDistance = "", MobileNumber = "";
    private LatLng latLngSouce, latLngDesti;
    private RatingBar driverRatingBottomSheet;
    public static final String TAG = "Home";
    private Marker source, destination, drivers, drivers1, drivers2, drivers3, drivers4, drivers5, drivers6, drivers7;
    private CircleImageView imageDriverBottomSheet, imageDriverBottomSheet1;
    private VehicalViewModel vehicalViewModel;
//    private BookingViewModel bookingViewModel;


    private RecyclerView vehiclesHomeRC;
    private double sourceLat, sourceLng, destiLat, destiLng;
    private RelativeLayout mainLay;
    private PolylineOptions polylineOptions;
    private Polyline polyline;
    private Timer timer;
    private String reason = "";


    //To calculate distance of to gmap points
    double _radiusEarthMiles = 3959;
    double _radiusEarthKM = 6371;
    double _m2km = 1.60934;
    double _toRad = Math.PI / 180;
    private double _centralAngle;

    private double Lat = 0.0, Lan = 0.0;
    private int checkCurrent = 0, MyValue = 0;
    List<VehicleTypePojo.Detail> list = new ArrayList<>();

    RippleBackground rippleBackground;
    ProSwipeButton proSwipeBtn;
    private LoginRegisterViewModel viewModel;
    private VersionViewModel versionViewModel;

    //navigation
    LinearLayout navMyWallet, navSetting, navInviteFriends, navCallsChats, navLogout, nvMyRide, navRide;
    private ImageView editProfile;

    private NearDriverViewModel driverViewModel;

    LinearLayoutManager linearLayoutManager;
    SnapHelper snapHelper;
    private BookingViewModel bookingViewModel;
    int version;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        versionViewModel = ViewModelProviders.of(this).get(VersionViewModel.class);


        navigationDrawer = findViewById(R.id.drawer);
//        navigationDrawer.setRadius(Gravity.START,25);

        navigationDrawer.setViewScale(Gravity.START, 0.9f);
        navigationDrawer.setRadius(Gravity.START, 35);
        navigationDrawer.setViewElevation(Gravity.START, 20);

        activity = MainActivity.this;

        App.getSinltonPojo().setCancelrequest("0");
        viewModel = ViewModelProviders.of(this).get(LoginRegisterViewModel.class);
        vehicalViewModel = ViewModelProviders.of(this).get(VehicalViewModel.class);
        driverViewModel = ViewModelProviders.of(this).get(NearDriverViewModel.class);
        bookingViewModel = ViewModelProviders.of(this).get(BookingViewModel.class);

        versionViewModel.checkJobUser(MainActivity.this, App.getAppPreference().GetString(ConstantData.USERID)).observe(MainActivity.this, new Observer<CheckUserJob>() {
            @Override
            public void onChanged(@Nullable CheckUserJob checkUserJob) {
                if (checkUserJob.getSuccess().equalsIgnoreCase("1")) {
                    App.getAppPreference().SaveString("MainData", "1");
                    App.getAppPreference().SaveString(ConstantData.JOB_ID, checkUserJob.getDetails().getJobId());
                    startActivity(new Intent(MainActivity.this, BookingActivity.class));
                    finishAffinity();
                }
            }
        });

        services();
        findIds();

//        checkConnection();
        App.getSinltonPojo().setVehicleId("0");

        startService(new Intent(getBaseContext(), MySerives.class));


        snapHelper = new LinearSnapHelper() {
            @Override
            public int findTargetSnapPosition(RecyclerView.LayoutManager layoutManager, int velocityX, int velocityY) {
                System.out.println("DataIs: " + String.valueOf(super.findTargetSnapPosition(layoutManager, velocityX, velocityY)));
                int dataji = super.findTargetSnapPosition(layoutManager, velocityX, velocityY);
                if (dataji >= 0) {
                    App.getSinltonPojo().setVehicleId(list.get(dataji).getId());
                    maxUser.setText(list.get(dataji).getSeatCapacity());
                    GetNearByDrivers(list.get(dataji).getId());
                }
                return super.findTargetSnapPosition(layoutManager, velocityX, velocityY);
            }
        };
        snapHelper.attachToRecyclerView(vehiclesHomeRC);

    }

    private void OpenWarningBox() {
        ViewGroup viewGroup = findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.update_app_pupup, viewGroup, false);
        Button buttonOk = dialogView.findViewById(R.id.buttonOk);

        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);


        //finally creating the alert dialog and displaying it
        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);
        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.omninos.saowari&hl=en")));
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

    private void initTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (App.getSinltonPojo().getVehicleId() != null) {
                    if (driverLocation == 0) {
                        GetNearByDrivers(App.getSinltonPojo().getVehicleId());
//                        GetNearByDrivers1(App.getSinltonPojo().getVehicleId());
                    }
                }
            }
        }, 10000, 10000);
    }

    private void GetNearByDrivers1(String vehicleId) {

        if (App.getSinltonPojo().getCurrentLatlng() != null) {
            driverViewModel.nearNewData(activity, vehicleId, String.valueOf(sourceLat), String.valueOf(sourceLng)).observe(MainActivity.this, new Observer<Map>() {
                @Override
                public void onChanged(@Nullable Map map) {

                }
            });
        }
    }

    private void GetNearByDrivers(final String vehicleId) {
//        activity.runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
        if (App.getSinltonPojo().getCurrentLatlng() != null) {
            driverViewModel.near(activity, vehicleId, String.valueOf(sourceLat), String.valueOf(sourceLng)).observe(MainActivity.this, new Observer<NearByDriverModel1>() {
                @Override
                public void onChanged(@Nullable NearByDriverModel1 nearByDriverModel) {
                    if (nearByDriverModel.getSuccess().equalsIgnoreCase("1")) {
                        if (drivers1 != null) {
                            drivers1.remove();
                        }
                        if (drivers2 != null) {
                            drivers2.remove();
                        }
                        if (drivers3 != null) {
                            drivers3.remove();
                        }
                        if (drivers4 != null) {
                            drivers4.remove();
                        }
                        if (drivers5 != null) {
                            drivers5.remove();
                        }
                        int size = nearByDriverModel.getDetails().size();
                        for (int i = 0; i < size; i++) {
                            if (i == 0) {
                                SingleSingleCars1(nearByDriverModel.getDetails().get(0).getPreviousLatitude(), nearByDriverModel.getDetails().get(0).getPreviousLongitude(), nearByDriverModel.getDetails().get(0).getLatitude(), nearByDriverModel.getDetails().get(0).getLongitude(), nearByDriverModel.getDetails().get(0).getDriverVehicleType());
                            }
                            if (i == 1) {
                                SingleSingleCars2(nearByDriverModel.getDetails().get(1).getPreviousLatitude(), nearByDriverModel.getDetails().get(1).getPreviousLongitude(), nearByDriverModel.getDetails().get(1).getLatitude(), nearByDriverModel.getDetails().get(1).getLongitude(), nearByDriverModel.getDetails().get(1).getDriverVehicleType());
                            }
                            if (i == 2) {
                                SingleSingleCars3(nearByDriverModel.getDetails().get(2).getPreviousLatitude(), nearByDriverModel.getDetails().get(2).getPreviousLongitude(), nearByDriverModel.getDetails().get(2).getLatitude(), nearByDriverModel.getDetails().get(2).getLongitude(), nearByDriverModel.getDetails().get(2).getDriverVehicleType());

                            }

                            if (i == 3) {
                                SingleSingleCars4(nearByDriverModel.getDetails().get(3).getPreviousLatitude(), nearByDriverModel.getDetails().get(3).getPreviousLongitude(), nearByDriverModel.getDetails().get(3).getLatitude(), nearByDriverModel.getDetails().get(3).getLongitude(), nearByDriverModel.getDetails().get(3).getDriverVehicleType());

                            }

                            if (i == 4) {
                                SingleSingleCars5(nearByDriverModel.getDetails().get(4).getPreviousLatitude(), nearByDriverModel.getDetails().get(4).getPreviousLongitude(), nearByDriverModel.getDetails().get(4).getLatitude(), nearByDriverModel.getDetails().get(4).getLongitude(), nearByDriverModel.getDetails().get(4).getDriverVehicleType());

                            }
                        }
                    } else {
                        if (drivers1 != null) {
                            drivers1.remove();
                        }
                        if (drivers2 != null) {
                            drivers2.remove();
                        }
                        if (drivers3 != null) {
                            drivers3.remove();
                        }
                        if (drivers4 != null) {
                            drivers4.remove();
                        }
                        if (drivers5 != null) {
                            drivers5.remove();
                        }

                        CommonUtils.showSnackbarAlert(navigationDrawer, "Please Select another vehicle");
//                                App.getSinltonPojo().setVehicleId("1");
//                                MyValue = 1;
//                                initDirectionAPI(sourceLat, sourceLng, destiLat, destiLng);
//                                source = map.addMarker(new MarkerOptions()
//                                        .position(latLngSouce)
//                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.green_marker)));
                    }
                }
            });
        }
//            }
//        });
    }

    private void SingleSingleCars5(String previousLatitude, String previousLongitude, final String latitude, final String longitude, final String driverVehicleType) {
        List<LatLng> latLngData = new ArrayList<>();
        final LatLng source = new LatLng(Double.parseDouble(previousLatitude), Double.parseDouble(previousLongitude));
        final LatLng destination = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        latLngData.add(source);
        latLngData.add(destination);
        if (HasMoved(Double.parseDouble(latitude), Double.parseDouble(longitude))) {
            animateCarOnMap5(latLngData, driverVehicleType);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (drivers5 != null) {
                        drivers5.remove();
                    }
                    double dLon = (Double.parseDouble(latitude) - App.getSinltonPojo().getCurrentLatlng().longitude);
                    double y = Math.sin(dLon) * Math.cos(Double.parseDouble(latitude));
                    double x = Math.cos(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.sin(Double.parseDouble(latitude)) - Math.sin(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.cos(Double.parseDouble(latitude)) * Math.cos(dLon);
                    double brng = Math.toDegrees((Math.atan2(y, x)));
                    brng = (360 - ((brng + 360) % 360));
//
                    if (driverVehicleType.equalsIgnoreCase("2")) {
                        drivers5 = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));
                    } else {
                        drivers5 = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
                    }

                    drivers5.setRotation((float) brng);
                }
            });

        }


    }

    private void animateCarOnMap5(final List<LatLng> latLngs, String driverVehicleType) {
        if (drivers5 != null) {
            drivers5.remove();
        }

        if (driverVehicleType.equalsIgnoreCase("2")) {
            drivers5 = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));

        } else {
            drivers5 = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));

        }

        drivers5.setPosition(latLngs.get(0));

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Float v = valueAnimator.getAnimatedFraction();
                double lng = v * latLngs.get(1).longitude + (1 - v)
                        * latLngs.get(0).longitude;
                double lat = v * latLngs.get(1).latitude + (1 - v)
                        * latLngs.get(0).latitude;
                LatLng newPos = new LatLng(lat, lng);
                drivers5.setPosition(newPos);
                drivers5.setAnchor(0.5f, 0.5f);
                drivers5.setRotation(getBearing(latLngs.get(0), newPos));
            }
        });
        valueAnimator.start();
    }

    private void SingleSingleCars4(String previousLatitude, String previousLongitude, final String latitude, final String longitude, final String driverVehicleType) {
        List<LatLng> latLngData = new ArrayList<>();
        final LatLng source = new LatLng(Double.parseDouble(previousLatitude), Double.parseDouble(previousLongitude));
        final LatLng destination = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        latLngData.add(source);
        latLngData.add(destination);
        if (HasMoved(Double.parseDouble(latitude), Double.parseDouble(longitude))) {
            animateCarOnMap4(latLngData, driverVehicleType);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (drivers4 != null) {
                        drivers4.remove();
                    }
                    double dLon = (Double.parseDouble(latitude) - App.getSinltonPojo().getCurrentLatlng().longitude);
                    double y = Math.sin(dLon) * Math.cos(Double.parseDouble(latitude));
                    double x = Math.cos(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.sin(Double.parseDouble(latitude)) - Math.sin(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.cos(Double.parseDouble(latitude)) * Math.cos(dLon);
                    double brng = Math.toDegrees((Math.atan2(y, x)));
                    brng = (360 - ((brng + 360) % 360));
//
                    if (driverVehicleType.equalsIgnoreCase("2")) {
                        drivers4 = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));
                    } else {
                        drivers4 = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
                    }

                    drivers4.setRotation((float) brng);
                }
            });

        }

    }

    private void animateCarOnMap4(final List<LatLng> latLngs, String driverVehicleType) {

        if (drivers4 != null) {
            drivers4.remove();
        }

        if (driverVehicleType.equalsIgnoreCase("2")) {
            drivers4 = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));

        } else {
            drivers4 = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));

        }

        drivers4.setPosition(latLngs.get(0));

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Float v = valueAnimator.getAnimatedFraction();
                double lng = v * latLngs.get(1).longitude + (1 - v)
                        * latLngs.get(0).longitude;
                double lat = v * latLngs.get(1).latitude + (1 - v)
                        * latLngs.get(0).latitude;
                LatLng newPos = new LatLng(lat, lng);
                drivers4.setPosition(newPos);
                drivers4.setAnchor(0.5f, 0.5f);
                drivers4.setRotation(getBearing(latLngs.get(0), newPos));
            }
        });
        valueAnimator.start();
    }

    private void SingleSingleCars3(String previousLatitude, String previousLongitude, final String latitude, final String longitude, final String driverVehicleType) {
        List<LatLng> latLngData = new ArrayList<>();
        final LatLng source = new LatLng(Double.parseDouble(previousLatitude), Double.parseDouble(previousLongitude));
        final LatLng destination = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        latLngData.add(source);
        latLngData.add(destination);
        if (HasMoved(Double.parseDouble(latitude), Double.parseDouble(longitude))) {
            animateCarOnMap3(latLngData, driverVehicleType);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (drivers3 != null) {
                        drivers3.remove();
                    }
                    double dLon = (Double.parseDouble(latitude) - App.getSinltonPojo().getCurrentLatlng().longitude);
                    double y = Math.sin(dLon) * Math.cos(Double.parseDouble(latitude));
                    double x = Math.cos(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.sin(Double.parseDouble(latitude)) - Math.sin(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.cos(Double.parseDouble(latitude)) * Math.cos(dLon);
                    double brng = Math.toDegrees((Math.atan2(y, x)));
                    brng = (360 - ((brng + 360) % 360));
//
                    if (driverVehicleType.equalsIgnoreCase("2")) {
                        drivers3 = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));
                    } else {
                        drivers3 = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
                    }

                    drivers3.setRotation((float) brng);
                }
            });

        }

    }

    private void animateCarOnMap3(final List<LatLng> latLngs, String driverVehicleType) {
        if (drivers3 != null) {
            drivers3.remove();
        }

        if (driverVehicleType.equalsIgnoreCase("2")) {
            drivers3 = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));

        } else {
            drivers3 = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));

        }

        drivers3.setPosition(latLngs.get(0));

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Float v = valueAnimator.getAnimatedFraction();
                double lng = v * latLngs.get(1).longitude + (1 - v)
                        * latLngs.get(0).longitude;
                double lat = v * latLngs.get(1).latitude + (1 - v)
                        * latLngs.get(0).latitude;
                LatLng newPos = new LatLng(lat, lng);
                drivers3.setPosition(newPos);
                drivers3.setAnchor(0.5f, 0.5f);
                drivers3.setRotation(getBearing(latLngs.get(0), newPos));
            }
        });
        valueAnimator.start();
    }

    private void SingleSingleCars1(String previousLatitude, String previousLongitude, final String latitude, final String longitude, final String vehicleType) {
        List<LatLng> latLngData = new ArrayList<>();
        final LatLng source = new LatLng(Double.parseDouble(previousLatitude), Double.parseDouble(previousLongitude));
        final LatLng destination = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        latLngData.add(source);
        latLngData.add(destination);
        if (HasMoved(Double.parseDouble(latitude), Double.parseDouble(longitude))) {
            animateCarOnMap1(latLngData, vehicleType);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (drivers1 != null) {
                        drivers1.remove();
                    }
                    double dLon = (Double.parseDouble(latitude) - App.getSinltonPojo().getCurrentLatlng().longitude);
                    double y = Math.sin(dLon) * Math.cos(Double.parseDouble(latitude));
                    double x = Math.cos(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.sin(Double.parseDouble(latitude)) - Math.sin(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.cos(Double.parseDouble(latitude)) * Math.cos(dLon);
                    double brng = Math.toDegrees((Math.atan2(y, x)));
                    brng = (360 - ((brng + 360) % 360));
//
                    if (vehicleType.equalsIgnoreCase("2")) {
                        drivers1 = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));
                    } else {
                        drivers1 = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
                    }

                    drivers1.setRotation((float) brng);
                }
            });

        }
    }

    private void animateCarOnMap1(final List<LatLng> latLngs, String vehicleType) {
        if (drivers1 != null) {
            drivers1.remove();
        }

        if (vehicleType.equalsIgnoreCase("2")) {
            drivers1 = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));

        } else {
            drivers1 = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));

        }

        drivers1.setPosition(latLngs.get(0));

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Float v = valueAnimator.getAnimatedFraction();
                double lng = v * latLngs.get(1).longitude + (1 - v)
                        * latLngs.get(0).longitude;
                double lat = v * latLngs.get(1).latitude + (1 - v)
                        * latLngs.get(0).latitude;
                LatLng newPos = new LatLng(lat, lng);
                drivers1.setPosition(newPos);
                drivers1.setAnchor(0.5f, 0.5f);
                drivers1.setRotation(getBearing(latLngs.get(0), newPos));
            }
        });
        valueAnimator.start();
    }

    private void SingleSingleCars2(String previousLatitude, String previousLongitude, final String latitude, final String longitude, final String vehicleData) {
        List<LatLng> latLngData = new ArrayList<>();
        final LatLng source = new LatLng(Double.parseDouble(previousLatitude), Double.parseDouble(previousLongitude));
        final LatLng destination = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        latLngData.add(source);
        latLngData.add(destination);

        if (HasMoved(Double.parseDouble(latitude), Double.parseDouble(longitude))) {
            animateCarOnMap2(latLngData, vehicleData);
        } else {
//
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (drivers2 != null) {
                        drivers2.remove();
                    }
                    double dLon = (Double.parseDouble(latitude) - App.getSinltonPojo().getCurrentLatlng().longitude);
                    double y = Math.sin(dLon) * Math.cos(Double.parseDouble(latitude));
                    double x = Math.cos(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.sin(Double.parseDouble(latitude)) - Math.sin(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.cos(Double.parseDouble(latitude)) * Math.cos(dLon);
                    double brng = Math.toDegrees((Math.atan2(y, x)));
                    brng = (360 - ((brng + 360) % 360));
//

                    if (vehicleData.equalsIgnoreCase("2")) {
                        drivers2 = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));
                    } else {
                        drivers2 = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
                    }

                    drivers2.setRotation((float) brng);
                }
            });

        }

    }

    private void animateCarOnMap2(final List<LatLng> latLngs, String vehicleData) {
        if (drivers2 != null) {
            drivers2.remove();
        }

        if (vehicleData.equalsIgnoreCase("2")) {
            drivers2 = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));
        } else {
            drivers2 = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car)));
        }


        drivers2.setPosition(latLngs.get(0));

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Float v = valueAnimator.getAnimatedFraction();
                double lng = v * latLngs.get(1).longitude + (1 - v)
                        * latLngs.get(0).longitude;
                double lat = v * latLngs.get(1).latitude + (1 - v)
                        * latLngs.get(0).latitude;
                LatLng newPos = new LatLng(lat, lng);
                drivers2.setPosition(newPos);
                drivers2.setAnchor(0.5f, 0.5f);
                drivers2.setRotation(getBearing(latLngs.get(0), newPos));
            }
        });
        valueAnimator.start();
    }

    public boolean HasMoved(double newLat, double newLan) {
        double significantDistance = 30;
        double currentDistance;

        currentDistance = DistanceMilesSEP(Lat, Lan, newLat, newLan);
        currentDistance = currentDistance * _m2km * 1000;

        if (currentDistance < significantDistance) {
            return false;
        } else {
            Lat = newLat;
            Lan = newLan;
            return true;
        }
    }

    public double DistanceMilesSEP(double Lat1, double Lon1, double Lat2, double Lon2) {
        try {
            double _radLat1 = Lat1 * _toRad;
            double _radLat2 = Lat2 * _toRad;
            double _dLat = (_radLat2 - _radLat1);
            double _dLon = (Lon2 - Lon1) * _toRad;

            double _a = (_dLon) * Math.cos((_radLat1 + _radLat2) / 2);

            // central angle, aka arc segment angular distance
            _centralAngle = Math.sqrt(_a * _a + _dLat * _dLat);

            // great-circle (orthodromic) distance on Earth between 2 points
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _radiusEarthMiles * _centralAngle;
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;

    }


    private void findIds() {

        activity = MainActivity.this;

        showBottomSheet = findViewById(R.id.showBottomSheet);
        showBottomSheet.setOnClickListener(this);

        appbarMenu = findViewById(R.id.appbarMenu);
        appbarMenu.setOnClickListener(this);

        drawer = findViewById(R.id.drawer);


        bgLayout = findViewById(R.id.bgLayout);
        oopsLayout = findViewById(R.id.oopsLayout);

//        paymentTypeImage = findViewById(R.id.paymentTypeImage);
        paymentTypeTV = findViewById(R.id.paymentTypeTV);
        fullTopAddress = findViewById(R.id.fullTopAddress);
        fullTopAddress.setFocusable(false);
        fullTopAddress.setOnClickListener(this);
        fullBottomAddress = findViewById(R.id.fullBottomAddress);
        fullBottomAddress.setFocusable(false);
        fullBottomAddress.setOnClickListener(this);

        imageDriverBottomSheet = findViewById(R.id.userImage);
        imageDriverBottomSheet1 = findViewById(R.id.imageDriverBottomSheet1);
        user_number = findViewById(R.id.user_number);
        User_name = findViewById(R.id.User_name);

        maxUser = findViewById(R.id.maxUser);

        nameDriverBottomSheet = findViewById(R.id.nameDriverBottomSheet);

        driverRatingBottomSheet = findViewById(R.id.driverRatingBottomSheet);
        otpRider = findViewById(R.id.otpRider);

        carInfoBottomSheet = findViewById(R.id.carInfoBottomSheet);
        rippleBackground = (RippleBackground) findViewById(R.id.content);


//        shimmerLayout = (ShimmerLayout) findViewById(R.id.shimmer_text);
//        shimmerLayout.startShimmerAnimation();

        mainLay = findViewById(R.id.mainLay);


        vehiclesHomeRC = findViewById(R.id.vehiclesHomeRC);

        linearLayoutManager = new LinearLayoutManager(activity);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        vehiclesHomeRC.setLayoutManager(linearLayoutManager);
        navRide = findViewById(R.id.navRide);
        navRide.setOnClickListener(this);
//        vehiclesHomeRC.setLayoutManager(new LinearLayoutManager(activity,
//                LinearLayoutManager.HORIZONTAL, false));


//        RecyclerView.LayoutManager layoutManager=vehiclesHomeRC.getLayoutManager();
//        View snapView=snapHelper.findSnapView(linearLayoutManager);
//        Toast.makeText(activity, String.valueOf(linearLayoutManager.getPosition(snapView)), Toast.LENGTH_SHORT).show();


        proSwipeBtn = (ProSwipeButton) findViewById(R.id.awesome_btn);

        proSwipeBtn.setOnSwipeListener(new ProSwipeButton.OnSwipeListener() {
            @Override
            public void onSwipeConfirm() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialogCancelBooking();
                    }
                }, 2000);
            }
        });


        paymentTypeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(activity, PaymentActivity.class));
            }
        });


        LinearLayout layoutBottomSheet = findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);


        cancelBooking = findViewById(R.id.cancelBooking);
        cancelBooking.setOnClickListener(this);

        msgHome = findViewById(R.id.msgHome);
        msgHome.setOnClickListener(this);

        callHome = findViewById(R.id.callHome);
        callHome.setOnClickListener(this);

        shareBooking = findViewById(R.id.shareBooking);
        shareBooking.setOnClickListener(this);


        driverInfoRL = findViewById(R.id.driverInfoRL);
//        driverInfoRL.setVisibility(View.VISIBLE);

//        Intent intent = getIntent();
//        String paymentType = intent.getStringExtra("paymentType");

//        if (App.getSinltonPojo().getPaymentType() != null) {
//            if (App.getSinltonPojo().getPaymentType().equalsIgnoreCase("Cash")) {
//                paymentTypeTV.setText(R.string.cash);
//                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                paymentTypeImage.setImageResource(R.drawable.ic_notes_color);
//            } else if (App.getSinltonPojo().getPaymentType().equalsIgnoreCase("Card")) {
//                paymentTypeTV.setText(R.string.card);
//                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                paymentTypeImage.setImageResource(R.drawable.credit_card__3_);
//            }
//
//        }

        Button pickUpBtn = findViewById(R.id.pickUpBtn);
        pickUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                proSwipeBtn.setVisibility(View.VISIBLE);

                rippleBackground.setVisibility(View.VISIBLE);
                rippleBackground.startRippleAnimation();
                SendRequestToDriver();

                mainLay.setVisibility(View.GONE);
//                shimmerLayout.startShimmerAnimation();
            }
        });

        cancelRequest = findViewById(R.id.cancelRequest);
        cancelRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                App.getSinltonPojo().setCancelrequest("1");
                driverInfoRL.setVisibility(View.GONE);
                mainLay.setVisibility(View.GONE);
//                shimmerLayout.stopShimmerAnimation();
//                bottomSheetLayout();
            }
        });


        //navigation Id's
        navMyWallet = findViewById(R.id.navMyWallet);
        navMyWallet.setOnClickListener(this);

        navSetting = findViewById(R.id.navSetting);
        navSetting.setOnClickListener(this);

        navInviteFriends = findViewById(R.id.navInviteFriends);
        navInviteFriends.setOnClickListener(this);


        editProfile = findViewById(R.id.editProfile);
        editProfile.setOnClickListener(this);

        navCallsChats = findViewById(R.id.navCallsChats);
        navCallsChats.setOnClickListener(this);

        navLogout = findViewById(R.id.navLogout);
        navLogout.setOnClickListener(this);

        nvMyRide = findViewById(R.id.nvMyRide);
        nvMyRide.setOnClickListener(this);


//        vehiclesHomeRC.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//
//            }
//
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//
//                View view = vehiclesHomeRC.getCenterView();
//                int pos = vehiclesHomeRC.getChildAdapterPosition(view);
//                System.out.println("ListData: " + pos);
//            }
//        });


        reverseAddress = findViewById(R.id.reverseAddress);
        reverseAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!fullBottomAddress.getText().toString().isEmpty() && !fullTopAddress.getText().toString().isEmpty()) {

                    RotateAnimation rotate = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                    rotate.setDuration(1000);
                    rotate.setInterpolator(new LinearInterpolator());

                    reverseAddress.startAnimation(rotate);

                    String sourceAddress = fullTopAddress.getText().toString();
                    String destinationAddress = fullBottomAddress.getText().toString();
                    String tempAddress = sourceAddress;
                    sourceAddress = destinationAddress;
                    destinationAddress = tempAddress;
                    fullBottomAddress.setText(destinationAddress);
                    fullTopAddress.setText(sourceAddress);

                    double tempsourceLat, tempsourceLng, tempdestiLat, tempdestiLng;

                    tempsourceLat = sourceLat;
                    sourceLat = destiLat;
                    destiLat = tempsourceLat;


                    tempsourceLng = sourceLng;
                    sourceLng = destiLng;
                    destiLng = tempsourceLng;


                    initDirectionAPI(sourceLat, sourceLng, destiLat, destiLng);
                }
            }
        });

    }

    private void dialogCancelBooking() {

        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.cancle_reason_popup);

        RelativeLayout rsnLostItem, rsnFareIssue, rsnRouteFeedback, rsnDriverFeedback, rsnVehicleFeedback,
                rsnRecieptAndPayment, rsnPromotion;
        final ImageView checkFareIssue, checkLostItem, checkRouteFeedback, checkDriverFeedback, checkVehicleFeedback,
                checkRecieptAndPayment, checkPromotion;

        checkFareIssue = dialog.findViewById(R.id.checkFareIssue);
        checkLostItem = dialog.findViewById(R.id.checkLostItem);
        checkRouteFeedback = dialog.findViewById(R.id.checkRouteFeedback);
        checkDriverFeedback = dialog.findViewById(R.id.checkDriverFeedback);
        checkVehicleFeedback = dialog.findViewById(R.id.checkVehicleFeedback);
        checkRecieptAndPayment = dialog.findViewById(R.id.checkRecieptAndPayment);
        checkPromotion = dialog.findViewById(R.id.checkPromotion);

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        Button cancelBtnPopupCancel = dialog.findViewById(R.id.cancelBtnPopupCancel);
        cancelBtnPopupCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button doneBtnPopupCancel = dialog.findViewById(R.id.doneBtnPopupCancel);
        doneBtnPopupCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingViewModel.cancleJob(MainActivity.this, App.getAppPreference().GetString(ConstantData.USERID), reason).observe(MainActivity.this, new Observer<Map>() {
                    @Override
                    public void onChanged(@Nullable Map map) {
                        if (map.get("success").equals("1")) {
                            proSwipeBtn.showResultIcon(true, true);
                            rippleBackground.stopRippleAnimation();
                            rippleBackground.setVisibility(View.GONE);
                            App.getSinltonPojo().setCancelrequest("1");
                            bottomSheetLayout();
//                            App.getAppPreference().SaveString(AppConstants.JOB_ID, "");
//                            startActivity(new Intent(DriverArrivedActivity.this, HomeActivity.class));
                        } else {

                        }
                    }
                });
                dialog.dismiss();
            }
        });

        rsnLostItem = dialog.findViewById(R.id.rsnLostItem);
        rsnLostItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.VISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
                reason = "Lost Item";
            }
        });
        rsnFareIssue = dialog.findViewById(R.id.rsnFareIssue);
        rsnFareIssue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.VISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
                reason = "Fare Issue";
            }
        });
        rsnRouteFeedback = dialog.findViewById(R.id.rsnRouteFeedback);
        rsnRouteFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.VISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
                reason = "Route Feedback";
            }
        });
        rsnDriverFeedback = dialog.findViewById(R.id.rsnDriverFeedback);
        rsnDriverFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.VISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
                reason = "Driver Feedback";
            }
        });
        rsnVehicleFeedback = dialog.findViewById(R.id.rsnVehicleFeedback);
        rsnVehicleFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.VISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
                reason = "Vehicle Feedback";
            }
        });

        rsnRecieptAndPayment = dialog.findViewById(R.id.rsnRecieptAndPayment);
        rsnRecieptAndPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.VISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
                reason = "Receipt and Payment";
            }
        });
        rsnPromotion = dialog.findViewById(R.id.rsnPromotion);
        rsnPromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.VISIBLE);
                reason = "Promotions";
            }
        });

        dialog.show();

    }


    private void SendRequestToDriver() {
        bookingViewModel.booking(activity, fullTopAddress.getText().toString(), String.valueOf(sourceLat), String.valueOf(sourceLng),
                fullBottomAddress.getText().toString(), String.valueOf(destiLat), String.valueOf(destiLng), App.getSinltonPojo().getVehicleId(), App.getAppPreference().GetString(ConstantData.USERID),
                App.getSinltonPojo().getPayment(), paymentTypeTV.getText().toString(), distance).observe(MainActivity.this, new Observer<Map>() {
            @Override
            public void onChanged(@Nullable Map map) {
                if (map.get("success").equals("1")) {
                    App.getSinltonPojo().setCancelrequest("0");
                    GetDriverInformation();
                } else {
                    App.getSinltonPojo().setCancelrequest("1");
                    proSwipeBtn.showResultIcon(true, true);
                    rippleBackground.stopRippleAnimation();
                    rippleBackground.setVisibility(View.GONE);
                    bottomSheetLayout();
                }
            }
        });
    }

    private void GetDriverInformation() {
        bookingViewModel.acceptRideModelLiveData(activity, App.getAppPreference().GetString(ConstantData.USERID)).observe(MainActivity.this, new Observer<AcceptRideModel>() {
            @Override
            public void onChanged(@Nullable AcceptRideModel acceptRideModel) {
                driverInfoRL.setVisibility(View.VISIBLE);
                mainLay.setVisibility(View.VISIBLE);
                proSwipeBtn.showResultIcon(true, true);
                proSwipeBtn.setVisibility(View.GONE);
                rippleBackground.stopRippleAnimation();
                rippleBackground.setVisibility(View.GONE);
                MobileNumber = acceptRideModel.getDetails().getDriverPhone();
                App.getAppPreference().SaveString(ConstantData.DrIVER_NUMBER, MobileNumber);
                Glide.with(MainActivity.this).load(acceptRideModel.getDetails().getDriverImage()).into(imageDriverBottomSheet1);
                nameDriverBottomSheet.setText(acceptRideModel.getDetails().getDriverName());
                carInfoBottomSheet.setText(acceptRideModel.getDetails().getVehicleModel());
                otpRider.setText(acceptRideModel.getDetails().getDriverVehicleNumber());
                App.getAppPreference().SaveString(ConstantData.DRIVER_ID, acceptRideModel.getDetails().getDriverId());
                App.getAppPreference().SaveString(ConstantData.JOB_ID, acceptRideModel.getDetails().getJobId());
                driverLocation = 1;
                App.getAppPreference().SaveString("MainData", "1");
                startActivity(new Intent(activity, BookingActivity.class));
                finishAffinity();
//                initDirectionAPI(Double.parseDouble(acceptRideModel.getDetails().getDriverLatitude()), Double.parseDouble(acceptRideModel.getDetails().getDriverLongitude()), sourceLat, sourceLng);
            }
        });
    }

    private void services() {
        if (App.getSinltonPojo().getCurrentLatlng() != null) {
            setMap();
        } else {
            stopService(new Intent(MainActivity.this, LocationService.class));
//            startService(new Intent(activity,MyLocationServices.class));
            Intent intent = new Intent(MainActivity.this, LocationService.class);
            if (!isMyServiceRunning(intent.getClass())) {
                startService(intent);
                setMap();
            }
        }
    }

    private void setMap() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapHome);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nvMyRide:
                startActivity(new Intent(MainActivity.this, MyRidesActivity.class));
                break;

            case R.id.appbarMenu:
                drawer.openDrawer(Gravity.START);
                break;

            case R.id.navMyWallet:
                startActivity(new Intent(MainActivity.this, MyWalletActivity.class));
                break;

            case R.id.navSetting:
                startActivity(new Intent(MainActivity.this, SettingActivity.class));
                break;

            case R.id.navInviteFriends:
                startActivity(new Intent(MainActivity.this, InviteFriendsActivity.class));
                break;

            case R.id.editProfile:
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
                break;
//            case R.id.confirmBooking:
//                startActivity(new Intent(MainActivity.this, BookingActivity.class));
//                break;

            case R.id.navCallsChats:
                startActivity(new Intent(MainActivity.this, ChatAndCallsActivity.class));
                break;

//            case R.id.paymentButton:
//                startActivity(new Intent(MainActivity.this, PaymentActivity.class));
//                break;

            case R.id.fullTopAddress:
                App.getSinltonPojo().setLocationStatus(null);
                Intent intent1 = new Intent(MainActivity.this, SearchPlaceActivity.class);
                intent1.putExtra("Type", "1");
                startActivity(intent1);
                break;

            case R.id.fullBottomAddress:
                App.getSinltonPojo().setLocationStatus(null);
                Intent intent2 = new Intent(MainActivity.this, SearchPlaceActivity.class);
                intent2.putExtra("Type", "2");
                startActivity(intent2);
                break;

            case R.id.navLogout:
                viewModel.LogOut(activity, App.getAppPreference().GetString(ConstantData.USERID)).observe(MainActivity.this, new Observer<Map>() {
                    @Override
                    public void onChanged(@Nullable Map map) {
                        if (map.get("success").toString().equalsIgnoreCase("1")) {
                            App.getAppPreference().Logout();
                            startActivity(new Intent(MainActivity.this, LoginActivity.class));
                            finishAffinity();
                        } else {
                            CommonUtils.showSnackbarAlert(navLogout, "Try Again");
                        }
                    }
                });
                break;

            case R.id.tryAgainButton:
//                checkConnection();
                break;
            case R.id.navRide:
                Intent intent = new Intent(activity, SelectLanguageActivity.class);
                intent.putExtra("Type", "1");
                startActivity(intent);
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionCode;
            System.out.println("VersionCode : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        versionViewModel.modelLiveData(MainActivity.this).observe(MainActivity.this, new Observer<VersionModel>() {
            @Override
            public void onChanged(@Nullable VersionModel versionModel) {
                if (versionModel.getSuccess().equalsIgnoreCase("1")) {
                    if (!versionModel.getDetails().getVersion().equalsIgnoreCase(String.valueOf(version))) {
                        OpenWarningBox();
                    }
                }
            }
        });


        initTimer();
        if (App.getAppPreference().GetString("MainData").equalsIgnoreCase("1")) {
            startActivity(new Intent(MainActivity.this, BookingActivity.class));
            finishAffinity();
        }

        Glide.with(activity).load(App.getAppPreference().getLoginDetail().getDetails().getImage()).into(imageDriverBottomSheet);
        user_number.setText(App.getAppPreference().getLoginDetail().getDetails().getPhone());
        User_name.setText(App.getAppPreference().getLoginDetail().getDetails().getName());

        try {
            if (App.getSinltonPojo().getSouceLat() != null) {
                sourceLng = Double.parseDouble(App.getSinltonPojo().getSourceLng());
                sourceLat = Double.parseDouble(App.getSinltonPojo().getSouceLat());
                fullTopAddress.setText(App.getSinltonPojo().getPickUpPoint());

                if (source != null) {
                    source.remove();
                }
//                    source = map.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(LocLat), Double.parseDouble(LocLong))));

                latLngSouce = new LatLng(sourceLat, sourceLng);
                if (Build.VERSION.SDK_INT >= 23) {
                    source = map.addMarker(new MarkerOptions()
                            .position(latLngSouce)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker)));

                } else {
                    source = map.addMarker(new MarkerOptions()
                            .position(latLngSouce));

                }
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(sourceLat, sourceLng), 12));

                if (checkCurrent == 0) {
                    if (fullBottomAddress.getText().toString().isEmpty() || fullTopAddress.getText().toString().isEmpty()) {
                        sheetBehavior.setHideable(true);
                    } else {
                        MyValue = 0;
                        initDirectionAPI(Double.parseDouble(App.getSinltonPojo().getSouceLat()), Double.parseDouble(App.getSinltonPojo().getSourceLng()), Double.parseDouble(App.getSinltonPojo().getDestiLat()), Double.parseDouble(App.getSinltonPojo().getDestiLng()));
                    }
                } else {
                    if (fullBottomAddress.getText().toString().isEmpty() || fullTopAddress.getText().toString().isEmpty()) {
                        sheetBehavior.setHideable(true);
                    } else {
                        MyValue = 0;
                        initDirectionAPI(sourceLat, sourceLng, Double.parseDouble(App.getSinltonPojo().getDestiLat()), Double.parseDouble(App.getSinltonPojo().getDestiLng()));
                    }
                }

            }
            if (App.getSinltonPojo().getDestiLat() != null) {
                destiLat = Double.parseDouble(App.getSinltonPojo().getDestiLat());
                destiLng = Double.parseDouble(App.getSinltonPojo().getDestiLng());
                fullBottomAddress.setText(App.getSinltonPojo().getDropPoint());
                if (checkCurrent == 0) {
                    if (fullBottomAddress.getText().toString().isEmpty() || fullTopAddress.getText().toString().isEmpty()) {
                        sheetBehavior.setHideable(true);
                    } else {
                        MyValue = 0;
                        initDirectionAPI(Double.parseDouble(App.getSinltonPojo().getSouceLat()), Double.parseDouble(App.getSinltonPojo().getSourceLng()), Double.parseDouble(App.getSinltonPojo().getDestiLat()), Double.parseDouble(App.getSinltonPojo().getDestiLng()));
                    }
                } else {
                    if (fullBottomAddress.getText().toString().isEmpty() || fullTopAddress.getText().toString().isEmpty()) {
                        sheetBehavior.setHideable(true);
                    } else {
                        MyValue = 0;
                        initDirectionAPI(sourceLat, sourceLng, Double.parseDouble(App.getSinltonPojo().getDestiLat()), Double.parseDouble(App.getSinltonPojo().getDestiLng()));
                    }
                }
            }
        } catch (Exception e) {

        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        map = googleMap;

        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
//        map.setMyLocationEnabled(true);
        if (App.getSinltonPojo().getCurrentLatlng() != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(App.getSinltonPojo().getCurrentLatlng().latitude, App.getSinltonPojo().getCurrentLatlng().longitude), 12));
            if (source != null) {
                source.remove();
            }
            sourceLat = App.getSinltonPojo().getCurrentLatlng().latitude;
            sourceLng = App.getSinltonPojo().getCurrentLatlng().longitude;
            latLngSouce = new LatLng(App.getSinltonPojo().getCurrentLatlng().latitude, App.getSinltonPojo().getCurrentLatlng().longitude);
            source = map.addMarker(new MarkerOptions()
                    .position(latLngSouce)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker)));
            fullTopAddress.setText(GetCurrentLocationName(String.valueOf(App.getSinltonPojo().getCurrentLatlng().latitude), String.valueOf(App.getSinltonPojo().getCurrentLatlng().longitude)));

            checkCurrent = 1;
        } else {
            services();
        }
    }

    public String GetCurrentLocationName(String latitude, String longitude) {
        Geocoder geocoder;
        List<Address> addresses;
        String address = null;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return address;
    }


    private void initDirectionAPI(final double sourceLat, final double sourceLng, final double destiLat, final double destiLng) {


        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (map != null) {
                    map.clear();
                }
                if (MyValue != 1) {


                    if (driverLocation == 0) {
                        latLngSouce = new LatLng(sourceLat, sourceLng);
                        source = map.addMarker(new MarkerOptions()
                                .position(latLngSouce)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker)));
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(sourceLat, sourceLng), 12));


                        latLngDesti = new LatLng(destiLat, destiLng);
                        destination = map.addMarker(new MarkerOptions()
                                .position(latLngDesti)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.destination_marker)));
                        map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(destiLat, destiLng), 12));
                    }


                    if (App.getSinltonPojo().getCurrentLatlng() != null) {

                        Map<String, String> data = new HashMap<>();
                        data.put("origin", sourceLat + "," + sourceLng);
                        data.put("destination", destiLat + "," + destiLng);
                        data.put("key", MainActivity.this.getResources().getString(R.string.map_key));
                        // ApiClient.getClientRoute();

                        if (CommonUtils.isNetworkConnected(activity)) {
                            vehicalViewModel.getRoute(activity, data).observe(MainActivity.this, new Observer<DirectionPojo>() {
                                @Override
                                public void onChanged(@Nullable DirectionPojo directionPojo) {
                                    List<Route> routeList = directionPojo.getRoutes();

                                    polylineOptions = new PolylineOptions();
                                    polylineOptions.width(5).color(Color.BLUE).geodesic(true);
//
                                    for (int i = 0; i < routeList.size(); i++) {
                                        List<Leg> legList = routeList.get(i).getLegs();
                                        for (int j = 0; j < legList.size(); j++) {
                                            distance = legList.get(j).getDistance().getText();
                                            duration = legList.get(j).getDuration().getValue();
                                            timeTake = legList.get(j).getDuration().getText();
                                            List<Step> stepList = legList.get(j).getSteps();
                                            for (int k = 0; k < stepList.size(); k++) {
                                                String polyline = stepList.get(k).getPolyline().getPoints();
                                                List<LatLng> latlngList = decodePolyline(polyline);
                                                for (int z = 0; z < latlngList.size(); z++) {
                                                    LatLng point = latlngList.get(z);
                                                    polylineOptions.add(point);
                                                }
                                            }
                                        }
                                    }
                                    polyline = map.addPolyline(polylineOptions);

                                    myValue = 1;


                                    if (driverLocation == 0) {
                                        setVehicalData(distance, duration, timeTake);
                                    } else {
                                        getDriverLocation(App.getAppPreference().GetString(ConstantData.JOB_ID));
                                    }


                                }
                            });
                        } else {
                            Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });


    }

    private void getDriverLocation(String string) {
        bookingViewModel.getDriverCurrentLocationLiveData(activity, string).observe(MainActivity.this, new Observer<GetDriverCurrentLocation>() {
            @Override
            public void onChanged(@Nullable GetDriverCurrentLocation getDriverCurrentLocation) {
                if (getDriverCurrentLocation.getSuccess().equalsIgnoreCase("1")) {
                    timer.cancel();
                    if (getDriverCurrentLocation.getDetails().getJobStatus().equalsIgnoreCase("1")) {
                        if (drivers != null) {
                            drivers.remove();
                        }
                        double dLon = (Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()) - App.getSinltonPojo().getCurrentLatlng().longitude);
                        double y = Math.sin(dLon) * Math.cos(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()));
                        double x = Math.cos(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.sin(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude())) - Math.sin(App.getSinltonPojo().getCurrentLatlng().longitude) * Math.cos(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude())) * Math.cos(dLon);
                        double brng = Math.toDegrees((Math.atan2(y, x)));
                        brng = (360 - ((brng + 360) % 360));

                        drivers = map.addMarker(new MarkerOptions()
                                .position(new LatLng(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude())))
                                .anchor(0.5f, 0.5f)
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));
                        drivers.setRotation((float) brng);

//                    startActivity(new Intent(activity, BookingActivity.class));
//                    finishAffinity();
//
                    }

                }
            }
        });
    }


    private void setVehicalData(String distance, int duration, final String timeTaken) {
        bottomSheetLayout();

        newDistance = distance.replace("km", "").trim();

        newdurationMin = duration / 60;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                vehicalViewModel.vehicleTypePojoLiveData(activity).observe(MainActivity.this, new Observer<VehicleTypePojo>() {
                    @Override
                    public void onChanged(@Nullable VehicleTypePojo vehicleTypePojo) {
                        if (vehicleTypePojo.getSuccess().equalsIgnoreCase("1")) {
                            if (vehicleTypePojo.getDetails() != null) {
                                list = vehicleTypePojo.getDetails();
//                                maxUser.setText(list.get(0).getSeatCapacity());
                                VehicleDetailAdapter adapter = new VehicleDetailAdapter(activity, timeTaken, newDistance, newdurationMin, list, new VehicleDetailAdapter.ChooseVehical() {
                                    @Override
                                    public void choose(String position) {
                                        App.getSinltonPojo().setVehicleId(position);
                                        GetNearByDrivers(position);
                                    }

                                    @Override
                                    public void passanger(String maxUsr) {
                                        maxUser.setText(maxUsr);
                                    }

                                    @Override
                                    public void longPressData(int position) {
                                        Toast.makeText(activity, "Long Press", Toast.LENGTH_SHORT).show();
                                        OnVehicleDailog(list.get(position).getServiceImage1(), list.get(position).getTitle(), list.get(position).getBasePrice(), list.get(position).getBaseDistance(), list.get(position).getUnitTimePricing(), list.get(position).getDescription());
                                    }
                                });

                                vehiclesHomeRC.setAdapter(adapter);
                            }
                        } else {
                            if (drivers1 != null) {
                                drivers1.remove();
                            }
                            if (drivers2 != null) {
                                drivers2.remove();
                            }
                            if (drivers3 != null) {
                                drivers3.remove();
                            }
                            if (drivers4 != null) {
                                drivers4.remove();
                            }
                            if (drivers5 != null) {
                                drivers5.remove();
                            }

                            CommonUtils.showSnackbarAlert(drawer, vehicleTypePojo.getMessage());
                        }
                    }
                });

            }
        });


//        vehiclesHomeRC.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
////                super.onScrolled(recyclerView, dx, dy);
//                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
//                View snapView = snapHelper.findSnapView(layoutManager);
//                int snapPosition = layoutManager.getPosition(snapView);
//                System.out.println("ListData: " + snapPosition);
//            }
//        });

    }

    private void OnVehicleDailog(String url, String type, String basefare, String kilometerfare, String minutefare, String description) {
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.vehicle_detail_popup);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Button gotItButton = dialog.findViewById(R.id.gotItButton);
        dialog.setCanceledOnTouchOutside(false);

        TextView baseFareText, perKiloText, perMinText, miniFareText, vehicleName, vehicleDescription;
        ImageView vehicleImage;

        vehicleImage = dialog.findViewById(R.id.vehicleImage);
        vehicleName = dialog.findViewById(R.id.vehicleName);
        baseFareText = dialog.findViewById(R.id.baseFareText);
        perKiloText = dialog.findViewById(R.id.perKiloText);
        perMinText = dialog.findViewById(R.id.perMinText);
        miniFareText = dialog.findViewById(R.id.miniFareText);
        vehicleDescription = dialog.findViewById(R.id.vehicleDescription);

        Glide.with(MainActivity.this).load(url).into(vehicleImage);
        vehicleName.setText(type);
        baseFareText.setText("৳ " + basefare);
        perKiloText.setText("৳ " + kilometerfare);
        perMinText.setText("৳ " + minutefare);
        miniFareText.setText("৳ " + basefare);
        vehicleDescription.setText(description);
        gotItButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void bottomSheetLayout() {

        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

    }

    private List<LatLng> decodePolyline(String polyline) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = polyline.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = polyline.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = polyline.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


    private BitmapDescriptor bitmapDescriptorFromVector(Context context,
                                                        @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    private void showCurvedPolyline(LatLng p1, LatLng p2, double k) {
        //Calculate distance and heading between two points
        double d = SphericalUtil.computeDistanceBetween(p1, p2);
        double h = SphericalUtil.computeHeading(p1, p2);

        //Midpoint position
        LatLng p = SphericalUtil.computeOffset(p1, d * 0.5, h);

        //Apply some mathematics to calculate position of the circle center
        double x = (1 - k * k) * d * 0.5 / (2 * k);
        double r = (1 + k * k) * d * 0.5 / (2 * k);

        LatLng c = SphericalUtil.computeOffset(p, x, h + 90.0);

        //Polyline options
        PolylineOptions options = new PolylineOptions();
        List<PatternItem> pattern = Arrays.<PatternItem>asList(new Dash(30), new Gap(10));

        //Calculate heading between circle center and two points
        double h1 = SphericalUtil.computeHeading(c, p1);
        double h2 = SphericalUtil.computeHeading(c, p2);

        //Calculate positions of points on circle border and add them to polyline options
        int numpoints = 100;
        double step = (h2 - h1) / numpoints;

        for (int i = 0; i < numpoints; i++) {
            LatLng pi = SphericalUtil.computeOffset(c, r, h1 + i * step);
            options.add(pi);
        }

        //Draw polyline
        map.addPolyline(options.width(4).color(Color.BLACK).geodesic(false).pattern(pattern));
    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
    }
}
