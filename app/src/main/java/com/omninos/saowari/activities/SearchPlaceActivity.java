package com.omninos.saowari.activities;

import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.omninos.saowari.ModelClass.GetLatLngModel;
import com.omninos.saowari.ModelClass.PlaceSearchModel;
import com.omninos.saowari.MyViewModelClasses.PlaceViewModel;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.adapters.PlaceAdapter;
import com.omninos.saowari.servicess.MySerives;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchPlaceActivity extends AppCompatActivity {


    private EditText search_bar;
    final List<String> addressesList = new ArrayList<>();
    private RecyclerView recyclerView;
    private PlaceViewModel viewModel;
    private PlaceAdapter adapter;
    private String Type;
    private RelativeLayout mainLayout;
    private TextView dragLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_place);

        stopService(new Intent(SearchPlaceActivity.this, MySerives.class));
        Intent intent = new Intent(SearchPlaceActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        viewModel = ViewModelProviders.of(this).get(PlaceViewModel.class);
        initView();
        SetUps();

        mainLayout = findViewById(R.id.mainLayout);


    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void initView() {
        search_bar = findViewById(R.id.search_bar);
        recyclerView = findViewById(R.id.recyclerView);

        Type = getIntent().getStringExtra("Type");
        dragLocation = findViewById(R.id.dragLocation);
        if (Type.equalsIgnoreCase("3")) {
            dragLocation.setVisibility(View.GONE);
        }

        dragLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchPlaceActivity.this, SelectLocationThroughDropActivity.class);
                intent.putExtra("Type", Type);
                startActivity(intent);
            }
        });
    }

    private void SetUps() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 2) {
                    recyclerView.setVisibility(View.VISIBLE);
//                    Toast.makeText(SearchPlaceActivity.this, s.toString(), Toast.LENGTH_SHORT).show();
                    findPlace(s.toString());
                } else {
                    recyclerView.setVisibility(View.GONE);
                    addressesList.clear();
                }
            }
        };


        search_bar.addTextChangedListener(textWatcher);
    }

    private void findPlace(String s) {
        Map<String, String> location = new HashMap<>();

        location.put("input", s);
        location.put("key", SearchPlaceActivity.this.getResources().getString(R.string.map_key));
        location.put("components", "country:bd");

        viewModel.placeSearchModelLiveData(SearchPlaceActivity.this, location).observe(SearchPlaceActivity.this, new Observer<PlaceSearchModel>() {
            @Override
            public void onChanged(@Nullable PlaceSearchModel placeSearchModel) {
                if (placeSearchModel.getStatus().equalsIgnoreCase("OK")) {

                    if (addressesList != null) {
                        addressesList.clear();
                    }
                    int size = placeSearchModel.getPredictions().size();

                    for (int i = 0; i < size; i++) {
                        addressesList.add(placeSearchModel.getPredictions().get(i).getDescription());
                    }
                    adapter = new PlaceAdapter(SearchPlaceActivity.this, addressesList, new PlaceAdapter.SelectPlace() {
                        @Override
                        public void Choose(final int position) {
                            Map<String, String> distanceMatrix = new HashMap<>();
                            distanceMatrix.put("address", addressesList.get(position));
                            distanceMatrix.put("key", SearchPlaceActivity.this.getResources().getString(R.string.map_key));

                            viewModel.getLatLngModelLiveData(SearchPlaceActivity.this, distanceMatrix).observe(SearchPlaceActivity.this, new Observer<GetLatLngModel>() {
                                @Override
                                public void onChanged(@Nullable GetLatLngModel getLatLngModel) {
                                    if (getLatLngModel.getStatus().equalsIgnoreCase("OK")) {
                                        if (Type.equalsIgnoreCase("1")) {
                                            App.getSinltonPojo().setPickUpPoint(addressesList.get(position));
                                            App.getSinltonPojo().setSouceLat(String.valueOf(getLatLngModel.getResults().get(0).getGeometry().getLocation().getLat()));
                                            App.getSinltonPojo().setSourceLng(String.valueOf(getLatLngModel.getResults().get(0).getGeometry().getLocation().getLng()));
                                            onBackPressed();
                                        } else if (Type.equalsIgnoreCase("2")) {
                                            App.getSinltonPojo().setDropPoint(addressesList.get(position));
                                            App.getSinltonPojo().setDestiLat(String.valueOf(getLatLngModel.getResults().get(0).getGeometry().getLocation().getLat()));
                                            App.getSinltonPojo().setDestiLng(String.valueOf(getLatLngModel.getResults().get(0).getGeometry().getLocation().getLng()));
                                            onBackPressed();

                                        } else if (Type.equalsIgnoreCase("3")) {
                                            App.getSinltonPojo().setEditAddress(addressesList.get(position));
                                            App.getSinltonPojo().setEditLat(String.valueOf(getLatLngModel.getResults().get(0).getGeometry().getLocation().getLat()));
                                            App.getSinltonPojo().setEditlng(String.valueOf(getLatLngModel.getResults().get(0).getGeometry().getLocation().getLng()));
                                            onBackPressed();
                                        }
//                                        Toast.makeText(SearchPlaceActivity.this,String.valueOf( getLatLngModel.getResults().get(0).getGeometry().getLocation().getLat()), Toast.LENGTH_SHORT).show();
                                    } else {

                                    }
                                }
                            });
                        }
                    });
                    recyclerView.setAdapter(adapter);

                } else {

                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (App.getSinltonPojo().getLocationStatus() != null) {
            onBackPressed();
        }
    }
}
