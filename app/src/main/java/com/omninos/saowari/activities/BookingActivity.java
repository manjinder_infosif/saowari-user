package com.omninos.saowari.activities;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.ActivityManager;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.omninos.saowari.Fragments.DriverRatingFragment;
import com.omninos.saowari.ModelClass.GetDriverCurrentLocation;
import com.omninos.saowari.MyViewModelClasses.BookingViewModel;
import com.omninos.saowari.MyViewModelClasses.ContactViewModel;
import com.omninos.saowari.MyViewModelClasses.LoginRegisterViewModel;
import com.omninos.saowari.MyViewModelClasses.VehicalViewModel;
import com.omninos.saowari.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.infideap.drawerbehavior.AdvanceDrawerLayout;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.directionApi.DirectionPojo;
import com.omninos.saowari.directionApi.Leg;
import com.omninos.saowari.directionApi.Route;
import com.omninos.saowari.directionApi.Step;
import com.omninos.saowari.servicess.LocationService;
import com.omninos.saowari.servicess.MySerives;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class BookingActivity extends AppCompatActivity implements View.OnClickListener, OnMapReadyCallback {

    private AdvanceDrawerLayout drawer;
    private GoogleMap map;
    private ImageView appbarMenu, callHome, msgHome, cancelBooking, shareBooking, emergencyCall, editProfile;
    private BookingViewModel viewModel;
    private BookingActivity activity;

    float slideX;
    private RelativeLayout allMain, firstLayout;
    private LinearLayout nvMyRide, navMyWallet, navSetting, navInviteFriends, navCallsChats, navLogout, navRide;
    //    private ImageView appbarMenu, , cancelBooking;
    private LinearLayout oopsLayout;


    private LatLng latLngSouce, latLngDesti;
    private Marker source, destination, drivers;
    private VehicalViewModel vehicalViewModel;
    private PolylineOptions polylineOptions;
    private Polyline polyline;
    private int LocationChangeVariable = 0, LocationChangeVariable1 = 0, LocationChangeVariable2 = 0, LocationChangeVariable3 = 0;
    private CircleImageView arriveimageDriverBottomSheet1;
    private TextView arrivenameDriverBottomSheet, arrivecarInfoBottomSheet, arriveotpRider, user_number, User_name;

    private CircleImageView imageDriverBottomSheet;
    private RatingBar arrivedriverRatingBottomSheet;

    //To calculate distance of to gmap points
    double _radiusEarthMiles = 3959;
    double _radiusEarthKM = 6371;
    double _m2km = 1.60934;
    double _toRad = Math.PI / 180;
    private double _centralAngle;

    private double Lat = 0.0, Lan = 0.0;
    //    private RegisterAndLoginViewModel logout;
    private String UserMobileNumber, driverName;
    private String reason = "";
    private LoginRegisterViewModel viewModel1;
    private ContactViewModel contactViewModel;
    private BookingViewModel bookingViewModel;

    private String callStartTime = "", callEndTime = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);

        activity = BookingActivity.this;

        stopService(new Intent(BookingActivity.this, MySerives.class));
        Intent intent = new Intent(BookingActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        contactViewModel = ViewModelProviders.of(this).get(ContactViewModel.class);
        bookingViewModel = ViewModelProviders.of(this).get(BookingViewModel.class);

        viewModel1 = ViewModelProviders.of(this).get(LoginRegisterViewModel.class);
        viewModel = ViewModelProviders.of(this).get(BookingViewModel.class);
        vehicalViewModel = ViewModelProviders.of(this).get(VehicalViewModel.class);


        initView();
        SetUp();
        services();


    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void initView() {
        drawer = findViewById(R.id.drawer);
//        drawer.setScrimColor(Color.TRANSPARENT);

        drawer.setViewScale(Gravity.START, 0.9f);
        drawer.setRadius(Gravity.START, 35);
        drawer.setViewElevation(Gravity.START, 20);
        allMain = findViewById(R.id.allMain);


        //side menu
        appbarMenu = findViewById(R.id.appbarMenu);
        nvMyRide = findViewById(R.id.nvMyRide);
        navMyWallet = findViewById(R.id.navMyWallet);
        navSetting = findViewById(R.id.navSetting);
        navInviteFriends = findViewById(R.id.navInviteFriends);
        editProfile = findViewById(R.id.editProfile);
        navCallsChats = findViewById(R.id.navCallsChats);
        navRide = findViewById(R.id.navRide);


        //down layout
        cancelBooking = findViewById(R.id.cancelBooking);


        oopsLayout = findViewById(R.id.oopsLayout);
        firstLayout = findViewById(R.id.firstLayout);
        navLogout = findViewById(R.id.navLogout);


        user_number = findViewById(R.id.user_number);
        User_name = findViewById(R.id.User_name);
        imageDriverBottomSheet = findViewById(R.id.userImage);


        arriveimageDriverBottomSheet1 = findViewById(R.id.arriveimageDriverBottomSheet1);
        arrivenameDriverBottomSheet = findViewById(R.id.arrivenameDriverBottomSheet);
        arrivecarInfoBottomSheet = findViewById(R.id.arrivecarInfoBottomSheet);
        arriveotpRider = findViewById(R.id.arriveotpRider);

        callHome = findViewById(R.id.callHome);
        callHome.setOnClickListener(this);

        msgHome = findViewById(R.id.msgHome);
        msgHome.setOnClickListener(this);

        cancelBooking = findViewById(R.id.cancelBooking);
        cancelBooking.setOnClickListener(this);


        shareBooking = findViewById(R.id.shareBooking);
        shareBooking.setOnClickListener(this);

        emergencyCall = findViewById(R.id.emergencyCall);
//        emergencyCall.setImageResource(R.drawable.ic_emergency_call);
        emergencyCall.setOnClickListener(this);

        arrivedriverRatingBottomSheet = findViewById(R.id.arrivedriverRatingBottomSheet);

    }

    private void SetUp() {
        nvMyRide.setOnClickListener(this);
        appbarMenu.setOnClickListener(this);
        navMyWallet.setOnClickListener(this);
        navSetting.setOnClickListener(this);
        navInviteFriends.setOnClickListener(this);
        editProfile.setOnClickListener(this);

        cancelBooking.setOnClickListener(this);
        navCallsChats.setOnClickListener(this);
        navLogout.setOnClickListener(this);
        navRide.setOnClickListener(this);
    }


    private void setMap() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapHome);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
//        map.setMyLocationEnabled(true);
        if (App.getSinltonPojo().getCurrentLatlng() != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(App.getSinltonPojo().getCurrentLatlng().latitude, App.getSinltonPojo().getCurrentLatlng().longitude), 12));
        } else {
            services();
        }
    }

    private void services() {
        if (App.getSinltonPojo().getCurrentLatlng() != null) {
            setMap();
        } else {
            stopService(new Intent(BookingActivity.this, LocationService.class));
//            startService(new Intent(activity,MyLocationServices.class));
            Intent intent = new Intent(BookingActivity.this, LocationService.class);
            if (!isMyServiceRunning(intent.getClass())) {
                startService(intent);
                setMap();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nvMyRide:
                startActivity(new Intent(BookingActivity.this, MyRidesActivity.class));
                break;

            case R.id.appbarMenu:
                drawer.openDrawer(Gravity.START);
                break;

            case R.id.navMyWallet:
                startActivity(new Intent(BookingActivity.this, MyWalletActivity.class));
                break;

            case R.id.navSetting:
                startActivity(new Intent(BookingActivity.this, SettingActivity.class));
                break;

            case R.id.navInviteFriends:
                startActivity(new Intent(BookingActivity.this, InviteFriendsActivity.class));
                break;

            case R.id.editProfile:
                startActivity(new Intent(BookingActivity.this, ProfileActivity.class));
                break;

            case R.id.cancelBooking:
                dialogCancelBooking();
                break;

            case R.id.navCallsChats:
                startActivity(new Intent(BookingActivity.this, ChatAndCallsActivity.class));
                break;
            case R.id.navLogout:
                viewModel1.LogOut(activity, App.getAppPreference().GetString(ConstantData.USERID)).observe(BookingActivity.this, new Observer<Map>() {
                    @Override
                    public void onChanged(@Nullable Map map) {
                        if (map.get("success").toString().equalsIgnoreCase("1")) {
                            App.getAppPreference().Logout();
                            startActivity(new Intent(BookingActivity.this, LoginActivity.class));
                            finishAffinity();
                        } else {
                            CommonUtils.showSnackbarAlert(navLogout, "Try Again");
                        }
                    }
                });
                break;

            case R.id.callHome:
                Calendar c = Calendar.getInstance();
                SimpleDateFormat dateformat = new SimpleDateFormat("hh:mm:aa");
                String datetime = dateformat.format(c.getTime());
                System.out.println("My Start Time: " + datetime);
                callStartTime = datetime;
                App.getSinltonPojo().setCallStatus("1");
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + App.getAppPreference().GetString(ConstantData.DrIVER_NUMBER)));
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
                break;

            case R.id.emergencyCall:
                CallToFriend();
                break;

            case R.id.shareBooking:
                ShareDriverInformation();
                break;

            case R.id.msgHome:
                Intent msgIntent = new Intent(BookingActivity.this, ChatActivity.class);
                msgIntent.putExtra("driver", App.getAppPreference().GetString(ConstantData.DRIVER_ID));
                msgIntent.putExtra("Title", arrivenameDriverBottomSheet.getText().toString());
                msgIntent.putExtra("status", "0");
                startActivity(msgIntent);
                break;

            case R.id.navRide:
                Intent intent1 = new Intent(activity, SelectLanguageActivity.class);
                intent1.putExtra("Type", "1");
                startActivity(intent1);
                break;

        }


    }

    private void CallToFriend() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + App.getAppPreference().GetString(ConstantData.EMERGENCY_CONTACT)));
        startActivity(intent);
    }


    private void dialogCancelBooking() {

        final Dialog dialog = new Dialog(BookingActivity.this);
        dialog.setContentView(R.layout.custom_cancel_layout);

        RelativeLayout rsnLostItem, rsnFareIssue, rsnRouteFeedback, rsnDriverFeedback, rsnVehicleFeedback,
                rsnRecieptAndPayment, rsnPromotion;
        final ImageView checkFareIssue, checkLostItem, checkRouteFeedback, checkDriverFeedback, checkVehicleFeedback,
                checkRecieptAndPayment, checkPromotion;

        checkFareIssue = dialog.findViewById(R.id.checkFareIssue);
        checkLostItem = dialog.findViewById(R.id.checkLostItem);
        checkRouteFeedback = dialog.findViewById(R.id.checkRouteFeedback);
        checkDriverFeedback = dialog.findViewById(R.id.checkDriverFeedback);
        checkVehicleFeedback = dialog.findViewById(R.id.checkVehicleFeedback);
        checkRecieptAndPayment = dialog.findViewById(R.id.checkRecieptAndPayment);
        checkPromotion = dialog.findViewById(R.id.checkPromotion);

        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);


        Button cancelBtnPopupCancel = dialog.findViewById(R.id.cancelBtnPopupCancel);
        cancelBtnPopupCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button doneBtnPopupCancel = dialog.findViewById(R.id.doneBtnPopupCancel);
        doneBtnPopupCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bookingViewModel.cancleJob(BookingActivity.this, App.getAppPreference().GetString(ConstantData.USERID), reason).observe(BookingActivity.this, new Observer<Map>() {
                    @Override
                    public void onChanged(@Nullable Map map) {
                        if (map.get("success").equals("1")) {
                            App.getSinltonPojo().setCancelrequest("1");
                            App.getAppPreference().SaveString(ConstantData.JOB_ID, "");
                            App.getAppPreference().SaveString("MainData","0");
                            startActivity(new Intent(BookingActivity.this, MainActivity.class));
                        } else {
                            Toast.makeText(BookingActivity.this, map.get("message").toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                dialog.dismiss();
            }
        });

        rsnLostItem = dialog.findViewById(R.id.rsnLostItem);
        rsnLostItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.VISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
//                    reason = "Lost Item";
            }
        });
        rsnFareIssue = dialog.findViewById(R.id.rsnFareIssue);
        rsnFareIssue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.VISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
//                    reason = "Fare Issue";
            }
        });
        rsnRouteFeedback = dialog.findViewById(R.id.rsnRouteFeedback);
        rsnRouteFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.VISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
//                    reason = "Route Feedback";
            }
        });
        rsnDriverFeedback = dialog.findViewById(R.id.rsnDriverFeedback);
        rsnDriverFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.VISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
//                    reason = "Driver Feedback";
            }
        });
        rsnVehicleFeedback = dialog.findViewById(R.id.rsnVehicleFeedback);
        rsnVehicleFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.VISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
//                    reason = "Vehicle Feedback";
            }
        });

        rsnRecieptAndPayment = dialog.findViewById(R.id.rsnRecieptAndPayment);
        rsnRecieptAndPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.VISIBLE);
                checkPromotion.setVisibility(View.INVISIBLE);
//                    reason = "Receipt and Payment";
            }
        });
        rsnPromotion = dialog.findViewById(R.id.rsnPromotion);
        rsnPromotion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkLostItem.setVisibility(View.INVISIBLE);
                checkFareIssue.setVisibility(View.INVISIBLE);
                checkRouteFeedback.setVisibility(View.INVISIBLE);
                checkDriverFeedback.setVisibility(View.INVISIBLE);
                checkVehicleFeedback.setVisibility(View.INVISIBLE);
                checkRecieptAndPayment.setVisibility(View.INVISIBLE);
                checkPromotion.setVisibility(View.VISIBLE);
//                    reason = "Promotions";
            }
        });

        dialog.show();

    }


    private void getDriverLocation() {

        viewModel.getDriverCurrentLocationLiveData(BookingActivity.this, App.getAppPreference().GetString(ConstantData.JOB_ID)).observe(BookingActivity.this, new Observer<GetDriverCurrentLocation>() {
            @Override
            public void onChanged(@Nullable GetDriverCurrentLocation getDriverCurrentLocation) {
                if (getDriverCurrentLocation.getSuccess().equalsIgnoreCase("1")) {

                    if (App.getSinltonPojo().getCurrentLatlng() != null) {

                        if (getDriverCurrentLocation.getDetails().getJobStatus().equalsIgnoreCase("3")) {
                            if (LocationChangeVariable == 0) {
                                Glide.with(activity).load(getDriverCurrentLocation.getDetails().getDriverImage()).into(arriveimageDriverBottomSheet1);
                                arrivenameDriverBottomSheet.setText(getDriverCurrentLocation.getDetails().getDriverName());
                                arrivecarInfoBottomSheet.setText(getDriverCurrentLocation.getDetails().getVehicleModel());
                                arriveotpRider.setText(getDriverCurrentLocation.getDetails().getDriverVehicleNumber());
                                arrivedriverRatingBottomSheet.setRating(getDriverCurrentLocation.getDetails().getDriverRating());
                                initDirectionAPI(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLat()), Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLong()), getDriverCurrentLocation.getDetails().getJobStatus(), getDriverCurrentLocation.getDetails().getDriverLatitude(), getDriverCurrentLocation.getDetails().getDriverLongitude());
                                LocationChangeVariable = 1;
                            } else {
                                if (drivers != null) {
                                    drivers.remove();
                                }
                                if (App.getSinltonPojo().getDriverLastLat() != null && App.getSinltonPojo().getDriverLastLng() != null) {
                                    initDirectionAPI(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLat()), Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLong()), getDriverCurrentLocation.getDetails().getJobStatus(), getDriverCurrentLocation.getDetails().getDriverLatitude(), getDriverCurrentLocation.getDetails().getDriverLongitude());
                                    List<LatLng> latLngData = new ArrayList<>();
                                    LatLng source = new LatLng(Double.parseDouble(App.getSinltonPojo().getDriverLastLat()), Double.parseDouble(App.getSinltonPojo().getDriverLastLng()));
                                    LatLng destination = new LatLng(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()));
                                    latLngData.add(source);
                                    latLngData.add(destination);
                                    if (HasMoved(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()))) {
                                        animateCarOnMap(latLngData);
                                    } else {
                                        if (drivers != null) {
                                            drivers.remove();
                                        }
                                        double dLon = (Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()) - App.getSinltonPojo().getCurrentLatlng().longitude);
                                        double y = Math.sin(dLon) * Math.cos(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()));
                                        double x = Math.cos(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.sin(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude())) - Math.sin(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.cos(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude())) * Math.cos(dLon);
                                        double brng = Math.toDegrees((Math.atan2(y, x)));
                                        brng = (360 - ((brng + 360) % 360));
                                        drivers = map.addMarker(new MarkerOptions()
                                                .position(new LatLng(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude())))
                                                .anchor(0.5f, 0.5f)
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));
                                        drivers.setRotation((float) brng);

                                    }
                                }
                                App.getSinltonPojo().setDriverLastLat(getDriverCurrentLocation.getDetails().getDriverLatitude());
                                App.getSinltonPojo().setDriverLastLng(getDriverCurrentLocation.getDetails().getDriverLongitude());
                            }


                        } else if (getDriverCurrentLocation.getDetails().getJobStatus().equalsIgnoreCase("2")) {

                            App.getAppPreference().SaveString(ConstantData.JOB_ID, "");
                            App.getAppPreference().SaveString("MainData","0");
                            startActivity(new Intent(activity, MainActivity.class));
                            finishAffinity();
                        } else if (getDriverCurrentLocation.getDetails().getJobStatus().equalsIgnoreCase("1")) {
                            if (LocationChangeVariable2 == 0) {
                                Glide.with(activity).load(getDriverCurrentLocation.getDetails().getDriverImage()).into(arriveimageDriverBottomSheet1);
                                arrivenameDriverBottomSheet.setText(getDriverCurrentLocation.getDetails().getDriverName());
                                arrivecarInfoBottomSheet.setText(getDriverCurrentLocation.getDetails().getVehicleModel());
                                arriveotpRider.setText(getDriverCurrentLocation.getDetails().getDriverVehicleNumber());
                                arrivedriverRatingBottomSheet.setRating(getDriverCurrentLocation.getDetails().getDriverRating());

                                initDirectionAPI(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLat()), Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLong()), getDriverCurrentLocation.getDetails().getJobStatus(), getDriverCurrentLocation.getDetails().getDriverLatitude(), getDriverCurrentLocation.getDetails().getDriverLongitude());
                                LocationChangeVariable2 = 1;
                            } else {
                                if (drivers != null) {
                                    drivers.remove();
                                }
                                if (App.getSinltonPojo().getDriverLastLat() != null && App.getSinltonPojo().getDriverLastLng() != null) {
                                    List<LatLng> latLngData = new ArrayList<>();
                                    LatLng source = new LatLng(Double.parseDouble(App.getSinltonPojo().getDriverLastLat()), Double.parseDouble(App.getSinltonPojo().getDriverLastLng()));
                                    LatLng destination = new LatLng(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()));
                                    latLngData.add(source);
                                    latLngData.add(destination);

                                    initDirectionAPI(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLat()), Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLong()), getDriverCurrentLocation.getDetails().getJobStatus(), getDriverCurrentLocation.getDetails().getDriverLatitude(), getDriverCurrentLocation.getDetails().getDriverLongitude());
                                    if (HasMoved(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()))) {
                                        animateCarOnMap(latLngData);
                                    } else {
                                        if (drivers != null) {
                                            drivers.remove();
                                        }
                                        double dLon = (Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()) - App.getSinltonPojo().getCurrentLatlng().longitude);
                                        double y = Math.sin(dLon) * Math.cos(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()));
                                        double x = Math.cos(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.sin(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude())) - Math.sin(App.getSinltonPojo().getCurrentLatlng().longitude) * Math.cos(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude())) * Math.cos(dLon);
                                        double brng = Math.toDegrees((Math.atan2(y, x)));
                                        brng = (360 - ((brng + 360) % 360));
                                        drivers = map.addMarker(new MarkerOptions()
                                                .position(new LatLng(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude())))
                                                .anchor(0.5f, 0.5f)
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));
                                        drivers.setRotation((float) brng);

                                    }
                                }
                                App.getSinltonPojo().setDriverLastLat(getDriverCurrentLocation.getDetails().getDriverLatitude());
                                App.getSinltonPojo().setDriverLastLng(getDriverCurrentLocation.getDetails().getDriverLongitude());
                            }
                        } else if (getDriverCurrentLocation.getDetails().getJobStatus().equalsIgnoreCase("4")) {

                            cancelBooking.setVisibility(View.GONE);
                            if (LocationChangeVariable1 == 0) {
                                Glide.with(activity).load(getDriverCurrentLocation.getDetails().getDriverImage()).into(arriveimageDriverBottomSheet1);
                                arrivenameDriverBottomSheet.setText(getDriverCurrentLocation.getDetails().getDriverName());
                                arrivecarInfoBottomSheet.setText(getDriverCurrentLocation.getDetails().getVehicleModel());
                                arriveotpRider.setText(getDriverCurrentLocation.getDetails().getDriverVehicleNumber());
                                arrivedriverRatingBottomSheet.setRating(getDriverCurrentLocation.getDetails().getDriverRating());

                                initDirectionAPI(Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLat()), Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLong()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDropLat()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDropLong()), getDriverCurrentLocation.getDetails().getJobStatus(), getDriverCurrentLocation.getDetails().getDriverLatitude(), getDriverCurrentLocation.getDetails().getDriverLongitude());
                                LocationChangeVariable1 = 1;
                            } else {
                                if (drivers != null) {
                                    drivers.remove();
                                }

                                if (App.getSinltonPojo().getDriverLastLat() != null && App.getSinltonPojo().getDriverLastLng() != null) {
                                    List<LatLng> latLngData = new ArrayList<>();
                                    LatLng source = new LatLng(Double.parseDouble(App.getSinltonPojo().getDriverLastLat()), Double.parseDouble(App.getSinltonPojo().getDriverLastLng()));
                                    LatLng destination = new LatLng(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()));
                                    latLngData.add(source);
                                    latLngData.add(destination);

                                    initDirectionAPI(Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLat()), Double.parseDouble(getDriverCurrentLocation.getDetails().getPicLong()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDropLat()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDropLong()), getDriverCurrentLocation.getDetails().getJobStatus(), getDriverCurrentLocation.getDetails().getDriverLatitude(), getDriverCurrentLocation.getDetails().getDriverLongitude());

                                    if (HasMoved(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()))) {
                                        animateCarOnMap(latLngData);
                                    } else {
                                        if (drivers != null) {
                                            drivers.remove();
                                        }
                                        double dLon = (Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude()) - App.getSinltonPojo().getCurrentLatlng().longitude);
                                        double y = Math.sin(dLon) * Math.cos(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()));
                                        double x = Math.cos(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.sin(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude())) - Math.sin(App.getSinltonPojo().getCurrentLatlng().latitude) * Math.cos(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude())) * Math.cos(dLon);
                                        double brng = Math.toDegrees((Math.atan2(y, x)));
                                        brng = (360 - ((brng + 360) % 360));
//
                                        drivers = map.addMarker(new MarkerOptions()
                                                .position(new LatLng(Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLatitude()), Double.parseDouble(getDriverCurrentLocation.getDetails().getDriverLongitude())))
                                                .anchor(0.5f, 0.5f)
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));
                                        drivers.setRotation((float) brng);
                                    }
                                }
                                App.getSinltonPojo().setDriverLastLat(getDriverCurrentLocation.getDetails().getDriverLatitude());
                                App.getSinltonPojo().setDriverLastLng(getDriverCurrentLocation.getDetails().getDriverLongitude());
                            }

                        } else if (getDriverCurrentLocation.getDetails().getJobStatus().equalsIgnoreCase("5") || getDriverCurrentLocation.getDetails().getJobStatus().equalsIgnoreCase("6")) {

                            cancelBooking.setVisibility(View.GONE);
                            if (LocationChangeVariable3 == 0) {
                                DriverRatingFragment fragment = new DriverRatingFragment();
                                Bundle args = new Bundle();
                                args.putString("DriverName", getDriverCurrentLocation.getDetails().getDriverName());
                                args.putString("DriverImage", getDriverCurrentLocation.getDetails().getDriverImage());
                                args.putString("JobID", getDriverCurrentLocation.getDetails().getJobId());
                                args.putString("DriverId", getDriverCurrentLocation.getDetails().getDriverId());
                                fragment.setArguments(args);
                                fragment.show(getSupportFragmentManager(), "Hii");
                                LocationChangeVariable3 = 1;

//                        DriverRatingFragment fragment = new DriverRatingFragment();
//                        Bundle args = new Bundle();
//                        args.putString("DriverName", getDriverCurrentLocation.getDetails().getDriverName());
//                        args.putString("DriverImage", getDriverCurrentLocation.getDetails().getDriverImage());
//                        args.putString("JobID", getDriverCurrentLocation.getDetails().getJobId());
//                        args.putString("DriverId", getDriverCurrentLocation.getDetails().getDriverId());
//                        fragment.setArguments(args);
//                        fragment.show(getSupportFragmentManager(), "Hii");
                            }
                        }
                    }
                }
            }
        });

    }

    private void initDirectionAPI(final double sourceLat, final double sourceLng, final double destiLat, final double destiLng, final String status, final String driverCurrentLat, final String DriverCurrentLng) {


        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                double sourceLatitude = 0.0;
                double sourceLongitude = 0.0;
                double destinationLatitude = 0.0;
                double destinationLongitude = 0.0;


                if (status.equalsIgnoreCase("3") || status.equalsIgnoreCase("1")) {

                    if (map != null) {
                        map.clear();
                    }
                    if (polyline != null) {
                        polyline.remove();

                    }
                    sourceLatitude = Double.parseDouble(driverCurrentLat);
                    sourceLongitude = Double.parseDouble(DriverCurrentLng);

                    destinationLatitude = destiLat;
                    destinationLongitude = destiLng;

                    latLngDesti = new LatLng(destiLat, destiLng);
                    destination = map.addMarker(new MarkerOptions()
                            .position(latLngDesti)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker)));

                } else if (status.equalsIgnoreCase("5") || status.equalsIgnoreCase("6")) {

                    if (map != null) {
                        map.clear();
                    }
                } else {
//                    if (LocationChangeVariable1 == 1) {

                    if (map != null) {
                        map.clear();
                    }
                    sourceLatitude = Double.parseDouble(driverCurrentLat);
                    sourceLongitude = Double.parseDouble(DriverCurrentLng);

                    destinationLatitude = destiLat;
                    destinationLongitude = destiLng;

                    latLngSouce = new LatLng(sourceLat, sourceLng);
                    source = map.addMarker(new MarkerOptions()
                            .position(latLngSouce)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.source_marker)));


                    latLngDesti = new LatLng(destiLat, destiLng);
                    destination = map.addMarker(new MarkerOptions()
                            .position(latLngDesti)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.destination_marker)));
                }


                if (App.getSinltonPojo().getCurrentLatlng() != null) {

                    Map<String, String> data = new HashMap<>();
                    data.put("origin", sourceLatitude + "," + sourceLongitude);
                    data.put("destination", destinationLatitude + "," + destinationLongitude);
                    data.put("key", BookingActivity.this.getResources().getString(R.string.map_key));

                    if (CommonUtils.isNetworkConnected(activity)) {
                        vehicalViewModel.getRoute(activity, data).observe(BookingActivity.this, new Observer<DirectionPojo>() {
                            @Override
                            public void onChanged(@Nullable DirectionPojo directionPojo) {
                                List<Route> routeList = directionPojo.getRoutes();

                                polylineOptions = new PolylineOptions();
                                polylineOptions.width(5).color(Color.BLUE).geodesic(true);
//
                                for (int i = 0; i < routeList.size(); i++) {
                                    List<Leg> legList = routeList.get(i).getLegs();
                                    for (int j = 0; j < legList.size(); j++) {
                                        List<Step> stepList = legList.get(j).getSteps();
                                        for (int k = 0; k < stepList.size(); k++) {
                                            String polyline = stepList.get(k).getPolyline().getPoints();
                                            List<LatLng> latlngList = decodePolyline(polyline);
                                            for (int z = 0; z < latlngList.size(); z++) {
                                                LatLng point = latlngList.get(z);
                                                polylineOptions.add(point);
                                            }
                                        }
                                    }
                                }
                                polyline = map.addPolyline(polylineOptions);

                            }
                        });
                    } else {
                        Toast.makeText(activity, "Network Issue", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }


    private List<LatLng> decodePolyline(String polyline) {
        List<LatLng> poly = new ArrayList<>();
        int index = 0, len = polyline.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = polyline.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = polyline.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


    public boolean HasMoved(double newLat, double newLan) {
        double significantDistance = 30;
        double currentDistance;

        currentDistance = DistanceMilesSEP(Lat, Lan, newLat, newLan);
        currentDistance = currentDistance * _m2km * 1000;

        if (currentDistance < significantDistance) {
            return false;
        } else {
            Lat = newLat;
            Lan = newLan;
            return true;
        }
    }

    public double DistanceMilesSEP(double Lat1, double Lon1, double Lat2, double Lon2) {
        try {
            double _radLat1 = Lat1 * _toRad;
            double _radLat2 = Lat2 * _toRad;
            double _dLat = (_radLat2 - _radLat1);
            double _dLon = (Lon2 - Lon1) * _toRad;

            double _a = (_dLon) * Math.cos((_radLat1 + _radLat2) / 2);

            // central angle, aka arc segment angular distance
            _centralAngle = Math.sqrt(_a * _a + _dLat * _dLat);

            // great-circle (orthodromic) distance on Earth between 2 points
        } catch (Exception e) {
            e.printStackTrace();
        }
        return _radiusEarthMiles * _centralAngle;
    }


    private void animateCarOnMap(final List<LatLng> latLngs) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : latLngs) {
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();
        CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 2);
        map.animateCamera(mCameraUpdate);

        if (drivers != null) {
            drivers.remove();
        }

        drivers = map.addMarker(new MarkerOptions().position(latLngs.get(0))
                .flat(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bike_logo)));

        drivers.setPosition(latLngs.get(0));

        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
        valueAnimator.setDuration(1000);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Float v = valueAnimator.getAnimatedFraction();
                double lng = v * latLngs.get(1).longitude + (1 - v)
                        * latLngs.get(0).longitude;
                double lat = v * latLngs.get(1).latitude + (1 - v)
                        * latLngs.get(0).latitude;
                LatLng newPos = new LatLng(lat, lng);
                drivers.setPosition(newPos);
                drivers.setAnchor(0.5f, 0.5f);
                drivers.setRotation(getBearing(latLngs.get(0), newPos));
                map.animateCamera(CameraUpdateFactory.newCameraPosition
                        (new CameraPosition.Builder().target(newPos)
                                .zoom(15.5f).build()));
            }
        });
        valueAnimator.start();
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;

    }

    @Override
    protected void onResume() {
        super.onResume();
        getDriverLocation();
        Glide.with(activity).load(App.getAppPreference().getLoginDetail().getDetails().getImage()).into(imageDriverBottomSheet);
        user_number.setText(App.getAppPreference().getLoginDetail().getDetails().getPhone());
        User_name.setText(App.getAppPreference().getLoginDetail().getDetails().getName());
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (App.getSinltonPojo().getCallStatus() != null) {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat dateformat = new SimpleDateFormat("dd MMMM,yyyy");
            String datetime = dateformat.format(c.getTime());
            System.out.println("My End Time: " + datetime);

            contactViewModel.Calls(BookingActivity.this, App.getAppPreference().GetString(ConstantData.USERID), App.getAppPreference().GetString(ConstantData.DRIVER_ID), callStartTime, datetime).observe(BookingActivity.this, new Observer<Map>() {
                @Override
                public void onChanged(@Nullable Map map) {
                    if (map.get("success").toString().equalsIgnoreCase("1")) {
                        App.getSinltonPojo().setCallStatus(null);
                    } else {

                    }
                }
            });
        }
    }

    private void ShareDriverInformation() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Driver Name: " + arrivenameDriverBottomSheet.getText().toString() + "\n Driver Number: " + App.getAppPreference().GetString(ConstantData.DrIVER_NUMBER));
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

}
