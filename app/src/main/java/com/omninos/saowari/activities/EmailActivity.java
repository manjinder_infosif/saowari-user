package com.omninos.saowari.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.saowari.R;

public class EmailActivity extends AppCompatActivity implements View.OnClickListener {


    private ImageView firstIcon, lastIcon;
    private TextView title;
    private ImageView nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);

        initView();
        SetUp();
    }

    private void initView() {
        lastIcon = findViewById(R.id.lastIcon);
        firstIcon = findViewById(R.id.firstIcon);
        title = findViewById(R.id.title);
        nextButton = findViewById(R.id.nextButton);

    }

    private void SetUp() {
        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        firstIcon.setOnClickListener(this);
        title.setText("Email");

        nextButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstIcon:
                onBackPressed();
                break;

            case R.id.nextButton:
                startActivity(new Intent(EmailActivity.this, MainActivity.class));
                break;
        }
    }
}
