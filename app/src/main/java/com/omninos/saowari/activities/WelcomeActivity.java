package com.omninos.saowari.activities;

import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.omninos.saowari.R;
import com.omninos.saowari.adapters.ConsultMyViewPagerAdapter;

import me.relex.circleindicator.CircleIndicator;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener, LocationListener {

    int[] layouts = {R.layout.welcome_first_layout, R.layout.welcome_second_layout, R.layout.welcome_third_layout};
    private ViewPager viewPager;
    private ConsultMyViewPagerAdapter myViewPagerAdapter;
    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        nextButton = findViewById(R.id.nextButton);
        nextButton.setOnClickListener(this);

        viewPager = findViewById(R.id.viewpager);
        myViewPagerAdapter = new ConsultMyViewPagerAdapter(layouts, WelcomeActivity.this);
        viewPager.setAdapter(myViewPagerAdapter);

        CircleIndicator indicator = findViewById(R.id.circle_indicator_laboratory);
        indicator.setViewPager(viewPager);

        myViewPagerAdapter.registerDataSetObserver(indicator.getDataSetObserver());


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        try {
                            sleep(2000);
                            startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
                            finish();

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                };
                if (i == 2) {
                    thread.start();
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextButton:
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
