package com.omninos.saowari.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.omninos.saowari.ModelClass.GetContactNumber;
import com.omninos.saowari.MyViewModelClasses.ContactViewModel;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.adapters.ContactAdapter;
import com.omninos.saowari.servicess.MySerives;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EmergencyContactActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int PICK_CONTACT = 101;
    private static final int READ_CONTACT_PERMISSION_CODE = 102;

    private RecyclerView rv_emergency_contact;
    private Button addButton;
    private List<GetContactNumber> getContactNumbers = new ArrayList<>();
    private List<GetContactNumber.Detail> detailList = new ArrayList<>();
    private ContactViewModel viewModel;
    private ContactAdapter adapter;

    List<String> numberList = new ArrayList<>();
    List<String> nameList = new ArrayList<>();
    String contactNumber, contactName;
    JSONArray jsonArray = new JSONArray();
    JSONObject jsonObject;
    private ImageView firstIcon;
    private TextView addNewContact,title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contact);

        viewModel = ViewModelProviders.of(this).get(ContactViewModel.class);

        stopService(new Intent(EmergencyContactActivity.this, MySerives.class));
        Intent intent = new Intent(EmergencyContactActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        InitView();
        SetUps();
        getList();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void getList() {
        viewModel.getContactNumberLiveData(EmergencyContactActivity.this, App.getAppPreference().GetString(ConstantData.USERID)).observe(EmergencyContactActivity.this, new Observer<GetContactNumber>() {
            @Override
            public void onChanged(@Nullable GetContactNumber getContactNumber) {
                if (getContactNumber.getSuccess().equalsIgnoreCase("1")) {
                    int size = getContactNumber.getDetails().size();

                    for (int i = 0; i < size; i++) {
                        App.getAppPreference().SaveString(ConstantData.EMERGENCY_CONTACT, getContactNumber.getDetails().get(0).getPhone());
                        GetContactNumber model = new GetContactNumber();
                        GetContactNumber.Detail detail = new GetContactNumber.Detail();

                        detail.setId(getContactNumber.getDetails().get(i).getId());
                        detail.setName(getContactNumber.getDetails().get(i).getName());
                        detail.setPhone(getContactNumber.getDetails().get(i).getPhone());

                        detailList.add(detail);
                        model.setDetails(detailList);
                        getContactNumbers.add(model);

                    }

                    adapter = new ContactAdapter(EmergencyContactActivity.this, getContactNumbers, "1");
                    rv_emergency_contact.setAdapter(adapter);
                } else {

                }
            }
        });
    }

    private void InitView() {
        rv_emergency_contact = findViewById(R.id.rv_emergency_contact);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(EmergencyContactActivity.this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_emergency_contact.setLayoutManager(linearLayoutManager);


//        appbarBack = findViewById(R.id.appbarBack);
//        appbarBack.setOnClickListener(this);
//        appbarText = findViewById(R.id.appbarText);

        addNewContact = findViewById(R.id.addnewContact);
        addNewContact.setOnClickListener(this);

        firstIcon = findViewById(R.id.firstIcon);
        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        firstIcon.setOnClickListener(this);

        title=findViewById(R.id.title);
        title.setText("Emergency Contacts");


    }

    private void SetUps() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstIcon:
                onBackPressed();
                break;

            case R.id.addnewContact:
                if (numberList.size() < 3) {
                    initContactReadPermission();
                } else {
                    Toast.makeText(EmergencyContactActivity.this, "You can Choose Maximum 3 Contact", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }

    private void initContactReadPermission() {
        try {
            if (ActivityCompat.checkSelfPermission(EmergencyContactActivity.this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(EmergencyContactActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, READ_CONTACT_PERMISSION_CODE);
            } else {
                getContactIntent();
            }

        } catch (Exception e) {

        }

    }

    private void getContactIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case READ_CONTACT_PERMISSION_CODE:
                Boolean read_contact = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (grantResults.length > 0 && read_contact) {
                    getContactIntent();

                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !shouldShowRequestPermissionRationale(permissions[0])) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EmergencyContactActivity.this);
                    builder.setTitle("Permissions");
                    builder.setMessage("Read Contact Permissions are Required");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //send to settings
                            // Toast.makeText(activity, "Go to Settings to Grant the Storage Permissions", Toast.LENGTH_LONG).show();
//                            sentToSettings = true;
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", EmergencyContactActivity.this.getPackageName(), null);
                            intent.setData(uri);
                            startActivityForResult(intent, READ_CONTACT_PERMISSION_CODE);
                        }
                    })
                            .create()
                            .show();
                } else {
                    initContactReadPermission();
                }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case PICK_CONTACT:
                if (resultCode == RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {

                        String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
                                    null, null);
                            phones.moveToFirst();
                            contactNumber = phones.getString(phones.getColumnIndex("data1"));

                        }
                        contactName = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                        numberList.add(contactNumber);
                        nameList.add(contactName);
                        jsonObject = new JSONObject();

                        try {
                            jsonObject.put("name", contactName);
                            jsonObject.put("phone", contactNumber);
                            jsonArray.put(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        rv_emergency_contact.setLayoutManager(new LinearLayoutManager(EmergencyContactActivity.this));
                        rv_emergency_contact.setNestedScrollingEnabled(false);
                        ContactAdapter emergencyContactListAdapter = new ContactAdapter(EmergencyContactActivity.this, numberList, nameList, "2");
                        rv_emergency_contact.setAdapter(emergencyContactListAdapter);

                        System.out.println("data is=" + jsonArray.length());
                        if (jsonArray.length() == 3) {
                            SendContactToserver(jsonArray);
                        }

                    }
                }
                break;
        }
    }

    private void SendContactToserver(JSONArray jsonArray) {
        viewModel.saveContactList(EmergencyContactActivity.this, App.getAppPreference().GetString(ConstantData.USERID), jsonArray).observe(EmergencyContactActivity.this, new Observer<Map>() {
            @Override
            public void onChanged(@Nullable Map map) {
                if (map.get("success").equals("1")) {
                    Toast.makeText(EmergencyContactActivity.this, "Add SuccessFull", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(EmergencyContactActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
