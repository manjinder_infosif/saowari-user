package com.omninos.saowari.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.servicess.MySerives;

import org.w3c.dom.Text;

public class InviteFriendsActivity extends AppCompatActivity implements View.OnClickListener {


    private ImageView firstIcon;
    private TextView title;
    private TextView code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite_friends);

        stopService(new Intent(InviteFriendsActivity.this, MySerives.class));
        Intent intent = new Intent(InviteFriendsActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        initView();
        Setup();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void initView() {
        firstIcon = findViewById(R.id.firstIcon);
        title = findViewById(R.id.title);
        code=findViewById(R.id.code);

    }

    private void Setup() {
        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        firstIcon.setOnClickListener(this);
        title.setText("Invite Friends");

        code.setText(App.getAppPreference().getLoginDetail().getDetails().getReferralCode());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstIcon:
                onBackPressed();
                break;
        }
    }
}
