package com.omninos.saowari.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.saowari.R;
import com.omninos.saowari.servicess.MySerives;

public class UpdateDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView nextButton;

    private ImageView firstIcon, lastIcon;
    private TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_detail);

        stopService(new Intent(UpdateDetailActivity.this, MySerives.class));
        Intent intent = new Intent(UpdateDetailActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        initView();
        SetUp();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void initView() {
        nextButton = findViewById(R.id.nextButton);
        lastIcon = findViewById(R.id.lastIcon);
        firstIcon = findViewById(R.id.firstIcon);
        title = findViewById(R.id.title);
    }

    private void SetUp() {
        nextButton.setOnClickListener(this);
        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        firstIcon.setOnClickListener(this);
        title.setText("Username");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextButton:
                startActivity(new Intent(UpdateDetailActivity.this, EmailActivity.class));
                break;
            case R.id.firstIcon:
                onBackPressed();
                break;
        }
    }
}
