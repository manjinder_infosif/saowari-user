package com.omninos.saowari.activities;

import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.omninos.saowari.ModelClass.ChatModelClass;
import com.omninos.saowari.ModelClass.GetConversionModel;
import com.omninos.saowari.ModelClass.SendMessageModel;
import com.omninos.saowari.MyViewModelClasses.MessegeViewModel;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.adapters.ChatInboxAdapter;
import com.omninos.saowari.servicess.MySerives;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView chatCycle;
    List<GetConversionModel.MessageDetail> models = new ArrayList<>();
    private ChatInboxAdapter chatAdapter;
    private EditText input_msg;
    private ImageView send, firstIcon;
    private TextView title;
    private MessegeViewModel viewModel;

    private TimerTask timerTask;
    private Timer timer;
    private String userId, driverId, Status;
    private RelativeLayout messageRL;
    private ProgressBar progress_circular;
    private TextView noData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        viewModel = ViewModelProviders.of(this).get(MessegeViewModel.class);
        stopService(new Intent(ChatActivity.this, MySerives.class));
        Intent intent = new Intent(ChatActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }

        driverId = getIntent().getStringExtra("driver");
        userId = App.getAppPreference().GetString(ConstantData.USERID);
        Status = getIntent().getStringExtra("status");

        initView();
        SetUp();
//        setChatAdapter();
        startTimer();

    }

    private void startTimer() {

        //set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
        initializeTimerTask();
        //schedule the timer, to wake up every 2 second
        timer.schedule(timerTask, 2000, 5000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                getConversationApi();

            }
        };
    }

    private void getConversationApi() {
        viewModel.getConversionModelLiveData(ChatActivity.this, userId, driverId).observe(ChatActivity.this, new Observer<GetConversionModel>() {
            @Override
            public void onChanged(@Nullable GetConversionModel getConversionModel) {
                if (getConversionModel.getSuccess().equalsIgnoreCase("1")) {
                    progress_circular.setVisibility(View.GONE);
                    noData.setVisibility(View.GONE);
                    chatCycle.setVisibility(View.VISIBLE);
                    if (getConversionModel.getMessageDetails() != null) {
                        models = getConversionModel.getMessageDetails();

                        chatAdapter = new ChatInboxAdapter(ChatActivity.this, getConversionModel.getMessageDetails());
                        chatCycle.setAdapter(chatAdapter);
                        int newMsgPosition = models.size() - 1;
                        chatAdapter.notifyItemInserted(newMsgPosition);
                        chatCycle.scrollToPosition(newMsgPosition);
                    } else {
                        progress_circular.setVisibility(View.GONE);

                        noData.setVisibility(View.VISIBLE);
                        chatCycle.setVisibility(View.GONE);
//                        CommonUtils.showSnackbarAlert(chatCycle, "No conversation");
                    }
                } else {
                    noData.setVisibility(View.VISIBLE);
                    chatCycle.setVisibility(View.GONE);
                    progress_circular.setVisibility(View.GONE);
//                    CommonUtils.showSnackbarAlert(chatCycle, getConversionModel.getMessage());
                }
            }
        });
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void initView() {
        chatCycle = findViewById(R.id.chatCycle);
        input_msg = findViewById(R.id.et_message);
        send = findViewById(R.id.sendInbox);
        progress_circular = findViewById(R.id.progress_circular);

        firstIcon = findViewById(R.id.firstIcon);
        title = findViewById(R.id.title);
        messageRL = findViewById(R.id.messageRL);
        noData = findViewById(R.id.noData);
        if (Status.equalsIgnoreCase("6")) {
            messageRL.setVisibility(View.INVISIBLE);
        } else {
            messageRL.setVisibility(View.VISIBLE);
        }

        chatCycle.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void SetUp() {

        send.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        chatCycle.setLayoutManager(linearLayoutManager);


        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        title.setText(getIntent().getStringExtra("Title"));
        firstIcon.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendInbox:
                sendMessage();
                break;
            case R.id.firstIcon:
                onBackPressed();
                break;
        }
    }

    private void sendMessage() {
        String data = input_msg.getText().toString();
        if (data.isEmpty()) {
            CommonUtils.showSnackbarAlert(input_msg, "enter message");
        } else {
            viewModel.message(ChatActivity.this, userId, driverId, data).observe(ChatActivity.this, new Observer<SendMessageModel>() {
                @Override
                public void onChanged(@Nullable SendMessageModel sendMessageModel) {
                    if (sendMessageModel.getMessage().equalsIgnoreCase("1")) {
                        input_msg.setText("");

                    } else {
                        input_msg.setText("");
                    }
                }
            });

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
    }
}
