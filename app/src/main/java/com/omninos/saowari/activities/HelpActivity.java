package com.omninos.saowari.activities;

import android.app.ActivityManager;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.omninos.saowari.ModelClass.HelpAndContactusModel;
import com.omninos.saowari.MyViewModelClasses.HelpAndContactUsViewModel;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.servicess.MySerives;

public class HelpActivity extends AppCompatActivity implements View.OnClickListener {


    private ImageView firstIcon;
    private TextView title;
    private HelpAndContactUsViewModel viewModel;
    private EditText name, mobileNumber, userEmail, message;
    private String nameString, numberString, emailString, messageString;
    private Button sendQuery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        viewModel = ViewModelProviders.of(this).get(HelpAndContactUsViewModel.class);

        stopService(new Intent(HelpActivity.this, MySerives.class));
        Intent intent = new Intent(HelpActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }
        initView();
        SetUp();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void initView() {
        firstIcon = findViewById(R.id.firstIcon);
        title = findViewById(R.id.title);

        name = findViewById(R.id.name);
        mobileNumber = findViewById(R.id.mobileNumber);
        userEmail = findViewById(R.id.userEmail);
        message = findViewById(R.id.message);
        sendQuery = findViewById(R.id.sendQuery);
        sendQuery.setOnClickListener(this);

    }

    private void SetUp() {
        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        firstIcon.setOnClickListener(this);
        title.setText("Help");


        name.setText(App.getAppPreference().getLoginDetail().getDetails().getName());
        mobileNumber.setText(App.getAppPreference().getLoginDetail().getDetails().getPhone());
        userEmail.setText(App.getAppPreference().getLoginDetail().getDetails().getEmail());


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstIcon:
                onBackPressed();
                break;
            case R.id.sendQuery:
                SendContactUsQuery();
                break;

        }
    }

    private void SendContactUsQuery() {
        nameString = name.getText().toString();
        numberString = mobileNumber.getText().toString();
        emailString = userEmail.getText().toString();
        messageString = message.getText().toString();
        if (nameString.isEmpty()) {
            CommonUtils.showSnackbarAlert(name, "enter name");
        } else if (numberString.isEmpty()) {
            CommonUtils.showSnackbarAlert(mobileNumber, "enter mobile number");
        } else if (emailString.isEmpty()) {
            CommonUtils.showSnackbarAlert(userEmail, "enter email");
        } else if (messageString.isEmpty()) {
            CommonUtils.showSnackbarAlert(message, "enter query");
        } else {
            viewModel.helpAndContactusModelLiveData(HelpActivity.this, "help", nameString, numberString, emailString, messageString, App.getAppPreference().GetString(ConstantData.USERID)).observe(HelpActivity.this, new Observer<HelpAndContactusModel>() {
                @Override
                public void onChanged(@Nullable HelpAndContactusModel helpAndContactusModel) {
                    if (helpAndContactusModel.getSuccess().equalsIgnoreCase("1")) {
                        CommonUtils.showSnackbarAlert(message, helpAndContactusModel.getMessage());
                        onBackPressed();
                    } else {
                        CommonUtils.showSnackbarAlert(message, helpAndContactusModel.getMessage());
                    }
                }
            });
        }
    }
}
