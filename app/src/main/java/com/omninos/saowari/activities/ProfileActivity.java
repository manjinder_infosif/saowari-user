package com.omninos.saowari.activities;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.servicess.MySerives;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {


    private ImageView firstIcon, lastIcon;
    private TextView title, userName, addressUser, complete_address, phoneNumber, emailUser;
    private CircleImageView driverProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        stopService(new Intent(ProfileActivity.this, MySerives.class));
        Intent intent = new Intent(ProfileActivity.this, MySerives.class);
        if (!isMyServiceRunning(intent.getClass())) {
            startService(intent);
        }


        initView();
        SetUp();
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }


    private void initView() {
        firstIcon = findViewById(R.id.firstIcon);
        title = findViewById(R.id.title);
        lastIcon = findViewById(R.id.lastIcon);

        userName = findViewById(R.id.userName);
        addressUser = findViewById(R.id.addressUser);
        complete_address = findViewById(R.id.complete_address);
        phoneNumber = findViewById(R.id.phoneNumber);
        emailUser = findViewById(R.id.emailUser);
        driverProfile = findViewById(R.id.driverProfile);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Glide.with(ProfileActivity.this).load(App.getAppPreference().getLoginDetail().getDetails().getImage()).into(driverProfile);
        userName.setText(App.getAppPreference().getLoginDetail().getDetails().getName());
        phoneNumber.setText(App.getAppPreference().getLoginDetail().getDetails().getPhone());
        emailUser.setText(App.getAppPreference().getLoginDetail().getDetails().getEmail());
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(Double.parseDouble(App.getAppPreference().getLoginDetail().getDetails().getLatitude()), Double.parseDouble(App.getAppPreference().getLoginDetail().getDetails().getLongitude()), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            addressUser.setText(address);
            complete_address.setText(address);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void SetUp() {
        firstIcon.setImageDrawable(getDrawable(R.drawable.ic_back));
        firstIcon.setOnClickListener(this);
        lastIcon.setImageDrawable(getDrawable(R.drawable.ic_pencil_edit_button));
        lastIcon.setOnClickListener(this);
        title.setText("Profile");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstIcon:
                onBackPressed();
                break;

            case R.id.lastIcon:
                startActivity(new Intent(ProfileActivity.this, EditProfileActivity.class));
                break;
        }
    }
}
