package com.omninos.saowari.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.omninos.saowari.BuildConfig;
import com.omninos.saowari.ModelClass.LoginRegisterModel;
import com.omninos.saowari.MyViewModelClasses.LoginRegisterViewModel;
import com.omninos.saowari.R;
import com.omninos.saowari.Util.App;
import com.omninos.saowari.Util.CommonUtils;
import com.omninos.saowari.Util.ConstantData;
import com.omninos.saowari.Util.ImageUtil;
import com.omninos.saowari.servicess.LocationService;
import com.omninos.saowari.servicess.MySerives;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView title;
    private Button signUp;
    private CircleImageView userImage;
    private EditText userName, userEmail, dateOfBirthData;
    private String Gender = "", Name, Email, DateOfBirth, UserimagePath = "";
    private SignUpActivity activity;
    private File photoFile;
    private static final int GALLERY_REQUEST = 101;

    private static final int CAMERA_REQUEST = 102;

    private Uri uri;

    private LoginRegisterViewModel viewModel;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        activity = SignUpActivity.this;

        token = FirebaseInstanceId.getInstance().getToken();

        stopService(new Intent(activity, MySerives.class));
        Intent intent1 = new Intent(activity, MySerives.class);
        if (!isMyServiceRunning1(intent1.getClass())) {
            startService(intent1);
        }

        viewModel = ViewModelProviders.of(this).get(LoginRegisterViewModel.class);

        if (App.getSinltonPojo().getCurrentLatlng() != null) {

        } else {
            stopService(new Intent(activity, LocationService.class));
            Intent intent = new Intent(activity, LocationService.class);
            if (!isMyServiceRunning(intent.getClass())) {
                startService(intent);
            }
        }

        initView();
        SetUp();
    }

    private boolean isMyServiceRunning1(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.i("isMyServiceRunning?", true + "");
                return true;
            }
        }
        Log.i("isMyServiceRunning?", false + "");
        return false;
    }

    private void initView() {
        title = findViewById(R.id.title);
        signUp = findViewById(R.id.signUp);
        dateOfBirthData = findViewById(R.id.dateOfBirthData);
        userImage = findViewById(R.id.userImage);
        userName = findViewById(R.id.userName);
        userEmail = findViewById(R.id.userEmail);
    }

    private void SetUp() {
        title.setText("Sign Up");
        signUp.setOnClickListener(this);
        dateOfBirthData.setOnClickListener(this);
        userImage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUp:
                if (App.getSinltonPojo().getCurrentLatlng() != null) {
                    UserRegister();
                } else {
                    CommonUtils.showSnackbarAlert(signUp, "Please enable Location");
                }
//                startActivity(new Intent(SignUpActivity.this, MainActivity.class));
//                finishAffinity();
                break;

            case R.id.dateOfBirthData:
                PickDate();
                break;

            case R.id.userImage:
                OpenImageData();
                break;
        }
    }

    private void OpenImageData() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent pictureIntent = new Intent(
                MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null) {
            //Create a file to store the image
            photoFile = null;
            photoFile = ImageUtil.getTemporaryCameraFile();
            if (photoFile != null) {
                Uri uri = FileProvider.getUriForFile(activity, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                startActivityForResult(pictureIntent,
                        CAMERA_REQUEST);
            }
        }
    }

    private void galleryIntent() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY_REQUEST);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALLERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {
                UserimagePath = ImageUtil.compressImage(photoFile.getAbsolutePath());
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 8;
                Bitmap mBitmapInsurance = BitmapFactory.decodeFile(photoFile.getAbsolutePath(), options);
//                userImage.setImageBitmap(mBitmapInsurance);
                Matrix matrix = new Matrix();
                matrix.postRotate(90);
                Bitmap rotated = Bitmap.createBitmap(mBitmapInsurance, 0, 0, mBitmapInsurance.getWidth(), mBitmapInsurance.getHeight(),
                        matrix, true);

                if (Build.VERSION.SDK_INT > 25 && Build.VERSION.SDK_INT < 27) {
                    // Do something for lollipop and above versions
                    userImage.setImageBitmap(rotated);
                } else {
                    // do something for phones running an SDK before lollipop
                    userImage.setImageBitmap(mBitmapInsurance);
                }
            }
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        if (data != null) {
            uri = data.getData();
            if (uri != null) {
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index_data = cursor.getColumnIndex(projection[0]);
                cursor.moveToFirst();
                UserimagePath = cursor.getString(column_index_data);
                Glide.with(activity).load("file://" + UserimagePath).into(userImage);
            }

        }
    }


    private void UserRegister() {
        Name = userName.getText().toString();
        Email = userEmail.getText().toString();
        DateOfBirth = dateOfBirthData.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[cC]+[oO]+[mM]";
        if (Name.isEmpty()) {
            CommonUtils.showSnackbarAlert(userName, "enter name");
        } else if (Email.isEmpty() || !Email.matches(emailPattern)) {
            CommonUtils.showSnackbarAlert(userEmail, "enter email");
        } else if (DateOfBirth.isEmpty()) {
            CommonUtils.showSnackbarAlert(dateOfBirthData, "choose date of birth");
        } else if (UserimagePath.isEmpty()) {
            CommonUtils.showSnackbarAlert(userImage, "choose image");
        } else if (Gender.isEmpty()) {
            CommonUtils.showSnackbarAlert(dateOfBirthData, "choose gender");
        } else {
            RequestBody nameBody = RequestBody.create(MediaType.parse("text/plain"), Name);
            RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), Email);
            RequestBody dateOfBirthBody = RequestBody.create(MediaType.parse("text/plain"), DateOfBirth);
            RequestBody phoneBody = RequestBody.create(MediaType.parse("text/plain"), App.getSinltonPojo().getMobilenumber());
            RequestBody genderBody = RequestBody.create(MediaType.parse("text/plain"), Gender);
            RequestBody device_typeBody = RequestBody.create(MediaType.parse("text/plain"), "android");
            RequestBody latitudeBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(App.getSinltonPojo().getCurrentLatlng().latitude));
            RequestBody longitudeBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(App.getSinltonPojo().getCurrentLatlng().longitude));
            RequestBody reg_id_body = RequestBody.create(MediaType.parse("text/plain"), token);
            RequestBody loginTypeBody = RequestBody.create(MediaType.parse("text/plain"), "normal");


            File file = new File(UserimagePath);

            final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            MultipartBody.Part userImage = MultipartBody.Part.createFormData("image", file.getName(), requestFile);

            viewModel.Register(activity, nameBody, phoneBody, emailBody, dateOfBirthBody, genderBody, device_typeBody, reg_id_body, latitudeBody, longitudeBody, loginTypeBody, userImage).observe(activity, new Observer<LoginRegisterModel>() {
                @Override
                public void onChanged(@Nullable LoginRegisterModel loginRegisterModel) {
                    if (loginRegisterModel.getSuccess().equalsIgnoreCase("1")) {
                        App.getAppPreference().saveLoginDetail(loginRegisterModel);
                        App.getAppPreference().SaveString(ConstantData.TOKEN, "1");
                        App.getAppPreference().SaveString(ConstantData.USERID, loginRegisterModel.getDetails().getId());
                        startActivity(new Intent(activity, MainActivity.class));
                        finishAffinity();
                    } else {
                        CommonUtils.showSnackbarAlert(signUp, loginRegisterModel.getMessage());
                    }
                }
            });
        }

    }

    private void PickDate() {
        final Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(
                SignUpActivity.this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker,
                                  int selectedyear, int selectedmonth,
                                  int selectedday) {

                mcurrentDate.set(Calendar.YEAR, selectedyear);
                mcurrentDate.set(Calendar.MONTH, selectedmonth);
                mcurrentDate.set(Calendar.DAY_OF_MONTH,
                        selectedday);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                dateOfBirthData.setText(sdf.format(mcurrentDate
                        .getTime()));
            }
        }, mYear, mMonth, mDay);

        mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        mDatePicker.setTitle("Date of Birth");
        mDatePicker.show();
    }


    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.maleRadio:
                if (checked)
                    Gender = "Male";
                break;
            case R.id.femaleRadio:
                if (checked)
                    Gender = "Female";
                break;

            case R.id.otherRadio:
                if (checked) {
                    Gender = "Other";
                }
                break;
        }
    }
}
